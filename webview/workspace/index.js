import "../shared/component/menu/menu.js";
import "../shared/component/menu/menu-item.js";
import './components/page/workspace-page.js';
import './components/page/workspace-header.js';
import './components/tree-view/indicators-tree-view.js';

window.workspacePage = () => {
    return document.getElementsByTagName("workspace-page").item(0);
}
