import "../shared/component/icon/faicon.js"
import "../shared/component/icon/fabutton.js"
import "./components/data-management/layers-edge/layers-edge-management.js"
import "./components/data-management/layers-datas/layers-data-management.js"
import "./components/data-management/layers-histo/layers-histogramme-management.js"
import "./components/data-management/layers-edge/contour-list.js"
import "../shared/component/selectfile/selectfile.js"
import "./components/data-management/layers-histo/layer-histo-display.js"
import "./components/data-management/layers-datas/layers-data-display.js"
import workspacesService from "../shared/service/workspacesService.js";


window.workspaceName = () => {    
    return workspacesService.queryWorkspaceName()
}
window.workspaceDataSetName = () => {
    return workspacesService.queryDataSetName()
}
