import { LitElement, html, css, unsafeCSS  } from '../../../lib/lit-element.js';
import ActionButtons from './model/actionButton.js';
import Legend from './model/legend.js'; 
import Background from './model/background.js';
import EdgeLayer from './model/edgeLayer.js';
import HistogramLayer from './model/histogramLayer.js';
import Histogram from './model/histogram.js';
import DataLayer from './model/dataLayer.js';
import InfoBulle from './model/infobulle.js';
import WorkspaceExport from '../page/workspace-page.js'

//mapService.convertGeoJsonMapToGaiaMundiMap("./myWorkspace/hydro.json", "./myWorkspace/data.json");

const sharp = require("sharp")
const path = require('path');
const rootPath = require('electron-root-path').rootPath

const ZONE_SELECTOR = "#svg g.background path";
const SVG_SELECTOR = "#svg";
const VIEWBOX_WIDTH = 1920;

class CarteSVG extends LitElement{
    /** Définition des types de layer */
    static LayerType = {
        background : "background",
        edge : "edge",
        data : "data",
        histogram: "histogram"
    }
    /** pointer vers le SVG via la librairie Snap (http://snapsvg.io/docs/)*/
    _svg;

    /** pointeur vers le group contenant l'ensemble des layers */
    _gLayer;

    /** pointeur vers le groupe contenant l'ensemble des layers excepté le fond de carte */
    _otherLayers;

    /** Pointeur vers le group contenant les boutons d'action */
    _gButtons;

    /** Pointeur vers le group contenant la légende */
    _gLegend;

    /**Pointeur vers le group contenant l'histograme */
    _gHistogram;

    /** Fonb d'écran */
    _background;

    /** Couches */
    _layers = [];

    /** ViewBox de la valise SVG */
    _viewBox;

    /** Zoom et translation */
    _transform ={
        scale : 1.0,
        dx : 0,
        dy : 0
    };

    /** orginial size of the backgound */
    _orgBgSize ={
        x : 0,
        y : 0,
        w : 0,
        h : 0
    };

    /** Gestion des bouttons d'action */
    _actionButtons; 

    /** Gestion des bouttons de la légende */
    _legend; 

    /** Affichage de l'infobulle */
    _infobulle;

    /**
     * Défintion des couleur pour la couche de fond
     *
     * @param {*} color couleur de fond de la cartes
     * @memberof CarteSVG
     */
    set backgroundColor(color){
        this.style.backgroundColor = color;
    }

    updateLayerBorderWidth(){
        let w = this._viewBox.w/( this._svg.node.width.baseVal.value*this._transform.scale);
        this._background.borderWidth  = w;
        this._layers.forEach((layer) => { 
            layer.borderWidth = w;
        });
    }

    /** Ratio pixel/valeur */
    get pixelRatio(){
       return {
            w : this._viewBox.w / this.width,
            h : this._viewBox.h / this.height
        }
    }

    /** Hauteur en pixel */
    get width(){
        return this._svg.node.width.baseVal.value;
    }

    /** largeur en pixel */
    get height(){
        return this._svg.node.height.baseVal.value;
    }

    /** View box du svg */
    get viewBox(){
        return this._viewBox;
    }

    /**
     * Chargement du fond de carte
     * @param {*} definition 
     */
    setBackground(definition){
        this._background = new Background(definition);    //définition des données
        this.backgroundColor = definition.style.bgColor;
        this.initializeViewPortWithBackground();
    }

     /**
     * Mise à jour d'une propriété d'une couche
     * @param {*} layerId identifiant du layer
     * @param {*} propertyPath chemin de la propriété
     * @param {*} value nouvelle valeur de la propriété
     */
    updateLayerProperty(layerId, propertyPath, value){
        let res= true;
        let layer = layerId === "background" ? this._background : this.layer(layerId);

        if(layerId === "background" && propertyPath === "style.bgColor"){
            //la couleur de fond estcas particulier
            this.backgroundColor = value;
        }else{
            switch(propertyPath){
                case "style.fillColor" :
                    layer.fillColor = value;
                    break;
                case "style.borderColor" :
                    layer.borderColor = value;
                    break;
                case "style.borderWidth" : 
                    let w = this._viewBox.w/( this.width*this._transform.scale);
                    layer.updateBorderWidth(value,w);
                    break;
                case "properties.slider.ranges":
                    //si nouveaux range reinit le silder
                    let initLength = layer.ranges.length;
                    layer.ranges = value;
                    if(initLength != value.length)
                        layer.initRanges();
                    // mise à jour des couleurs au changement des ranges
                    layer.colorize();
                    break;
                case "properties.idIndic":
                    // changement de l'indicateur de la couche
                    layer.idIndic = value;
                    
                    res = false;
                    break;
                case propertyPath.startsWith("style.rangeFillColors[") ? propertyPath : '':
                    //mise à jour d'une des couleurs du range 
                    //propertyPath example: style.rangeFillColors[2]

                    //extraire l'index de la couleur à modifier
                    let colorIndex = propertyPath.match(/\[(\d+)\]/)[1];

                    //mise à jour de la couleur dans la couche
                    layer.updateColorRange(colorIndex,value);

                    // mise à jour des couleurs
                    layer.colorize();

                    break;
                case propertyPath.startsWith("style.rangeFillColors") ? propertyPath : '':
                    //mise à jour de toutes la liste dans le cas de rajout/supression d'une couleur de plus
                    //nothing to do silder is updated at refresh
                    layer.rangeFillColors = value;

                    // mise à jour des couleurs au changement des ranges
                    layer.colorize();
                    break;
                case "style.iconType":
                    layer.iconType = value;
                    //redessiner la couche avec le nouveaux type d'icone
                    layer.redraw();
                    layer.colorize()
                    break;
                default:
                    console.debug(`Propriété inconnue ${propertyPath} pour la couche ${layerId}`);
            }

            //le backgounrd n'a pas de légende
            if(layerId !== "background" && layer._definition.type != "histogram"){
                this._legend.updateLayerProperty(layerId, propertyPath, value);
            }
            this.refreshLegend();
        }
    }

    /**
     * Récuperation d'un layer par son di
     * @param {*} id 
     */
    layer(id){
        return this._layers.find(l => l.id === id);
    }

    /**
     * Récuperation d'un layer par la profondeur d'affichage
     * @param {*} depth 
     */
     layerByDepth(depth){
        return this._layers.find(l => l.depth === depth);
    }

    /**
     * Réorganisation des couches
     * @param {*} configuration 
     */
    setDisplayConfiguration(configuration){
        //on réorganise l'ensemble des layers
        configuration.forEach( (c,idx) => {
            const layer = this.layer(c.id);
            layer.moveToBottom();
            if(c.display){
                layer.show();

            }else{
                layer.hide();
            }
            layer.depth = idx;
        });
        this._histogram.hide();
        this._legend.setDisplayConfiguration(configuration);
    }
    /**
     * Affichage Masquage d'une couche 
     * @param {*} type type de couche
     * @param {*} id identifiant de la couche
     */
    toogleLayer(type, id){
        switch(type){
            case CarteSVG.LayerType.background :
                this._background.toggle();
                break;
            case CarteSVG.LayerType.histogram:
                this.layer(id).toogle();
                this._histogram.toggle(id);
                break;
            default :
                 this.layer(id).toogle();
                 this._legend.toggle(id);
        }
    }

    /**
     * Déplacement d'une couche vers l'avant plan,
     * @param {*} layerId identifiant de la couche
     */
     moveLayerToFront(layerId){
        let layer = this.layer(layerId);
        let previousLayer = this.layerByDepth(layer.depth-1);
        previousLayer.depth++;
        layer.depth--;
        layer.moveToFront();
        this._legend.moveToFront(layerId);
    }

    /**
     * Déplacement d'une couche vers l'arrière plan,
     * @param {*} layerId  identifiant de la couche
     */
    moveLayerToBack(layerId){
        let layer = this.layer(layerId);
        let nextLayer = this.layerByDepth(ayer.depth+1);
        nextLayer.depth--;
        layer.depth++;
        layer.moveToBack();
        this._legend.moveToBack(layerId);
    }

    /**
     * Ajout d'une couche 
     * @param {*} definition définition de la couche
     */
    addLayer(definition){
        //création de la couche
        let layer;
        let depth = this._layers.length + 1;
        switch(definition.type){
            case CarteSVG.LayerType.edge :
                layer = new EdgeLayer(definition,depth);
                break;
            case  CarteSVG.LayerType.data :
                layer = new DataLayer(definition,depth);
                break;
            case CarteSVG.LayerType.histogram:
                layer = new HistogramLayer(definition, this._histogram,depth);
                break;
            default :
                console.error(`Type de layer '${definition.type}`);
        }
        this._layers.push(layer);
        let gLayer = layer.g = this._otherLayers.g();
        this._otherLayers.prepend(gLayer);
        layer.draw();
        layer.colorize();
        this.updateLayerBorderWidth();

        if (!definition.style.display) {
            layer.toogle();
        }
        
        //Ajout de la couche dans la légende
        this._legend.addLayer(definition);
        this._legend.draw();
        this._histogram.refresh();
    }

    /**
     * Suppression de l'ensemble des layers
     */
    cleanLayers(){
        while(this._layers.length > 0) {
            const layer = this._layers.pop();
            layer._g.remove();
        }
        this._legend.cleanLayers();
    }

    /** Initialisation du fond de carte */
    initializeViewPortWithBackground(){
        if(this._background && this._gLayer){
            this._background.g = this._gLayer.g();               //affectation du groupe SVG
            this._otherLayers = this._gLayer.g();               //groupe pour l'ensemble des autres couches
            this._background.colorize();
            this._background.draw();                          //dessin du fond de carte
            this.initOriginalSize();
        }
    }

    /**
     * Instanciation des coordonnées initiale du fond de carte
     */
    initOriginalSize(){
        let h = this.height;

        if(!h){
            setTimeout(this.initOriginalSize.bind(this),100);
        }else{
            let bbox = this._gLayer.getBBox();
            this._orgBgSize = {
                x:  bbox.x,  
                y : bbox.y, 
                w : bbox.width, 
                h : bbox.height,
                cx : bbox.cx,
                cy : bbox.cy
            }
            this.initViewPort();
            //window.onresize = () => { this.initViewPort();}; //on force le re calcul lors d'un redimenssionnement
        }
    }

    /**
     * Définission de la View Box pout faire en sorte que le background prenne la maximum
     * d'espace
     */
    initViewPort(){
        //récupréation de la taille réelle de la balise SVG (en px)
        let h = this.height;
        let w = this.width;
        if(h > 30 && w > 30){
            //rapport largeur/hauteur
            let c = w/h ;
                
            //définition de la view box
            this._viewBox = {x:0, y:0, w:VIEWBOX_WIDTH, h: VIEWBOX_WIDTH/c};
            this._svg.attr({viewBox: this._viewBox.x + ' ' + this._viewBox.y + ' ' + this._viewBox.w + ' ' + this._viewBox.h})
                
            this.reset(c);
            this._actionButtons.draw();
            this._legend.draw();
            this._histogram.refresh();
        }
    }

    /**
     * Raffraichissement de l'affichage de la lègende
     */
    refreshLegend(){
        this._legend.draw();
    }

    /**
     * Réinitialisation de l'affichage (Zoom/décalage)
     * @param {*} c 
     */
    reset(c){
        if(!c){
           //rapport largeur/hauteur
            c = this.width/this.height;
        }

        //calcul du rapport px / coordonnées pour adapter le contenu du
        //background au mieux
        let tx = this._viewBox.w/2 - this._orgBgSize.cx;
        let ty = this._viewBox.h/2 - this._orgBgSize.cy;
        let factor = (c > this._orgBgSize.w/this._orgBgSize.h) ? this._viewBox.h/this._orgBgSize.h : this._viewBox.w/this._orgBgSize.w;
        
        this._transform = {
            scale : factor,
            dx : tx,
            dy : ty
        }
        this.transform();
    }

    /** Agrandissement */
    zoomIn(){
        this._transform.scale *=1.2;
        this.transform();
    }

    /** Zoom arrière */
    zoomOut(){
        this._transform.scale /=1.2;
        this.transform();
    }

    /**
     * Décalage
     * @param {*} dx 
     * @param {*} dy 
     */
    translate(dx,dy){
        this._transform.dx = dx;
        this._transform.dy = dy;
        this.transform();
    }

    /**
     * Transformation de l'affichage
     */
    transform(){
        //aaplication de la transformation
        let matrix = new Snap.Matrix();
        matrix.translate(this._transform.dx,this._transform.dy).scale(this._transform.scale,-this._transform.scale, this._orgBgSize.cx, this._orgBgSize.cy);
        this._gLayer.transform(matrix.toTransformString());   
        this.updateLayerBorderWidth();    
    }

    /**
     * Export png de la carte
     * @param {*} filePath 
     */
    exportToPNG(filePath){
        this._actionButtons.hide();
        sharp(Buffer.from(this._svg.node.outerHTML))
            .png()
            .toFile(filePath)
            .catch(err => {console.error(err)})
            .finally( () => {this._actionButtons.show()});
    }

    /**
     * Identification du svg
     * @param {*} changedProperties proporiété modifiées 
     */
    firstUpdated(changedProperties){
        let svgTag = this.shadowRoot.getElementById("svg");
        this._svg = Snap(svgTag);

        this._gLayer = this._svg.g();
        this._gButtons = this._svg.g();
        this._gLegend = this._svg.g();
        this._gHistogram = this._svg.g();
        this._actionButtons = new ActionButtons(this, this._gButtons);
        this._legend = new Legend(this, this._gLegend);
        this._histogram = new Histogram(this, this._gHistogram);
        this._infobulle = new InfoBulle(this,this._svg.g(),false);

        this.initializeViewPortWithBackground();
        
        //déplacement sur la carte
        
        this._svg.mouseover((ev,x,y) => {
            let {x : xLayer, y : yLayer} = this.convertDOMCoordinateToLayerCoordinate (x,y);
            this._layers.forEach( l => {
                if(l.displayed){
                    l.mouseover(xLayer, yLayer);
                }
            });            
        })

        this._svg.mousemove((ev,x,y) => {
            let ratio = this.pixelRatio;
            let {x : xLayer, y : yLayer} = this.convertDOMCoordinateToLayerCoordinate (x,y);
            this._infobulle.setPosition(x * ratio.w,y * ratio.h);
            this._layers.forEach( l => {
                if(l.displayed){
                    l.mousemove(xLayer, yLayer);
                }
            });            
        })

        this._svg.mouseout((ev,x,y) => {
            let {xLayer,yLayer} = this.convertDOMCoordinateToLayerCoordinate (x,y);
            this._layers.forEach( l => {
                if(l.displayed){
                    l.mouseout(xLayer, yLayer);
                }
            });            
        })

        this._svg.click((ev,x,y) => {
            let {x : xLayer, y : yLayer} = this.convertDOMCoordinateToLayerCoordinate (x,y);
            this._layers.forEach( l => {
                if(l.displayed){
                    l.mouseclick(xLayer, yLayer);
                }
            });            
        })
        

        //drag and drop pour la carte
        let startPoint;
        this._svg.drag((dx,dy,x,y) => this.translate(dx * this.pixelRatio.w + startPoint.x, dy * this.pixelRatio.h + startPoint.y),
            () => startPoint = {x : this._transform.dx, y : this._transform.dy} );
        
        //utilisation de molette pour le zoom
        let canScroll = true;
        this._svg.node.addEventListener('wheel', function(e){
            if(Math.abs(e.deltaY) > 3&& canScroll){
                e.preventDefault();
                var scale = e.deltaY<0 ? this.zoomIn() : this.zoomOut();
                canScroll = false;
                setTimeout(() => {canScroll = true;}, 150);
            }
        }.bind(this));

        //gestion des évènements pour l'infobulle
        this._svg.node.addEventListener('overZone', function(e){
            this._infobulle.addInfo(e.detail.zone, e.detail.layer, e.detail.data.value);
        }.bind(this));
        this._svg.node.addEventListener('outZone', function(e){
            this._infobulle.removeInfo(e.detail.zone, e.detail.layer, e.detail.data.value);
        }.bind(this));
    }

    /**
     * Conversion des coordonnées écran en coordonnées de la couche permettant d'afficher des layer
     * @param {*} x abscisse
     * @param {*} y ordonnée
     * @returns {X,Y}
     */
    convertDOMCoordinateToLayerCoordinate(x,y){
        let rati = this.pixelRatio;
        return {
            x :this._orgBgSize.cx + (x * rati.w - this._transform.dx)/this._transform.scale,
            y : this._orgBgSize.cy+ (-(y -60) * rati.h + this._transform.dy)/this._transform.scale
        }
    }
    
    iconZoomIn(){
        this.shadowRoot.querySelectorAll('.icon').forEach(c=>{c.dispatchEvent(new Event('iconZoomIn'))});
    }
    iconZoomOut(){
        this.shadowRoot.querySelectorAll('.icon').forEach(c=>{c.dispatchEvent(new Event('iconZoomOut'))});
    }

    /** Affichage */
    render(){
        return html`<svg id="svg"></svg>`;
    }

    /** CSS spécifique au composant */
    static get styles() {
        let res = css`
        :host{
            display : block;
        }

        ${unsafeCSS(ZONE_SELECTOR)}{}

        ${unsafeCSS(SVG_SELECTOR)}{ 
            display : block;
            width: 100%;
            height : 100%;}
        
        #svg path.button{
            fill: var(--color-alt);
            stroke : var(--bg-color-alt3);
            opacity : 0.7;
        }

        #svg path.button:hover{
            fill: var(--bg-color-alt3);
            stroke : var(--color-alt);
        }

        #svg g.legend rect.main{
            fill: var(--color-alt);
            stroke : var(--bg-color-alt3);
        }

        #svg text {
            font-size : var(--font-size-body3);
        }
        
        #svg g.data path{
            opacity: 0.5;
        }

        #svg g.data circle, svg g.data rect{
            opacity: 0.7;
        }

        #svg g.infobulle rect.main {
            fill: var(--color-alt);
            stroke : var(--bg-color-alt3);
        }

        #svg g.infobulle text.zone-title {
            font-weight:bold;
        }
        `;


        return res;
    }
}

customElements.define("carte-svg", CarteSVG);
export default CarteSVG;

