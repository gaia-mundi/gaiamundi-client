import Layer from "./layer.js";

/**
 * Background de carte
 */
 class Background extends Layer{
    /**
     * Définition du backgroup
     * @param {*} definition structure définissant le background : titre, zone géographique, mail et détail de la zone
     */
    constructor(definition){
        super(definition, Number.MAX_VALUE);
    }

        /**
     * id de la couche
     *
     * @readonly
     * @memberof Layer
     */
         get id() { return "background";};

     /**
     * Récupération du group associé au background
     *
     * @memberof Background
     */
    set g(group){ 
        super.g = group;
        this._g.addClass("background");
    };
    
    /**
     * Récupération des coordonnées de centre d'une zone
     * @param {*} index index de la zone
     * @returns coordonnées (x,y) du centre de la zone
     */
    centroid(index){
        return { x : this._zones[index].cx, y : this._zones[index].cy}
    }

   /**
     * Draw the path associated with the backgound
     */
    draw(){
        super.draw((p)=>{
            var detail = { detail: {title : this.title }};
			p.node.onclick = () => {
                let event = new CustomEvent('zone-selected', detail);
                dispatchEvent(new CustomEvent(event));
            }
            p.node.onmouseover = () => {
                let event = new CustomEvent('zone-over', detail);
                dispatchEvent(new CustomEvent(event));
            }
            p.node.onmouseout = () => {
                let event = new CustomEvent('zone-out', detail);
                dispatchEvent(new CustomEvent(event));
            }
        });

    }

}

export default Background;