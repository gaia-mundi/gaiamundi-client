import layerService from "../../../../shared/service/layerService.js";
import Layer from "./layer.js";

//utiliser pour la détection des points à l'interieur d'un palygone
const SCALE = 100000;
const MATRIX = new Snap.Matrix();
MATRIX.scale(SCALE,SCALE,0,0);

class HistogramLayer extends Layer{


    _zoneElements = [];

    _selectedElement ;
    
    /**
     * liste des tableaux de données qui correspond à au colonne sélectioner
     */
    _dataList;

    _histogram;
    /**
     * Définition de la couche de contour
     * @param {*} definition structure définissant la couche : titre, détail de la zone
     * @param {*} depth profondeur du layer
     */
     constructor(definition, histogram,depth){
        super(definition,depth);
        this._histogram = histogram;
    }

    _insidePath(path,x,y){
        //La fonction isPointInside marche mal (voire pas) avec les chiffres
        //à virgule on multiplie les coordonnées par SCALE pour s'assurer
        //de travailler sur des entiers
        let t = Snap.path.map(path.attr("d"),MATRIX).toString();
        let X = Math.round(x * SCALE);
        let Y = Math.round(y * SCALE);
        return Snap.path.isPointInside(t, X, Y);
    }

    /**
     * Action sur le mouvement de la souris (interne ou externe à un layer)
     * @param {*} x 
     * @param {*} y 
     */
     mousemove(x,y){
        if(Snap.path.isPointInsideBBox(this._g.getBBox(), x, y)){
            let found = false; //utiliser pour acceler la boucle suivante
            this._zoneElements.forEach(path => {
                if(path !== this._selectedElement){
                    if(!found && this._insidePath(path,x,y)){
                        found = true;
                        path.attr({
                            fill: "white",
                            stroke: 'white',
                            opacity: 0.6
                        });
                    }else{
                        path.attr({
                            fill: 'transparent',
                            stroke: 'transparent'
                        })
                    }
                }
            });
        }
    }

     /**
     * Action sur le click de la souris 
     * @param {*} x 
     * @param {*} y 
     */
      mouseclick(x,y){
        if(Snap.path.isPointInsideBBox(this._g.getBBox(), x, y)){
            let element = this._zoneElements.find((path) => {
               return this._insidePath(path,x,y);
            });

            if(!element || this._selectedElement === element){
                if(this._selectedElement){
                    this._selectedElement.attr({
                        fill: "transparent",
                        stroke: 'transparent'
                    });
                    this._selectedElement = null;
                    this._histogram.eraseHistogram();
                }
            }else{
                if(this._selectedElement){
                    this._selectedElement.attr({
                        fill: "transparent",
                        stroke: 'transparent'
                    });
                }

                element.attr({
                    fill: "black",
                    stroke: 'white',
                    opacity: 0.5
                });
                this._selectedElement = element;

                //récupérer les données des colonnes sélectionner  qui corespond a un id carte
                let idCarte = element.data('idCarte');
                let data = this._dataList.find( data => data.idCarte == idCarte);
                this._histogram.refresh(this.title, data);

            }
        }
    }


    /**
     * dessin de la couche histogramme
     */
    draw(){
        
        this._definition.zones.forEach(zone => {

            let element = this._g.path(zone.path);

            element.attr({
                fill: 'transparent',
                stroke: 'transparent'
            })

            this._zoneElements.push(element);

            element.data('idCarte', zone.idCarte);
            
            //chargement des donnée à afficher sur l'histograme
            layerService.getHistogramData(this._definition,(dataList) => {
                this._dataList = dataList;
            });

        });
    }

    /**
    * on change le masquage de la couche
    */
    toogle() {

        let style = this._g.node.style;

        if (!style.visibility || style.visibility === "visible") {
            style.visibility = "hidden";
        } else {
            style.visibility = "visible";
            
            if (this._selectedElement && this._histogram.currentLayerId() != this._definition.id) {
                this._selectedElement.attr({
                    fill: "transparent",
                    stroke: 'transparent'
                });
                this._selectedElement = undefined;
            }
        }

    }

    colorize(){
    }

}

export default HistogramLayer;