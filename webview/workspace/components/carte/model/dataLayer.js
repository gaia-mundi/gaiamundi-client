import IconTypeConstants from "../../../../shared/util/IconTypeConstants.js";
import Layer from "./layer.js";
import layerService from '../../../../shared/service/layerService.js';
import { hslToHex , hexToHsl } from '../../../../shared/util/css-utils.js';
const { pointInSvgPath } = require('point-in-svg-path');

//utiliser pour la détection des points à l'interieur d'un palygone
const SCALE = 100000;
const MATRIX = new Snap.Matrix();
MATRIX.scale(SCALE,SCALE,0,0);

class DataLayer extends Layer{
    /**
     * liste des element svg des icone (icone de type Zone dans cas de type coloredZone)
     */
    _iconElements = [];

    /** Liste des éléments correspondants aux coordonnées de la souris */
    _currentOverElements = [];

     /**
     * Récupération du group associé au background
     *
     * @memberof Background
     */
    set g(group){ 
        super.g = group;
        this._g.addClass("data");
    };

    set rangeFillColors(colors) {
        this._definition.style.rangeFillColors = colors;
    }

    set ranges(ranges) {
        this._definition.slider.ranges = ranges;
    }

    get ranges() {
        return this._definition.slider.ranges;
    }

    set iconType(iconType) {
        this._definition.style.iconType = iconType;
    }

    get idIndic() {
        return this._definition.idIndic;
    }

    set idIndic(idIndic) {
        this._definition.idIndic = idIndic;
        this.resetRanges();
    }


    /**
     * dessin de la couche
     */
    draw() {
        switch (this._definition.style.iconType) {
            case IconTypeConstants.ICON_CIRCLE:
                this.drawCircle();
                break;
            
            case IconTypeConstants.ICON_SQUARE:
                this.drawSquare();
                break;
            
            case IconTypeConstants.ICON_COLORED_ZONES:
                this.drawColoredZones();
                break;
        }
    }

    /**
     * initier le slider avec les donnés calculer
     * @param {*} dataList les donnés calculer
     */
    initSlider(dataList) {
        let maxValue = Math.max(...dataList.filter(d => !isNaN(d.value)).map(d => d.value));
        let minValue = Math.min(...dataList.filter(d => !isNaN(d.value)).map(d => d.value));
        let step = 1;//TODO: à modifier pour pouvoir donner au user la main do preciser cette valeur ;

        this._definition.slider.maxValue = maxValue;
        this._definition.slider.minValue = minValue;
        this._definition.slider.step = step;
        this.initRanges();
       
        this._updateLayerSlider(this._definition.id);
    }

    
    /**
     * modifier une couleur du range
     * @param {*} index l'index de la couleur à modifier
     * @param {*} color la nouvelle couleur
     */
     updateColorRange(index, color){
        this._definition.style.rangeFillColors[index] = color;
    }

    /**
     * calculer/recalculer les range initiale du slider
     */
    initRanges() {
        let colorNbr = this._definition.style.rangeFillColors.length
        let rangeNbr = colorNbr - 1;
        let ranges = [];
        //couleur unique pas de ranges
        if (rangeNbr > 0) {
            let rangStep = (this._definition.slider.maxValue - this._definition.slider.minValue) / colorNbr;
            ranges = [this._definition.slider.minValue + rangStep];
            for (let i = 1; i < rangeNbr; i++) {
                ranges[i] = ranges[i - 1] + rangStep;
            }
        }
        this._definition.slider.ranges = ranges;
    }

    resetRanges() {
        layerService.calculateData(this._definition,(data)=>{
            this.initSlider(data);
            this.redraw();
          
        });
    }

    /**
     * permet de mettre à jour les données du slider de la couche dans le workspace.json
     * @param {*} id layerId
     */
    _updateLayerSlider(id){
        //dispatch an event via the carte-svg element
        let event = new CustomEvent('update-layer-slider',{ detail: { id : id }});
        let carteSvgElement = this._g.paper.node.parentNode.host;
        carteSvgElement.dispatchEvent(event);
    }

    /**
     * Calcul de la couleur de bordure en fonction de 
     * la couleur de remplissage
     * @param {*} fillHexValue 
     */
    computeBorderColor(fillHexValue){
        let hsl = hexToHsl(fillHexValue);
        let l = hsl.l;
        hsl.l = parseInt(l <= 50 ? (100-l)/2 : l/2);
        return hslToHex(hsl.h,hsl.s,hsl.l);
    }
    /**
     * récupérer la couleur qui correspond à la valuer
     * @param {*} value 
     * @returns 
     */
    getColorForValue(value) {
        let ranges = this._definition.slider.ranges;
        let colors = this._definition.style.rangeFillColors;

        let idx = ranges.length;
        let breakNow = true;
        for (let i = 0; i < ranges.length && breakNow; i++) {
            if (value <= ranges[i]) {
                idx = i;
                breakNow = false;
            }
        }

        //return {fill : colors[idx], stroke : this.computeBorderColor(colors[idx])};
        return {fill : colors[idx], stroke : "#000"};
    }


    colorize() {
        this._iconElements.forEach(ele => {

            let data = ele.data('value');
            let colors = this.getColorForValue(data); 

            ele.attr("stroke", colors.stroke);
            ele.attr("fill", colors.fill);
        })
    }

    /**
     * Association d'un objet SVG présentant une zone à ses données
     * @param {*} object 
     * @param {*} data 
     * @param {*} zone 
     */
    associatedSVGObject(object, data, zone){
        object.data("cairo_data",data);
        object.data("cairo_zone",zone);
        object.data("cairo_layer", this);
    }

    /**
     * Action sur le mouvement de la souris (interne ou externe à un layer)
     * @param {*} x 
     * @param {*} y 
     */
    mousemove(x,y){
        if(Snap.path.isPointInsideBBox(this._g.getBBox(), x, y)){
            let elm = this.getIconElementsByCoor(x,y);
            //calcul des nouvelles et anciennes zone sourvolées par la souris
            let newElm = elm.filter( item => !this._currentOverElements.includes(item));
            let oldElm = this._currentOverElements.filter( item => !elm.includes(item));
            //emission des évènement correspondant
            newElm.forEach(item => this.emitEvent("overZone"
                                            , item.data("cairo_zone")
                                            , item.data("cairo_layer")
                                            , item.data("cairo_data")
                        )
            );
            oldElm.forEach(item => this.emitEvent("outZone"
                                            , item.data("cairo_zone")
                                            , item.data("cairo_layer")
                                            , item.data("cairo_data")
                        )
            );
            this._currentOverElements = elm;
        }else{
            this._currentOverElements.forEach(item => this.emitEvent("outZone"
                , item.data("cairo_zone")
                , item.data("cairo_layer")
                , item.data("cairo_data")));
            this._currentOverElements = [];
        }
    }

    /**
     * Emission d'un évènement sur la zone
     * @param {*} eventType 
     * @param {*} zone 
     * @param {*} layer 
     * @param {*} data 
     */
    emitEvent(eventType, zone, layer, data){
        const eventToEmit = new CustomEvent(eventType, {
            bubbles: true,
            detail: { zone:zone, layer:layer, data:data}
          });
          this._g.node.dispatchEvent(eventToEmit);
    }
    /**
     * Récupération de l'élément associé aux coordonnes
     * @param {*} x 
     * @param {*} y 
     * @returns l'ensemble des élments correspondant
     */
    getIconElementsByCoor(x,y){
        return this._iconElements.filter( elm => {
            let res = false;
            switch (this._definition.style.iconType) {
                case IconTypeConstants.ICON_CIRCLE:
                    let cx = elm.attr("cx");
                    let cy = elm.attr("cy");
                    let r = elm.attr("r");
                    res = Math.pow(x-cx,2) + Math.pow(y-cy,2) <= Math.pow(r,2);
                    break;
                
                case IconTypeConstants.ICON_SQUARE:
                    let bbox = elm.getBBox();
                    res = bbox.x < x && bbox.y < y && bbox.x + bbox.width > x && bbox.y + bbox.height > y
                    break;
                case IconTypeConstants.ICON_COLORED_ZONES:
                    //La fonction isPointInside marche mal (voire pas) avec les chiffres
                    //à virgule on multiplie les coordonnées par SCALE pour s'assurer
                    //de travailler sur des entiers
                    let t = Snap.path.map(elm.attr("d"),MATRIX).toString();
                    let X = Math.round(x * SCALE);
                    let Y = Math.round(y * SCALE);
                    res = Snap.path.isPointInside(t, X, Y);
                    break;
            }
            return res;
        });
    }

    /**
     * dessin des icones cercle
     */
    drawCircle(){
        if (this._definition.idIndic){
            // calculate the data to display
            layerService.calculateData(this._definition,(data)=>{
                
                //valeur du zoom max initial (pour éviter qu'une valeur très grande nous crée une icone qui couvre toute la carte)
                //il correspond au diagonale du cercle
                let valMaxZoomInit = 0.02;

        
                let maxValue = Math.max(...data.filter(d => !isNaN(d.value)).map(d => Math.abs(d.value)));
        
                //le coefficient sur lequel on va multiplier pour avoir le rayon
                let coef = (valMaxZoomInit) / maxValue;

                //initier les valeurs utilisées par le slider
                // verifier si le slider et initier
                if(!this._definition.slider.maxValue)
                    this.initSlider(data);

                data.forEach(d => {

                    //recuperation de la zone
                    let zone = this._definition.zones.find(z => z.idCarte === d.idCarte);
                    if (zone && d.value && !isNaN(d.value)) {
                        
                        //dessin du cercle
                        let circle = this._g.circle(zone.cx, zone.cy, 0);
                        this.associatedSVGObject(circle, d, zone);
                        this._iconElements.push(circle);
                        circle.data('value', d.value);
    
        
                        let diagonal = d.value * coef;
        
                        if (diagonal > 0.0005) {
                            circle.attr({
                                r: diagonal/2
                            });
                        } else if (diagonal <= 0.0005 && diagonal > 0) {
                            circle.attr({
                                r: 0.00025
                            });
                        } else {
                            circle.attr({
                                r: Math.abs(diagonal/2)
                            });
                            this._g.rect(zone.cx - 0.0005, zone.cy - 0.00025, 0.001, 0.0005).attr({fill: this._definition.style.borderColor,
                                stroke: this._definition.style.borderColor});                
                        }
        
                        circle.addClass('icon');
                        circle.data('diagonal', Math.abs(diagonal));
                        // let scaleLevel = 1;
                        // circle.data('scaleLevel', scaleLevel);

                        //ajout d'un evenement pour zommer l'icone
                        circle.node.addEventListener('iconZoomIn', (e) => {
                            let diagonal = parseFloat(circle.data('diagonal'));
        
                            let newDiagonal= diagonal*1.2;
                            circle.data('diagonal', newDiagonal);
                            if (newDiagonal < 0.0005) {
                                circle.attr({
                                    r: 0.00025,
                                })
                        } else {
                            circle.attr({
                                r: newDiagonal/2,
                            })
                        }
                    });

                    //Ajout d'un événement pour dézoomer l'icone
                    circle.node.addEventListener('iconZoomOut', (e) => {
                        let diagonal = parseFloat(circle.data('diagonal'));
                        let newDiagonal = diagonal/1.2;
                        circle.data('diagonal', newDiagonal);
    
                        if (newDiagonal < 0.0005){
                            circle.attr({
                                r: 0.00025,
                            })
                        } else {
                            circle.attr({
                                r: diagonal/2,
                            })
                        }

                    })
                }
    
            });
            this.colorize();
        })}else{return}
    }   

    /**
     * dessin des icones carrées
     */
    drawSquare(){   
        if (this._definition.idIndic){     
        // calculate the data to display
        layerService.calculateData(this._definition,(data)=>{
        
            //valeur du zoom max initial (pour éviter qu'une valeur très grande nous crée une icone qui couvre toute la carte)
            //il correspond au diagonale du carré
            let valMaxZoomInit = 0.02;
            
            let maxValue = Math.max(...data.filter(d => !isNaN(d.value)).map(d => Math.abs(d.value)));

    
            //le coefficient sur lequel on va multiplier pour avoir la longueur du coté du carré (sachant que la maxvalue correspond a un carré de diag valMaxZoomInit)
            let coef = valMaxZoomInit / maxValue;


            //initier les valeurs utilisées par le slider
            // verifier si le slider et initier
            if(!this._definition.slider.maxValue)
                this.initSlider(data);
    
            data.forEach(d => {
                let zone = this._definition.zones.find(z => z.idCarte == d.idCarte);
                if (zone && d.value && !isNaN(d.value)) {
                    
                    let squareDiagonalLength = d.value * coef;
    
                    let squareSideLength = Math.sqrt(squareDiagonalLength*squareDiagonalLength/2);
                    
                    let x = zone.cx - Math.abs(squareSideLength)/2;
                    let y = zone.cy - Math.abs(squareSideLength)/2;
                    
                    let smallSquareSideLength = Math.sqrt(0.001*0.001/2);

                    //dessin du carré
                    let square = this._g.rect(x, y, 0, 0);
                    this.associatedSVGObject(square, d, zone);
                    this._iconElements.push(square);
                    square.data('value', d.value);
                    // let color = this.getColorForValue(d.value);
                    
                    if (squareDiagonalLength > 0.0005) {
                        square.attr({
                            height: squareSideLength,
                            width: squareSideLength
                        });
                    } else if (squareDiagonalLength <= 0.0005 && squareDiagonalLength > 0 ) {

                        square.attr({
                            height: smallSquareSideLength,
                            width: smallSquareSideLength
                        });
                    } else {
                        square.attr({
                            height: squareSideLength,
                            width: squareSideLength
                        });
                        this._g.rect(zone.cx - 0.0005, zone.cy - 0.00025, 0.001, 0.0005).attr({fill: this._definition.style.borderColor,
                            stroke: this._definition.style.borderColor});
                    }
                    
                    square.addClass('icon');
                    square.data('squareDiagonalLength', Math.abs(squareDiagonalLength));
    
                    //ajout d'un evenement pour zommer l'icone
                    square.node.addEventListener('iconZoomIn', (e) => {
                        
                        let squareDiagonalLength = parseFloat(square.data('squareDiagonalLength'));
                        let newSquareDiagonalLength = squareDiagonalLength * 1.2;
                        let newSquareSideLength = Math.sqrt(newSquareDiagonalLength*newSquareDiagonalLength/2);
                        
    
                        square.data('squareDiagonalLength', newSquareDiagonalLength);
    
                        let newX = zone.cx - Math.abs(newSquareSideLength)/2;
                        let newY = zone.cy - Math.abs(newSquareSideLength)/2;
    
                        if (newSquareSideLength < 0.0005) {

                            square.attr({
                                height: smallSquareSideLength,
                                width: smallSquareSideLength
                            })
                        } else {
                            square.attr({
                                x: newX,
                                y: newY,
                                height: newSquareSideLength,
                                width: newSquareSideLength
                            })
                        }
                    });
    
                    //Ajout d'un événement pour dézoomer l'icone
                    square.node.addEventListener('iconZoomOut', (e) => {
                        let squareDiagonalLength = parseFloat(square.data('squareDiagonalLength'));
                        let newSquareDiagonalLength = squareDiagonalLength / 1.2;
                        let newSquareSideLength = Math.sqrt(newSquareDiagonalLength*newSquareDiagonalLength/2);
    
                        square.data('squareDiagonalLength', newSquareDiagonalLength);
    
    
                        let newX = zone.cx - Math.abs(newSquareSideLength)/2;
                        let newY = zone.cy - Math.abs(newSquareSideLength)/2;
    
                        if (newSquareSideLength < 0.0005) {
                            square.attr({
                                height: smallSquareSideLength,
                                width: smallSquareSideLength
                            })
                        } else {
                            square.attr({
                                x: newX,
                                y: newY,
                                height: newSquareSideLength,
                                width: newSquareSideLength
                            })
                        }
                    })
                }

            });
            this.colorize();
        })}else{return};
    }
    
    /**
     * dessin des zones colorée
     */
    drawColoredZones(){   
        if (this._definition.idIndic){
        //recuperation des données calculer
        layerService.calculateData(this._definition, (dataList) => {

            //initier les valeurs utilisées par le slider
            // verifier si le slider est initier
            if(!this._definition.slider.maxValue)
                this.initSlider(dataList);


            this._definition.zones.forEach((z) => {

                let element = this._g.path(z.path);
                let data = dataList.find(data => data.idCarte == z.idCarte);
                element.data('value', data.value);
                // element.addClass('icon');
                this.associatedSVGObject(element, data, z);
                this._iconElements.push(element);

            });

            this.colorize();
        })}else{return};
    }

    /**
     * redessiner la couche (suite à un changement de type d'icon )
     */
    redraw(){
        this._iconElements.map(ele => ele.remove());
        this._iconElements = [];
        this.draw();
    }

}

export default DataLayer;