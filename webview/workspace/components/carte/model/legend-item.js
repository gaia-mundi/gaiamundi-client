/**
 * Legende de la carte
 */
 class LegendItem {
    
   /**
    * Legende contenant l'item
    *
    * @memberof LegendItem
    */
   _legend;

     /** Groupe SVG associé à la couche de contour */
    _g;

    /** configuration du layer  */
    _definition;

    /**
     * Définition de la legende pour une couche
     * @param {*} legend Legende contenant l'item
     * @param {*} definition structure définissant la couche : titre, détail de la zone
     */
    constructor(legend, definition){
        this._legend = legend;
        this._definition = definition;
        this._g = legend._g.g();
    }

    /**Récupération du contour de la légende */
    get bbox(){
       return this._g.getBBox();
    }

    get visible(){
       return this._g.node.style.visibility === "visible" 
               || this._g.node.style.visibility === "";
    }

    /**
     * Suppression de la légence
     */
    remove(){
      this._g.remove();
      this._g = null;
    }
    /**
      * Masque les boutons d'action 
      */
     hide(){
      this._g.node.style.visibility = "hidden";
   }

   /**
   * affiche les boutons d'action 
   */
   show(){
    this._g.node.style.visibility = "visible";
  }

  /**
   * Affichage masquage de la couche
   */
  toggle(){
   this._g.node.style.visibility = this.visible ? "hidden" : "visible";
  }

  /** Définition de la tail de la fonte */
   get fontsize(){
      let stdSize = getComputedStyle(this._legend._carte).getPropertyValue('--font-size-body3');
      return parseInt(parseInt(stdSize) * this._legend.ratio.h);
   }    

  /**
   * definition de la position {x,y}
   * @memberof LegendItem
   */
   set position(coor){
      let matrix = new Snap.Matrix();
      matrix.translate(coor.x,coor.y);
      this._g.transform(matrix.toTransformString());
   }

   /**
   * Mise à jour d'une propriété d'une couche
   * @param {*} propertyPath chemin de la propriété
   * @param {*} value nouvelle valeur de la propriété
   */
   updateProperty(propertyPath, value){
   }

   /**
   * Dessin de l'élément de légende
   */
   draw(){
   }
      
 }

 export default LegendItem;