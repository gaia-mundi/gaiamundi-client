class Layer {
    /** Groupe SVG associé à la couche de contour */
    _g;

    /** configuration du layer  */
    _definition;

    /**
     * Profondeur du layer
     * @memberof Layer
     */
    _depth;

    /**
     * Définition de la couche de contour
     * @param {*} definition structure définissant la couche : titre, détail de la zone
     * @param {*} depth profondeur du layer
     */
    constructor(definition,depth){
        this._definition = definition;
        this._depth = depth
    }

    /**
     * Titre de la couche
     *
     * @readonly
     * @memberof Layer
     */
    get title() { return this._definition.title};

    /**
     * id de la couche
     *
     * @readonly
     * @memberof Layer
     */
     get id() { return this._definition.id};

    /**
     * Groupe associé à la couche
     *
     * @readonly
     * @memberof Layer
     */
    get g(){ return this._g};

    /**
     * Récupération du group associé à la couche
     *
     * @memberof Layer
     */
     set g(group){ 
        this._g = group;
    };

    /**
     * Récupération de la profondeur de la couche
     *
     * @readonly
     * @memberof Layer
     */
    get depth(){ return this._depth};

    /**
     * Définition de la nouvelle profondeur
     *
     * @memberof Layer
     */
    set depth(depth){
        this._depth = depth;
    }
    /**
     * Déifnintion de la couleur du background
     * @param {*} bgColr couleur de repmplissage
     * @param {*} borderColor couleur du contour
     */
     colorize(){
      this._g.attr("stroke",this._definition.style.borderColor);
      this._g.attr("fill",this._definition.style.fillColor);
    }

    set borderColor(color){
        this._definition.style.borderColor = color;
        this.colorize();
    }

    set fillColor(color){
        this._definition.style.fillColor = color;
        this.colorize();
    }

    /**
     * Action sur le mouvement de la souris (interne ou externe à un layer)
     * @param {*} x 
     * @param {*} y 
     */
    mousemove(x,y){
       
    }

    /**
     * Action sur le mouvement de la souris (interne ou externe à un layer)
     * @param {*} x 
     * @param {*} y 
     */
    mouseover(x,y){
   
    }

    /**
     * Action sur la sortie de la souris de la zone associée au layer
     */
    mouseout(){

    }

     /**
     * Action sur le click de la souris 
     * @param {*} x 
     * @param {*} y 
     */
    mouseclick(x,y){

    }

    /**
     * Mise à jour de la largeur de base des countours puis on réaffecte le rapport de zoom
     * @param {*} baseWidth largeur de base en pixel de la bordure
     * @param {*} scale facteur de zoom
     */
    updateBorderWidth(baseWidth, scale){
        this._definition.style.borderWidth = baseWidth;
        this.borderWidth = scale;
    }

    /**
     * Largeur des contours
     * @memberof Layer
     */
    set borderWidth(width){
        let borderWidthFactor = this._definition.style.borderWidth ? this._definition.style.borderWidth : 1;
        this._g.attr("stroke-width",width * borderWidthFactor);
    }

  /**
   * Permet de spécifier si la couche est affiché
   *
   * @readonly
   * @memberof Layer
   */
  get displayed(){
      return this._g.node.style.visibility !== "hidden";
  }
    /**
     * on change le masquage de la couche
     */
     toogle(){
      let style = this._g.node.style;
      style.visibility = !style.visibility || style.visibility === "visible" ? "hidden" : "visible";
  }

  /**
   * On masque la couche
   */
    hide(){
        let style = this._g.node.style;
        style.visibility = "hidden";
    }

    /**
     * On affiche la couche
     */
    show(){
        let style = this._g.node.style;
        style.visibility = "visible";
    }

    /**
         * Déplacement d'une couche vers l'avant plan,
         */
    moveToFront(){
        this._g.incZindex();
    }

    /**
     * Déplacement d'une couche vers l'arrière plan,
     */
    moveToBack(){
        this._g.decZindex();
    }

    /**
     * Déplacement de la couche en base de la pile
     */
    moveToBottom(){
        this._g.bringToBack();
    }

   /**
     * Draw the path associated with the backgound
     * @param {*} onDrawZone fonction prenant en parametre le path construit
     */
    draw(onDrawZone){
        this._definition.zones.forEach((z) => { 
              let p = this._g.path(z.path);
              if(onDrawZone){ onDrawZone(p);}
          });
      }

}

export default Layer;