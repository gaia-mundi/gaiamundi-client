import Layer from "./layer.js";
/**
 * Couche de contour
 */
 class EdgeLayer extends Layer{
    /**
     * Définition de la couche de contour
     * @param {*} definition structure définissant la couche : titre, détail de la zone
     * @param {*} depth profondeur du layer
     */
    constructor(definition, depth){
        super(definition,depth);
    }

     /**
     * Groupe associé à la couche
     *
     * @readonly
     * @memberof Layer
     */
      get g(){ return super.g};

    /**
     * Récupération du group associé à la couche
     *
     * @memberof EdgeLayer
     */
     set g(group){ 
        super.g = group;
        this._g.addClass("edgelayer");
    };

 }

 export default EdgeLayer;