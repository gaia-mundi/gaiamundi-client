import LegendItemEdge from "./legend-item-edge.js";
import LegendItemData from "./legend-item-data.js";

/**
 * Legende de la carte
 */
 class Legend {
     static WIDTH = 200;
     static PADDING = 5;
     static ITEM_PADDING = 3;
     static BORDER = 1.5;
     /**
      *Nombre de pixel pôur l'interligne
      * 
      * @static
      * @memberof Legend
      */
     static INTERLIGNE = 2;
     
     /**
      * Composant de la carte
      *
      * @memberof Legend
      */
     _carte;

     /**
      * group la légende
      *
      * @memberof Legend
      */
     _g

     /**
      * liste des elements de légende associée à chaque couche
      *
      * @memberof Legend
      */
     _layers;

     /**
      * Identifiant ordonné de couche
      */
     _layersOrders;

     /**
      * contour associé à la légende
      *
      * @memberof Legend
      */
     _rect;

     /**
      * 
      * @param {*} carte carte associée à la légende
      * @param {*} g groupe contenant à la légende
      */
     constructor(carte, g){
        this._carte = carte;
        this._g = g;
        this._g.addClass("legend");
        this._layers = new Map();
        this._layersOrders = [];
     }

     /** carte associée à la légende */
     get carte(){
        return this._carte;
     }

     /**
      * Renvoi le ratio valeur/pixel dans le svg
      *
      * @readonly
      * @memberof Legend
      */
     get ratio(){
         return this._carte.pixelRatio;
     }
     /**
      * Masque les boutons d'action 
      */
     hide(){
        this._g.node.style.visibility = "hidden";
     }

     /**
     * affiche les boutons d'action 
     */
     show(){
      this._g.node.style.visibility = "visible";
     }

    /**
     * Déplacement des couches
     * @param {*} layerId identifiant de la couche
     * @param {*} delta decalage
     */
    move(layerId, delta){
       const idx = this._layersOrders.indexOf(layerId);
       if(idx+delta >= 0 && idx+delta < this._layersOrders.length){
       const layer = this._layersOrders.splice(idx,1)[0];
       this._layersOrders.splice(idx+delta,0,layer);

       this.draw();
       }
    }
    /**
     * Switch de l'affichage d'un item
     * @param {*} layerId identifiant de la couche
     */
    toggle(layerId){
      this._layers.get(layerId).toggle();
      this.draw();
    }

    /**
     * Déplacement de la couche vers le haut de la pile
     * @param {*} layerId 
     */
    moveToFront(layerId){
      this.move(layerId, 1);
    }

    /**
     * Déplacement de la couche vers le bas de la pile
     * @param {*} layerId 
     */
    moveToBack(layerId){
      this.move(layerId, -1);
    }

    /**
     * Mise à jour d'une propriété d'une couche
     * @param {*} layerId identifiant du layer
     * @param {*} propertyPath chemin de la propriété
     * @param {*} value nouvelle valeur de la propriété
     */
     updateLayerProperty(layerId, propertyPath, value){
        this._layers.get(layerId).updateProperty(propertyPath, value);
     }

    /**
     * Dessine la légende et l'ensemble des éléments de légende
     */
    draw(){
         let hPadding = Legend.PADDING * this.ratio.h;
         let itemPadding = Legend.ITEM_PADDING * this.ratio.h;
         let wPadding = Legend.PADDING * this.ratio.w;
         let bottom = this._carte.height * this.ratio.h - hPadding;
         let top = bottom - hPadding;

         //création du cadre
         if(!this._rect){
            this._rect = this._g.rect();
            this._rect.addClass("main");
             //affichage masquage de la couche
         }

         //dessin des légendes des différentes couches
         this._layersOrders.forEach( id => {
            let item = this._layers.get(id);
            if(item.visible){
               item.draw();
               top -= item.bbox.height;
               item.position = { x : 2 * wPadding, y : top};
               top -= itemPadding;
            }
         });
         
         //dessin du cadre
         let coor = {x: wPadding, y : top - hPadding , w : Legend.WIDTH * this.ratio.w, h : bottom - top + hPadding};
         let border = Legend.BORDER *  this.ratio.w;
         this._rect.attr( {
            "x" : coor.x,
            "y" : coor.y,
            "width" : coor.w,
            "height" : coor.h,
            "strokeWidth": Legend.BORDER *  this.ratio.w});
         let clip = `M ${-border} ${-border} h ${coor.w + 2*border} v ${coor.h + 2*border} h -${coor.w + 2*border} Z`;
         // this._g.attr("clip-path", `path('${clip}')`); //TODO: the path clip in this line hide some of the legends. to be fixed
     }

     /**
      * Masque la lègende associé à une couche
      * @param {*} id identifiant de la couche
      */
     hideItem(id){
         this._layers.get(id).hide();
     }

     /**
      * Affiche la lègende associé à une couche
      * @param {*} id identifiant de la couche
      */
      showItem(id){
         this._layers.get(id).show();
      }
     /**
      * Ajout d'une couche dans la legende
      *
      * @param {*} definition définition de la légende
      * @memberof Legend
      */
     addLayer(definition){
         let item;
      
         switch(definition.type){
            case "edge" : 
               item = new LegendItemEdge(this,definition);
               break;
            case "data" :
               item = new LegendItemData(this,definition);
               break;
            case "histogram":
               //do nothing because ne legends needed for histogram layer
               break;
            default : 
               console.error(`Type de layer '${definition.type}' inconnu`);
         }

         
         if(item){
            //ajout de la couche
            this._layers.set(definition.id, item);
            this._layersOrders.push(definition.id);
            //affichage masquage de la couche
            if(definition.style.display){
               item.show();
            }else{
               item.hide();
            }
         }
      }

   /**
   * Définition de l'ordre d'affichage et du status
   * @param {*} configuration 
   */
   setDisplayConfiguration(configuration){
      this._layersOrders = [];
      configuration.forEach(conf => {
         if(this._layers.get(conf.id)){
            this._layersOrders.push(conf.id);
            if(conf.display){
               this.showItem(conf.id);
            }else{
               this.hideItem(conf.id);
            }
         }
      });
      this.draw();
   }

      /**
       * Suppression de l'ensemble des couches
       * @memberof Legend
       */
      cleanLayers(){
         while(this._layersOrders.length > 0){
            const id = this._layersOrders.pop();
            const item = this._layers.get(id);
            item.remove();
         }
         this._layers.clear();
         this.draw();
      }
     
 }

 export default Legend;