import LegendItem from "./legend-item.js";
import Legend from "./legend.js";
import {WorkspaceExport} from "../../page/workspace-page.js"


/**
 * Legende de la carte
 */
class LegendItemData extends LegendItem {
    static WIDTH = 20;
    static HEIGHT = 10;
    static MARGIN = 5;

    _Legends = [];
    _title;

    /**
    * Définition de la legende pour une couche
    * @param {*} legend Legende contenant l'item
    * @param {*} definition structure définissant la couche : titre, détail de la zone
    */
    constructor(legend, definition) {
        super(legend, definition)
    }

    /**
    * Dessin de l'élément de légende
    */
    draw() {
        super.draw();
        //WorkspaceExport.updateLayerSlider();
        let ranges = this._definition.slider.ranges;
        let colors = this._definition.style.rangeFillColors;
        let minValue = this._definition.slider.minValue;
        let maxValue = this._definition.slider.maxValue;

        let previous = minValue;
        let i = 0;

        this._drawTitleLegend(this._definition.title);

        //dessiner les legendes
        for (; i < ranges.length; i++) {
            let title = '[ ' + parseInt(previous) + ' ; ' + parseInt(ranges[i]) + ' ]';
            previous = ranges[i];

            this._drawRangeLegend( title, colors[i], i+1, this._Legends[i]);}
            
        //dessiner la derniere legend qui corspond au dernier range jusqu'au valeur max
        this._drawRangeLegend( '[ ' + parseInt(previous) + ' ; ' + parseInt(maxValue) + ' ]', colors[i], i+1, this._Legends[i]);

        // Dans le cas du suppression d'une ou plusieurs quantiles supprimer les legends qui sont en plus
        let legendNbrToDraw =  colors.length;
        let legendCurrentNbr = this._Legends.length;
        if( legendCurrentNbr > legendNbrToDraw) {
            for(let i = legendNbrToDraw ; i < legendCurrentNbr; i++){
                this._Legends[this._Legends.length-1].text.remove();
                this._Legends[this._Legends.length-1].icon.remove();
                this._Legends.pop();
            }
        }
    }

    _drawTitleLegend(title) {
        if(!this._title){
            this._title = this._g.text();
        }

        this._title.attr({
            "y": this.fontsize,
            "text": title,
            "font-size": this.fontsize,
        });
        this._title.node.style.textTransform = "uppercase";

    }

    /**
     * Dessine la legend et mis à jour le titre et couleur si déjà dessiner.
     * @param {*} title titre de la legende 
     * @param {*} color couleur de la legend
     * @param {*} index l'index de la legend dans la liste des legende
     * @param {*} legend la legende
     */
    _drawRangeLegend(title, color, index, legend) {

        if (!legend) {
            let text = this._g.text();
            let rect = this._g.rect(0, 10, 40, 40);

            legend = {
                text: text,
                icon: rect
            };

            this._Legends.push(legend)
        }

        legend.text.attr({
            "y": this.fontsize + (index * (LegendItemData.HEIGHT + LegendItemData.MARGIN) * this._legend.ratio.h),
            "text": title,
            "font-size": this.fontsize
        });

        let h = LegendItemData.HEIGHT * this._legend.ratio.h;

        legend.icon.attr({
            "x": (Legend.WIDTH - 2 * Legend.PADDING - LegendItemData.WIDTH) * this._legend.ratio.w,
            "y": ((this.fontsize - h) / 2) + (index * (LegendItemData.HEIGHT + LegendItemData.MARGIN) * this._legend.ratio.h),
            "width": LegendItemData.WIDTH * this._legend.ratio.w,
            "height": h,
            "stroke-width": Legend.BORDER * this._legend.ratio.w,
            "fill": color,
            "stroke": this._definition.style.borderColor
        });
    }


}
export default LegendItemData;