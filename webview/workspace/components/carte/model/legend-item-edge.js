import LegendItem from "./legend-item.js";
import Legend from "./legend.js";
/**
 * Legende de la carte
 */
 class LegendItemEdge extends LegendItem{
   static WIDTH = 20;
   static HEIGHT = 10;
   /**
    * label pour la couche
    * @memberof LegendItemEdge
    */
   _title;

   /**
    * icon associté au titre
    *
    * @memberof LegendItemEdge
    */
   _icon;

   /**
     * Définition de la legende pour une couche
     * @param {*} legend Legende contenant l'item
     * @param {*} definition structure définissant la couche : titre, détail de la zone
     */
   constructor(legend, definition){
      super(legend, definition)
   } 

   /**
   * Dessin de l'élément de légende
   */
   draw(){
      super.draw();
      let fontSize = this.fontsize;
      if(!this._title){
         this._title = this._g.text();
         this._icon = this._g.rect(0,10,40,40);
      }

      this._title.attr({
         "y" : fontSize,
         "#text" : this._definition.title,
         "font-size" : this.fontsize
      });

      this._title.node.style.textTransform = "uppercase";


      //si backgroundground transparent on trace une ligne de l'epaisseur de la couche
      let h = this._definition.style.fillColor === "transparent" 
               ? this._definition.style.borderWidth * this._legend.ratio.h
               : LegendItemEdge.HEIGHT * this._legend.ratio.h;
      let fill =  this._definition.style.fillColor === "transparent" 
               ? this._definition.style.borderColor
               : this._definition.style.fillColor;

      this._icon.attr({
         "x" : (Legend.WIDTH - 2 * Legend.PADDING - LegendItemEdge.WIDTH) * this._legend.ratio.w,
         "y" : (fontSize - h)/2,
         "width" : LegendItemEdge.WIDTH * this._legend.ratio.w,
         "height" : h,
         "stroke-width" : Legend.BORDER * this._legend.ratio.w,
         "fill" : fill,
         "stroke" : this._definition.style.borderColor
      });
   }

   /**
   * Mise à jour d'une propriété d'une couche
   * @param {*} propertyPath chemin de la propriété
   * @param {*} value nouvelle valeur de la propriété
   */
   updateProperty(propertyPath, value){
      super.updateProperty();
      switch(propertyPath){
         case "style.fillColor" :
            if(value === "transparent"){
               this._icon.attr({
                     "fill" : this._icon.attr("stroke"),
                     "height" : this._definition.style.borderWidth * this._legend.ratio.h
               });
            }else{
               this._icon.attr({
                  "fill" : value,
                  "height" :LegendItemEdge.HEIGHT * this._legend.ratio.h
               });
            }
            break;
         case "style.borderColor" :
            this._icon.attr("stroke", value);
            break;
         default:
      }
   }
 }

 export default LegendItemEdge;