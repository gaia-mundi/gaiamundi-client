import MultipleSlider from "../../../shared/component/slider/slider.js";

class LayerSlide extends MultipleSlider{

    _layer;

    /**
     * Affectation d'un layer associé au slider
     *
     * @memberof LayerSlide
     */
    set layer(layer){
        this._layer = layer;
        this._setLayerDataToSlider();
    }
    
    /**
     * 
     * @returns récupérer l'id de la couche courante sur le slider
     */
    getLayerId(){
        if(this._layer)
            return this._layer.id;
    }

    /**
     * initialiser le slider avec les données de la couche
     */
    _setLayerDataToSlider(){
        this.colors = this._layer.style.rangeFillColors;
        this.values = this._layer.slider.ranges;
        this.max = this._layer.slider.maxValue; 
        this.min = this._layer.slider.minValue; 
        this.step = this._layer.slider.step;
        let size = this._layer.style.rangeFillColors.length;
        this.pointerColor = this._layer.style.rangeFillColors[size-1];
        this.style.backgroundColor = this.pointerColor;
    }

    /**
     * Raffraichissement de l'affichage de l'ensemble des pointeurs
     */
     refresh(){
         
        if(this._layer){
            this._setLayerDataToSlider();
            
            // affiche le slider si la couche est affiché et le nbr de quantile > 1  
            if(this._layer.style.display && this._layer.slider.ranges.length > 0){
                this.show();
            }else {
                this.hide();
            }
        }


        this.requestUpdate();
        //refresh the slider background with the last color on the list
        this.style.backgroundColor = this.pointerColor;
        
    }

    /**
     * Mise à jour d'un des pointeurs
     * @param {*} idx index du pointeur
     * @param {*} value nouvelle valeur
     */
	updatePointer(idx, value, pointer){
        super.updatePointer(idx, value, pointer);
        let associatedProperty =  "properties.slider.ranges"
        let event = new CustomEvent('slider-value-change', {detail: { value: this.values , layerId: this._layer.id, property: associatedProperty}});
        this.dispatchEvent(event);
    }

    /**
     * Affichage Masquage du slider 
     */
    toogle(){
        // masquer le slider si le nombre de range est 0
        if(this._layer.slider.ranges.length == 0){
            this.hide();
        }else{
            this.style.visibility = !this.style.visibility || this.style.visibility === "visible" ? "hidden" : "visible";
        }
    }
}

customElements.define('layer-slider', LayerSlide );
export default new LayerSlide;