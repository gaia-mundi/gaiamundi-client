import { html, css, LitElement } from '../../../lib/lit-element.js';

import workspacesService from "../../../shared/service/workspacesService.js";

const fs = require('fs');
const path = require('path');
const { env } = require('../../container/environments.js');

const workspace = env.get("workspace.location");

export const Tag = "indicator-add";

class AddIndicator extends LitElement {

    /** tableau des calculs*/
    calculTypeTab = ["valeur a","somme","(somme/d)%","(a/b)%","[(a-b)/c]%","[S1/S2]%","[(a-b)/S]%"];
    dataSetName;
    loadData = true;
    dataSet= [];
    jsonIndic;

    /**
     * Propriétés et attribut de l'élément
     */
     static get properties() {
        return {
            workspaceName: { attribute: true, type: String },
            calculType: {attribute: false, reflect : true, type: String},
            dataSet: {attribute: false, type: Array}
        };
    }

    firstUpdated(changedProperties) {
        workspacesService.getWorkspaceById(
            workspacesService.queryWorkspaceName(),
            (wk) => {
                this._workspace = wk;
            }
        );
    }

    updated(){
        workspacesService.getWorkspaceById(
            workspacesService.queryWorkspaceName(),
            (wk) => {
                this._workspace = wk;
            }
        );
    }

    display(selectedNode){
        this.labelSelectedNode = selectedNode.label;
    }

    /**
     * 
     * @returns charge la liste des données à calculer
     */
    _loadData() {
        this.dataSetName = this.dataSetName.replace(/[^\wèéòàùì\s]/gi, '').replace(/ /g,'_');
        if(this.dataSetName){
            const datas = JSON.parse(fs.readFileSync(path.resolve(workspace, this.workspaceName, "dataSets", this.dataSetName,"data.json")));
            for(const item in datas.data) {
                if (!['idcarte', 'libel', 'codeINSEE'].includes(item))
                    this.dataSet.push(item);
            }
            this.requestUpdate();
        }
        return this.dataSet;
    }

    /**
     * mise à jour le type de calcul
     * @param {*} e event
     */
     _updateCalculationType(e){
        if(this.loadData){
            this._loadData();
            this.loadData=false;
        }
        this.calculType = e.target.value;
    }

    /**
     * Récupérer et valider les données insérer dans le formulaire de création de couche
     * @returns true si les données sont valide
     */
    _isIndicCreationFormDataValid(){
        //validate calculation type
        let data1=[];
        let data2=[];
        let data3=[];

        let errorMessageList = [];

        if(this.calculType){
            //validation de la selection de donnée
            
            //only list with multiple choices need to be validated select have the first value as default

            switch (this.calculType) {
                case "valeur a":
                    // no validation needed list with the first element as default
                    data1.push(this.shadowRoot.getElementById('firstNumerator-select').value);
                    break;
                case "somme":
                    //get checked elements
                    this.shadowRoot.querySelectorAll(".firstNumerator-Checkbox").forEach( (element) => {
                        if(element.checked){
                            data1.push(element.value);
                        }
                    });

                    //validation : au moin 2 de donnée selectioner
                    if(data1.length < 2){
                        errorMessageList.push("Veuillez selectionner au moin 2 donnée pour le Numérateur 1.")
                    }
                    break;
                case "(somme/d)%":
                        //firstNumerator
                        this.shadowRoot.querySelectorAll(".firstNumerator-Checkbox").forEach( (element) => {
                        if(element.checked){
                            data1.push(element.value);
                        }
                    });

                    //validation : au moin 2 de donnée selectioner
                    if(data1.length < 2){
                        errorMessageList.push("sélectionner aux moins 2 données pour le Numérateur 1.")
                    }

                    //denominateur
                    data3.push(this.shadowRoot.getElementById('denominator-select').value);

                    break;
                case "(a/b)%":
                    //firstNumerator
                    data1.push(this.shadowRoot.getElementById('firstNumerator-select').value);

                    //denominateur
                    data3.push(this.shadowRoot.getElementById('denominator-select').value);
                    break;

                case "[(a-b)/c]%":
                    //firstNumerator
                    data1.push(this.shadowRoot.getElementById('firstNumerator-select').value);

                    //secondNumerator
                    data2.push(this.shadowRoot.getElementById('secondNumerator-select').value);

                    //denominateur
                    data3.push(this.shadowRoot.getElementById('denominator-select').value);
                    break;
                case "[S1/S2]%":
                    //firstNumerator
                    this.shadowRoot.querySelectorAll(".firstNumerator-Checkbox").forEach( (element) => {
                        if(element.checked){
                            data1.push(element.value);
                        }
                    });

                    //validation firstNumerator: au moin 2 de donnée selectioner
                    if(data1.length < 2){
                        errorMessageList.push("Veuillez sélectionner aux moins 2 données pour le Numérateur 1.")
                    }

                    //denominateur
                    this.shadowRoot.querySelectorAll(".denominator-Checkbox").forEach( (element) => {
                        if(element.checked){
                            data3.push(element.value);
                        }
                    });

                    //validation denominateur: au moin 2 de donnée selectioner
                    if(data3.length < 2){
                        errorMessageList.push("Veuillez sélectionner aux moins 2 données pour le Dénominateur.")
                    }
                    break;
                case "[(a-b)/S]%":
                    //firstNumerator
                    data1.push(this.shadowRoot.getElementById('firstNumerator-select').value);

                    //secondNumerator
                    data2.push(this.shadowRoot.getElementById('secondNumerator-select').value);

                    //denominateur
                    this.shadowRoot.querySelectorAll(".denominator-Checkbox").forEach( (element) => {
                        if(element.checked){
                            data3.push(element.value);
                        }
                    });

                    //validation : au moin 2 de donnée selectioner
                    if(data3.length < 2){
                        errorMessageList.push("Veuillez sélectionner aux moins 2 données pour le Dénominateur.")
                    }
                    break;
            }

        }else{
            errorMessageList.push("Veuillez choisir un type de calcul");
        }

        //validation input titre de la couche
        let indicTitle = this.shadowRoot.getElementById('indicTitle').value;

        if(!indicTitle){
            errorMessageList.push("Veuillez rensigner le titre de l'indicateur");
        }

        //recueprer la source de donnée
        let dataSource = this.shadowRoot.getElementById('dataSource').value;

        //recueprer le liblele long
        let indicDescription = this.shadowRoot.getElementById('indicDescription').value;

        //  envoyer les message d'erreur
        if(errorMessageList.length != 0){
            let str = "<ul>";
            for (let i = 0; i < errorMessageList.length; i++) {
                str += "<li>"+ errorMessageList[i] + "</li>";
            }
            str += "</ul>";
            this.shadowRoot.getElementById('validation-message').innerHTML = str;

            return false;
        }else{
            // affectation des données
            this.data1 = data1;
            this.data2 = data2;
            this.data3 = data3;
            this.indicTitle = indicTitle;
            this.dataSource = dataSource;
            this.indicDescription = indicDescription;
            return true;
        }

    }

    /**
     * Vérification de la conformité du formulaire, puis création du json de l'indicateur et appel de la fonction pour le créer
     */
    _createIndicInfo() {
        this.updated();
        this.dataSetName = this.dataSetName.replace(/[^\wèéòàùì\s]/gi, '').replace(/ /g,'_');
        let formValidation = this._isIndicCreationFormDataValid();
        
        if(formValidation){
            const regexp = /[^A-zÀ-ÿ0-9]/g;
            let name = this.indicTitle.replaceAll(regexp, "").toLowerCase();
            this.jsonIndic = {
                "title": this.indicTitle,
                "id": name,
                "dataSetFile": "dataSets\\"+ this.dataSetName + "\\data.json",
                "calculationType": this.calculType,
                "usedData": {
                    "firstNumerator": this.data1,
                    "secondNumerator": this.data2,
                    "denominator": this.data3,
                },
                "dataSource": this.dataSource,
                "longTitle": this.indicDescription
            }
            this.fetchJsonInfoCreate(this._workspace.indicators, this.labelSelectedNode, this.jsonIndic)
        }
    }

    /**
     * Ajout d'un indicateur à un dossier
     * @param {*} nodes noeud de départ
     * @param {*} id label du dossier dans lequel on veut ajouter
     * @param {*} json indicateur ajouté au workspace
     * @returns 
     */
    fetchJsonInfoCreate(nodes, id, json){
        let size = nodes.length;
        let found;
        for (let i = 0; i < size && !found; i++){
            let node = nodes[i];
            if(id==="Indicateurs"){
                nodes.push([json]);
                workspacesService.saveWorkspace(this._workspace);
                found = true;
            } else {
                if(node.label === id) {
                    node.indicators.push(json);
                    workspacesService.saveWorkspace(this._workspace);
                    found = true;
                } else {
                    if(node.children && !found){
                        found = this.fetchJsonInfoCreate(node.children, id, json);
                    };
                }
            }
        };
        return found;
    }

    /**
     * renvoi des selecteurs de données selon le type de clacul
     */
    _renderDataSelectors(){
        if(this.calculType){
            switch (this.calculType) {
                case "valeur a":
                    return this._renderList('firstNumerator','Numérateur 1');
                
                case "somme":
                    return this._renderMultiSelectionList('firstNumerator','Numérateur 1');

                case "(somme/d)%":
                    return html`
                        ${this._renderMultiSelectionList('firstNumerator','Numérateur 1')}
                        ${this._renderList('denominator','Dénominateur')}
                        `
                case "(a/b)%":
                    return html`
                        ${this._renderList('firstNumerator','Numérateur 1')}
                        ${this._renderList('denominator','Dénominateur')}
                        `
                case "[(a-b)/c]%":
                    return html`
                        ${this._renderList('firstNumerator','Numérateur 1')}
                        ${this._renderList('secondNumerator','Numérateur 2')}
                        ${this._renderList('denominator','Dénominateur')}
                    `
                case "[S1/S2]%":
                    return html`
                        ${this._renderMultiSelectionList('firstNumerator','Numérateur 1')}
                        ${this._renderMultiSelectionList('denominator','Dénominateur')}
                    `
                case "[(a-b)/S]%":
                    return html`
                        ${this._renderList('firstNumerator','Numérateur 1')}
                        ${this._renderList('secondNumerator','Numérateur 2')}
                        ${this._renderMultiSelectionList('denominator','Dénominateur')}
                    `
            }
        }
    }

    /**
     *  genère un input de type select
     * @param {*} name  un partie du nom de l'attribut class de l'input select
     * @param {*} label le label de l'input
     */
    _renderList(name, label){
        return html`
        <div class="selectBox" >
            <label for=${name}> ${label}</label>
            <select id='${name}-select' name=${name}>
                ${this.dataSet.map(data =>
                    html`
                        <option value="${data}">${data}</option>
                    `
                )}
            </select>
        </div>
        `
    }

    /**
     * gener un input de selction multiple
     * @param {*} name  un partie de la valeur de l'attibut class de l'input select
     * @param {*} label le label de l'input
     */
    _renderMultiSelectionList(name, label){
        return html`
            <div class="selectBox" >
                <label > ${label}</label>
                <div class="dorpDown-element" @click=${()=>this._toggleListCheckBoxes(name)}>
                    <div class='text'> selectionner des colonnes </div> <fa-icon icon="angle-down" ></fa-icon>
                </div>
                <div id='${name}-dropList' class="multipleChoiceDropList " style='display:none'>
                    ${this.dataSet.map(element =>
                        html`
                            <div>
                                <input type="checkbox" name="histo-data1" class="${name}-Checkbox" value="${element}" >${element}</input>
                            </div>
                        `
                    )}
                </div>            
            </div>
        `
    }

    /**
     * afficher/cacher le MultiSelectionList
     * @param {*} name une partie de la valeur de l'attribut class de MultiSelectionList
     */
    _toggleListCheckBoxes(name){
        let dropList = this.shadowRoot.getElementById(name+'-dropList');
        let display = dropList.style.display;
        dropList.style.display = display == 'none' ? 'block': 'none'; 

    }

    /** Affichage */
    render(){
        return html`
        <div>            
            <form>
                <label>Choisissez le type de calcul : </label>
                <div>
                    ${this.calculTypeTab.map((element, i) =>
                    html`
                        <label for="calculType">${element}</label>
                        <input type="radio" id="${i}" name="calculType" value="${element}" @change=${e=>{this._updateCalculationType(e)}}>
                        <br />
                    `)}
                    </div>
                <div>
                    ${this._renderDataSelectors()}
                </div>
                <div>
                    <label for="indicTitle">Titre de l'indicateur</label>
                    <input type="text" id="indicTitle" name="indicTitle" value="" placeholder="Titre de l'indicateur">
                </div>

                <div>
                    <label for="dataSource">Source des données</label>
                    <input type="text" id="dataSource" name="dataSource" value="" placeholder="CAF 69 2017">
                </div>

                <div>
                    <label for="indicDescription">Libellé long</label>
                    <input type="text" id="indicDescription" name="indicDescription" value="" placeholder="Libellé long">
                </div>
            </form>
            <div id='validation-message'></div>
            <button @click="${this._createIndicInfo}">Créer</button>
        </div>
        `;
    }

    /** CSS spécifique au composant */
    static get styles() {
        return css`        
        .multipleChoiceDropList {
            border: 1px #dadada solid;
            height: 300px;
            overflow: scroll;
            position: absolute;
            background-color: white;
            left: 100px;
            z-index: 1;
        }

        .selectBox .dorpDown-element fa-icon{
            display: inline;

        }
        .selectBox {
            position: relative;
            margin-bottom: 5px;
            margin-top: 5px;
        }

        .selectBox label{
            display: inline;
        }
        .selectBox select{
            display: inline;
        }
        .selectBox .dorpDown-element{
            display: inline;
            border: solid;
            border-width: 1px;
            padding-left: 2px;
            padding-right: 2px;
        }

        .dorpDown-element:hover {
            background-color: #1e90ff;

        }

        .selectBox .dorpDown-element .text{
            display: inline;
        }

        #validation-message{
            background-color: #efcccc;
        }
        
        .dataCheckbox {
        width: fit-content;
        }
        `;
    }
}

customElements.define('indicator-add', AddIndicator);
export default AddIndicator;