import { html, css, LitElement } from '../../../lib/lit-element.js';
import workspacesService from "../../../shared/service/workspacesService.js";
import dataSetService from "../../../shared/service/data-set-service.js";
import "../indicators/multipleDropDownCheckbox.js"

const path = require('path');
const fs = require('fs');
const { env } = require('../../container/environments.js');
const workspace = env.get("workspace.location");

export const Tag = "indicator-edit";

class EditIndicator extends LitElement {

    dataSet = [];
    /** propriétés de l'indicateur */
    dataSetInfo = [];
    /** Est-ce que le bouton radio est plein ? */
    radioChecked=true;
    /** tableau des calculs*/
    calculTypeTab = ["valeur a","somme","(somme/d)%","(a/b)%","[(a-b)/c]%","[S1/S2]%","[(a-b)/S]%"];
    loadData = true;
    //Numérateur et dénominateur multiple
    firstNumeratorMult;
    secondNumeratorMult;
    denominatorMult;
    //Numérateur et dénominateur unique
    firstNumerator;
    secondNumerator;
    denominator;

    data1;
    data2;
    data3;

    _workspace;

    static get properties() {
        return {
            setOfDatas: { attribute: false},
            dataSetName: { attribute: false},
            calculType: { attribute: false }
        };
    }

    displayIndicator(selectIndicatorProperties){
        this.dataSetName = selectIndicatorProperties.dataSetFile;
        this.dataSetInfo = selectIndicatorProperties;
        this.setOfDatas = dataSetService.getDataSetInfo(this._workspace.name, this.dataSetName);
        this.calculType = this.dataSetInfo.calculationType;
        this.requestUpdate();
    }

    firstUpdated(changedProperties){
        workspacesService.getWorkspaceById(
            workspacesService.queryWorkspaceName(),
            (wk) => {
                this._workspace = wk;
            }
        );
    }

    /**
     * Aprés la mise à jour du DOM on charge les données
     *
     * @param {*} changedProperties list des propriétés mise à jour
     * @memberof EditIndicator
     */
    updated(changedProperties) {
        this.firstNumeratorMult = this.shadowRoot.getElementById("firstNumerator");
        this.secondNumeratorMult = this.shadowRoot.getElementById("secondNumerator");
        this.denominatorMult = this.shadowRoot.getElementById("denominator");
        this.firstNumerator = this.shadowRoot.getElementById("firstNumerator-select");
        this.secondNumerator = this.shadowRoot.getElementById("secondNumerator-select");
        this.denominator = this.shadowRoot.getElementById("denominator-select");
        this.validationMsg = this.shadowRoot.getElementById('validation-message');
    }

     /**
     * 
     * @returns charge la liste des données à calculer
     */
    _loadData() {
        if(this.dataSetName) {
            const datas = JSON.parse(fs.readFileSync(path.resolve(workspace, this._workspace.name, this.dataSetName)));
        
            for(const item in datas.data) {
                if (!['idcarte', 'libel', 'codeINSEE'].includes(item)){
                    this.dataSet.push(item);
                }
            }
            this.requestUpdate();
        }
        return this.dataSet;
    }

/**
 * renvoi des selecteurs de données selon le type de calcul
 * @param {*} calculation type de calcul
 */
    _renderDataSelectors(calculation){
        if(this.loadData){
            this._loadData();
            this.loadData=false;
        }
        if(calculation){            
            switch (calculation) {
                case "valeur a":
                    return this._renderList('firstNumerator','Numérateur 1', this.dataSetInfo.usedData.firstNumerator); 

                case "somme":
                    return this._renderMultiSelectionList('firstNumerator','Numérateur 1', this.dataSetInfo.usedData.firstNumerator, this.dataSet);

                case "(somme/d)%":
                    return html`
                        ${this._renderMultiSelectionList('firstNumerator','Numérateur 1', this.dataSetInfo.usedData.firstNumerator, this.dataSet)}
                        ${this._renderList('denominator','Dénominateur', this.dataSetInfo.usedData.denominator)}
                        `
                case "(a/b)%":
                    return html`
                        ${this._renderList('firstNumerator','Numérateur 1', this.dataSetInfo.usedData.firstNumerator)}
                        ${this._renderList('denominator','Dénominateur', this.dataSetInfo.usedData.denominator)}
                        `
                case "[(a-b)/c]%":
                    return html`
                        ${this._renderList('firstNumerator','Numérateur 1', this.dataSetInfo.usedData.firstNumerator)}
                        ${this._renderList('secondNumerator','Numérateur 2', this.dataSetInfo.usedData.secondNumerator)}
                        ${this._renderList('denominator','Dénominateur', this.dataSetInfo.usedData.denominator)}
                    `
                case "[S1/S2]%":
                    return html`
                        ${this._renderMultiSelectionList('firstNumerator','Numérateur 1', this.dataSetInfo.usedData.firstNumerator, this.dataSet)}
                        ${this._renderMultiSelectionList('denominator','Dénominateur', this.dataSetInfo.usedData.denominator, this.dataSet)}
                    `
                case "[(a-b)/S]%":
                    return html`
                        ${this._renderList('firstNumerator','Numérateur 1', this.dataSetInfo.usedData.firstNumerator)}
                        ${this._renderList('secondNumerator','Numérateur 2', this.dataSetInfo.usedData.secondNumerator)}
                        ${this._renderMultiSelectionList('denominator','Dénominateur', this.dataSetInfo.usedData.denominator, this.dataSet)}
                    `
            }
        } else {
            return html `${this._renderMultiSelectionList('histogramColones','',this.dataSetInfo.usedData, this.dataSet)}`
        }
    }

    /**
     *  genère un input de type select
     * @param {*} name  une partie du nom de l'attibut class de l'input select
     * @param {*} label le label de l'input
     * @param {*} value valeur enregistrée
     */
    _renderList(name, label, value){
        return html`
        <div class="selectBox" >
            <label for=${name}> ${label}</label>
            <select id='${name}-select' name=${name}>
                ${this.dataSet.map(data =>
                    html`
                        ${value==data ?
                            html `<option selected value="${data}">${data}</option>`
                            : html `<option value="${data}">${data}</option>`
                        }
                    `
                )}
            </select>
        </div>
        `
    }

    /**
     * genère un input de selection multiple
     * @param {*} name  une partie de la valeur de l'attibut class de l'input select
     * @param {*} label le label de l'input
     * @param {*} value valeur(s) enregistrée(s)
     * @param {*} dataSet jeu de données
     */
     _renderMultiSelectionList(name, label, value, dataSet){
        return html`
            <checkbox-dropdown id="${name}" 
                               name="${name}"
                               label="${label}"
                               value="${value}"
                               dataSet="${dataSet}"></checkbox-dropdown>
        `
    }

    /**
     * on assigne une valeur à calculType et on détermine si le radio bouton est coché ou non
     * @param {*} e element cible qui va déterminer la valeur de calculType
     * @param {*} bool booleen
     */

    updateCalculType(e, bool){
        this.calculType = e.target.value;
        this.radioChecked = bool;
        this.requestUpdate();
    }

    /**
     * Récupérer et valider les données insérées dans le formulaire
     * @returns true si les données sont valides
     */
     _isindicatorCreationFormDataValid(){
        //validate calculation type
        let data1=[];
        let data2=[];
        let data3=[];

        let errorMessageList = [];

        if(this.calculType){
            //validation de la selection de donnée
            
            //only list with multiple choices need to be validated select have the first value as default

            switch (this.calculType) {
                case "valeur a":
                    // no validation needed list with the first element as default
                    data1.push(this.firstNumerator.value);
                    break;
                case "somme":
                    //get checked elements
                    for(let i = 0; i<this.firstNumeratorMult.tab.length; i++){
                        data1.push(this.firstNumeratorMult.tab[i]);
                    }

                    //validation : au moins 2 données selectionnées
                    if(data1.length < 2){
                        errorMessageList.push("Veuillez selectionner au moins 2 données pour le Numérateur 1.")
                    }
                    break;
                case "(somme/d)%":
                     //firstNumerator
                    for(let i = 0; i<this.firstNumeratorMult.tab.length; i++){
                        data1.push(this.firstNumeratorMult.tab[i]);
                    }

                    //validation : au moins 2 données selectionnées
                    if(data1.length < 2){
                        errorMessageList.push("sélectionner au moins 2 données pour le Numérateur 1.")
                    }

                    //denominateur
                    data3.push(this.denominator.value);

                    break;
                case "(a/b)%":
                    //firstNumerator
                    data1.push(this.firstNumerator.value);

                    //denominateur
                    data3.push(this.denominator.value);
                    break;

                case "[(a-b)/c]%":
                    //firstNumerator
                    data1.push(this.firstNumerator.value);

                    //secondNumerator
                    data2.push(this.secondNumerator.value);

                    //denominateur
                    data3.push(this.denominator.value);
                    break;
                case "[S1/S2]%":
                    //firstNumerator
                    for(let i = 0; i<this.firstNumeratorMult.tab.length; i++){
                        data1.push(this.firstNumeratorMult.tab[i]);
                    }

                    //validation firstNumerator: au moins 2 données selectionnées
                    if(data1.length < 2){
                        errorMessageList.push("Veuillez sélectionner aux moins 2 données pour le Numérateur 1.")
                    }

                    //denominateur
                    for(let i = 0; i<this.denominatorMult.tab.length; i++){
                        data3.push(this.denominatorMult.tab[i]);
                    }

                    //validation denominateur: au moins 2 données selectionnées
                    if(data3.length < 2){
                        errorMessageList.push("Veuillez sélectionner aux moins 2 données pour le Dénominateur.")
                    }
                    break;
                case "[(a-b)/S]%":
                    //firstNumerator
                    data1.push(this.firstNumerator.value);

                    //secondNumerator
                    data2.push(this.secondNumerator.value);

                    //denominateur
                    for(let i = 0; i<this.denominatorMult.tab.length; i++){
                        data3.push(this.denominatorMult.tab[i]);
                    }

                    //validation : au moins 2 données selectionnées
                    if(data3.length < 2){
                        errorMessageList.push("Veuillez sélectionner aux moins 2 données pour le Dénominateur.")
                    }
                    break;
            }
            


        }else{
            errorMessageList.push("Veuillez choisir un type de calcul");
        }

        //  envoyer les message d'erreur
        if(errorMessageList.length != 0){
            let str = "<ul>";
            for (let i = 0; i < errorMessageList.length; i++) {
                str += "<li>"+ errorMessageList[i] + "</li>";
            }
            str += "</ul>";
            this.validationMsg.innerHTML = str;
            setTimeout(() => {this.validationMsg.innerHTML = "";}, 5000);


            return false;
        }else{
            // affectation des données
            this.data1 = data1;
            this.data2 = data2;
            this.data3 = data3;
            return true;
        }

    }

    /**
     * On parcourt l'arbre des indicateurs pour retourner l'indicateur souhaité
     * @param {*} indicator l'arborescence à parcourir
     * @param {*} id identifiant de l'indicateur souhaité
     */

    fetchJsonInfo(nodes, id){
        let size = nodes.length;
        let res;
        for (let i = 0; i < size && !res; i++){
            let node = nodes[i];
            if(node.indicators){
                node.indicators.forEach(indIc => {
                    if(indIc.id === id){
                        res =  indIc;
                    }
                        
                })
            }
            if(!res && node.children){
                 res = this.fetchJsonInfo(node.children,id);
            };
        };
        return res;
    }

    /**
     * On met à jour nos données après vérification de leur format
     */
    updateJson(){
        let formValidation = this._isindicatorCreationFormDataValid();
        if(formValidation){
            let indicator = this.fetchJsonInfo(this._workspace.indicators, this.dataSetInfo.id);
            indicator.calculationType=this.calculType;
            indicator.usedData.firstNumerator = this.data1;
            indicator.usedData.secondNumerator = this.data2;
            indicator.usedData.denominator = this.data3;
            workspacesService.saveWorkspace(this._workspace);
            this.validationMsg.style.backgroundColor = "green";
            this.validationMsg.innerHTML = "Modifications enregistrées";
            setTimeout(() => {this.validationMsg.style.backgroundColor = "";
            this.validationMsg.innerHTML = "";}, 2000);
        }
        let event = new CustomEvent('node-modified');
        this.dispatchEvent(event);
        this.requestUpdate();
        this._renderList();
    }

    /** Affichage */
    render(){
        return html `
            ${this.setOfDatas ? 
                html `
                <div>
                    <h4>Jeu de données de l'indicateur :</h4>
                    <p>${this.setOfDatas ? this.setOfDatas.title : ""}</p>
                </div>
                <div>
                    <h4>Modification de l'indicateur :</h4>
                    ${this.dataSetInfo.calculationType ?
                        html `
                            <form>
                                <label>Choisissez le type de calcul : </label>
                                    <div>
                                        ${this.calculTypeTab.map((element, i) =>
                                        html`
                                            ${this.dataSetInfo.calculationType === element ?
                                                html `
                                                    <label for="calculType">${element}</label>
                                                    <input type="radio" id="${i}" name="calculType" value="${element}" @change="${e => {this.updateCalculType(e, true)}}" checked>
                                                    <br />

                                                `
                                                : html `
                                                    <label for="calculType">${element}</label>
                                                    <input type="radio" id="${i}" name="calculType" value="${element}" @click="${e => {this.updateCalculType(e, false) }}">
                                                    <br />
                                                `
                                            }
                                        `)}
                                    </div>
                                    <div>
                                        ${this._renderDataSelectors(this.radioChecked ? this.dataSetInfo.calculationType : this.calculType)}
                                    </div>
                            </form>
                            <div id='validation-message'></div>
                            <button @click="${e=>{this.updateJson()}}">Modifier</button>`
                        : html `
                            ${this.setOfDatas ? 
                                html `
                                    <form>
                                        <label>Choisissez les colonnes de l'histogramme : </label>
                
                                        ${this._renderDataSelectors()}

                                        <div>
                                            <label for="histogramTitle">Titre de la couche histogramme</label>
                                            ${this.dataSetInfo.title ? 
                                                html`
                                                    <input type="text" id="histogramTitle" name="histogramTitle" value="${this.dataSetInfo.title}" placeholder="titre de l'histogramme">
                                                `
                                                : html`
                                                    <input type="text" id="histogramTitle" name="histogramTitle" value="" placeholder="titre de l'histogramme ">
                                                `
                                            }
                                        </div>

                                        <div>
                                            <label for="histogramDataSource">Source des données</label>
                                            ${this.dataSetInfo.dataSource ? 
                                                html `
                                                    <input type="text" id="histogramDataSource" name="histogramDataSource" value="${this.dataSetInfo.dataSource}" placeholder="CAF 69 2017">
                                                `
                                                : html `
                                                    <input type="text" id="histogramDataSource" name="histogramDataSource" value="" placeholder="CAF 69 2017">
                                                `
                                            } 
                                        </div>

                                        <div>
                                            <label for="histogramDescription">Libellé long histogramme</label>
                                            ${this.dataSetInfo.longTitle ? 
                                                html `
                                                    <input type="text" id="histogramDescription" name="histogramDescription" value="${this.dataSetInfo.longTitle}" placeholder="Libellé long">
                                                `
                                                : html `
                                                    <input type="text" id="histogramDescription" name="histogramDescription" value="" placeholder="Libellé long">
                                                `
                                            }
                                        </div>
                                    </form>
                                    <div id='validation-message-histogram'></div>
                                    <button>Modifier histogramme</button>
                                `
                                : html ``
                            }
                        `
                    }
                </div>`
                : html ``
            }
        `
    }

    /** CSS spécifique au composant */
    static get styles() {
        return css`
        .multipleChoiceDropList {
            border: 1px #dadada solid;
            height: 300px;
            overflow: scroll;
            position: absolute;
            background-color: white;
            left: 100px;
            z-index: 1;
        }

        .selectBox .dorpDown-element fa-icon{
            display: inline;

        }
        .selectBox {
            position: relative;
            margin-bottom: 5px;
            margin-top: 5px;
        }

        .selectBox label{
            display: inline;
        }
        .selectBox select{
            display: inline;
        }
        .selectBox .dorpDown-element{
            display: inline;
            border: solid;
            border-width: 1px;
            padding-left: 2px;
            padding-right: 2px;
            background-color: #ffffff;
        }

        .dorpDown-element:hover {
            background-color: #1e90ff;

        }

        .selectBox .dorpDown-element .text{
            display: inline;
            background-color: #efcccc;
        }
        
        .dataCheckbox {
        width: fit-content;
        }

        #validation-message, #validation-message-histogram{
            background-color: #efcccc;
        }
        `;
    }
}

customElements.define('indicator-edit', EditIndicator);
export default EditIndicator;