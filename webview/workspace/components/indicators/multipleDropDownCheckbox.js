import { html, css, LitElement } from '../../../lib/lit-element.js';

export const Tag = "checkbox-dropdown";

class DropDown extends LitElement {

    var;
    tab=[];
    tabFirst=[];
    tabSecond=[];
    tabDenominator=[];

    static get properties() {
        return {
            label: { attribute: true },
            name: { attribute: true },
            value: { attribute: true },
            dataSet: { attribute: true }
        };
    }

    /**
     * Aprés la mise à jour du DOM on charge les données
     *
     * @param {*} changedProperties list des propriétés mise à jour
     * @memberof EditIndicator
     */
    firstUpdated(changedProperties) {
    }
    
     /**
     * afficher/cacher le MultiSelectionList
     * @param {*} name une partie de la valeur de l'attribut class de MultiSelectionList
     */
    _toggleListCheckBoxes(name){
        let dropList = this.shadowRoot.getElementById(name+'-dropList');
        let display = dropList.style.display;
        dropList.style.display = display == 'none' ? 'block': 'none'; 
    }

    /** Liste des checkbox cochées */
    arrayFunc(e){
        if(!this.tab.includes(e.target.value)){
            this.tab.push(e.target.value);
        } else {
            for(let i = 0; i<this.tab.length; i++){
                if(this.tab[i] == e.target.value){
                    this.tab.splice(i,1);
                }
            }
        }
    }

    /** Drop down menu */
    dropDownFunc(e) {
        let elems= this.value.split(',');
        for (let i = 0; i<=elems.length; i++) {
            if (elems[i] == e) {
                if(!this.tab.includes(e)){
                    this.tab.push(e);
                }
                return html`<input type="checkbox" name="histo-data${i}" class="${this.name}-Checkbox" value="${e}" @click="${e=>{this.arrayFunc(e, this.name)}}" checked>${e}</input>`;
            } else {
                this.var = html`<input type="checkbox" name="histo-data${i}" class="${this.name}-Checkbox" value="${e}" @click="${e=>{this.arrayFunc(e, this.name)}}">${e}</input>`;
            }
        }
        return this.var;
    }

    /** Affichage */
    render(){
        return html`
        <div class="selectBox" >
            <label > ${this.label}</label>
            <div class="dorpDown-element" @click="${()=>this._toggleListCheckBoxes(this.name)}">
                <div class='text'> selectionner des colonnes </div> <fa-icon icon="angle-down" ></fa-icon>
            </div>
            <div id='${this.name}-dropList' class="multipleChoiceDropList " style='display:none'>
                ${this.dataSet.split(',').map(element =>
                    html`
                        <div>
                            ${this.dropDownFunc(element)}
                        </div>
                    `
                )}
            </div>            
        </div>
        `
    }

    /** CSS spécifique au composant */
    static get styles() {
        return css`
        .multipleChoiceDropList {
            border: 1px #dadada solid;
            height: 300px;
            overflow: scroll;
            position: absolute;
            background-color: white;
            left: 100px;
            z-index: 1;
        }

        .selectBox .dorpDown-element fa-icon{
            display: inline;

        }
        .selectBox {
            position: relative;
            margin-bottom: 5px;
            margin-top: 5px;
        }

        .selectBox label{
            display: inline;
        }
        .selectBox select{
            display: inline;
        }
        .selectBox .dorpDown-element{
            display: inline;
            border: solid;
            border-width: 1px;
            padding-left: 2px;
            padding-right: 2px;
        }

        .dorpDown-element:hover {
            background-color: #1e90ff;

        }

        .selectBox .dorpDown-element .text{
            display: inline;
        }
        
        .dataCheckbox {
        width: fit-content;
        }
        `;
    }
    
}

customElements.define('checkbox-dropdown', DropDown);
export default DropDown;