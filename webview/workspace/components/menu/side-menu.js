import { LitElement, html, css, unsafeCSS } from '../../../lib/lit-element.js';
import {unsafeHTML} from '../../../lib/unsafe-html.js';
import "../../../shared/component/icon/faicon.js"
import "../../../shared/component/icon/fabutton.js"
import workspacesService from "../../../shared/service/workspacesService.js";
const { env } = require('../../container/environments.js');
import "./side-menu-items.js";
import "./editor/editor.js";
import IconTypeConstants from '../../../shared/util/IconTypeConstants.js';
import layerService from '../../../shared/service/layerService.js';

class SideMenuWorkspace extends LitElement{

    _closed = false;
    _closeButton;
    _tabs;
    _layers = [];
    _background;
    _activeLayer;

    static get properties() {
        return {
          "min-width": {type: String, attribute: true},
          workspaceName: {type: String, attribute: false},
        };
      }

    constructor() {
        super();
        this.workspaceName = workspacesService.queryWorkspaceName();
        this.dataSetName = workspacesService.queryDataSetName();
    }
    /**
     * Identification du bouton de fermeture/ouverture
     */
    firstUpdated(){
        this._closeButton = this.shadowRoot.getElementById("closeButton");
        let tabContainer =  this.shadowRoot.getElementById("tabs");
        this._tabs = tabContainer.children;
        workspacesService.getWorkspaceById(
            workspacesService.queryWorkspaceName(),
            (wk) => {
                this._workspace = wk;
            }
        );
    }

    /**
     * Activation du layer actif
     *
     * @memberof SideMenuWorkspace
     */
    set activeLayer(layerId){
        //Masquage des menus non associé au menu selectionné
        let displayItem = (id) => {
            let menuItem = this.shadowRoot.getElementById("item-"  +id);
            if(layerId === id){
                menuItem.show();
                menuItem.parentElement.style.flexGrow = 1;
            }else{
                menuItem.hide();
                menuItem.parentElement.style.flexGrow = "unset";
            }
        }
        displayItem("background");
        this._layers.forEach( layer => { displayItem(layer.id);});

        //émission d'un évènement de sélection
        let event; 
        if(layerId === "background"){
            this._activeLayer = this._background;
        }else{
            this._activeLayer = this._layers.find(l => l.id === layerId);
        }
        event = new CustomEvent('activate-layer',{ detail: { layer : this._activeLayer }});
        this.dispatchEvent(event);
    }

    /**
     * Acces à la définition du background
     */
    set backgroundStyle(style){
        this._background.style = style;
        this.requestUpdate();
    }
    /**
     * Récupération de la couche active
     *
     * @memberof SideMenuWorkspace
     */
    get activeLayer(){
        return this._activeLayer;
    }

    /**
     * Récupération du layer par son id
     * @param {*} id 
     * @returns 
     */
    getLayer(id){
        return this._layers.find(l => l.id === id);
    }

    /**
     * A chaque affichage il faut reinitialiser les sous menus
     * @param {*} changedProperties 
     */
    updated(changedProperties){
        let bubbleUpdateLayers = (e) => this.dispatchEvent(new CustomEvent(e.type, {detail : e.detail}));
        let menuItem = this.shadowRoot.getElementById("item-background");
        menuItem.layer = this._background;
        menuItem.addEventListener("update-layer", bubbleUpdateLayers);
        this._layers.forEach( layer => {
            let menuItem = this.shadowRoot.getElementById("item-" + layer.id);
            menuItem.layer = layer;
            menuItem.addEventListener("update-layer", (e)=>{
                bubbleUpdateLayers(e);
                if(env.get("workspace.user.profile") !== "basic"){
                    this.requestUpdate();
                }          
            });
            

        });
        this.activeLayer = "background";
        this.layers = this.shadowRoot.getElementById("layers");
    }

    /**
     * Définition de l'ordre d'affichage et du status
     * @param {*} configuration 
     */
    setDisplayConfiguration(configuration){
        this._layers = configuration.map(conf => {
                const layer = this.getLayer(conf.id);
                layer.style.display = conf.display;
                return layer;
            });
        this.requestUpdate();
    }

    /**
     * monter ou desendre une couche d'un nombre de pas  
     * @param {*} layerId l'id de la couche
     * @param {*} gap nombre de pas
     */
    moveLayer(layer, gap){
        
        // monter ou desendre la couche dans la liste courante (le methode move to back et front le fait juste pour la liste dans le wordkspace page)
        let updateCurrentLayer = (upOrDown) => {
            const pos = this._layers.findIndex(layerEle => layerEle.id == layer.id);
            const res = pos + upOrDown >= 0 && pos +upOrDown < this._layers.length
            if(res){
                this._layers.splice(pos, 1);
                this._layers.splice(pos + upOrDown, 0, layer);
            }
            return res;
        }
        // monter ou desendre la couche au niveau de la carte, legend via un event
        if(gap>0){
            for (let i = 0; i < gap; i++) {
                if(updateCurrentLayer(-1)){ this.moveToBack(layer.id)} 
            }
        }else if (gap<0){
            for (let i = 0; i < Math.abs(gap); i++) {
                if(updateCurrentLayer(1)){ this.moveToFront(layer.id)}
            }
        }
        
    }

    displayTab(tabIndex){
        for (let idx = 0; idx < this._tabs.length; idx++) {
            if(idx === tabIndex){
                this._tabs[idx].classList.add("active");
            }else{
                this._tabs[idx].classList.remove("active");
            }
        }
    }

    /**
     * Définition du menu correspondant au fond de carte
     * @param {*} background 
     */
    setBackground(background){
        this._background = background;
        this._background.id="background";
        this.requestUpdate();
    }

    /**
     * Ajout d'une couche dans le menu
     * @param {*} layer 
     */
    addLayer(layer){
        this._layers.push(layer);
        this.requestUpdate();
    }

    /**
     * Suppression de l'ensemble des layers
     */
    cleanLayers(){
        this._layers.length = 0;
        this.requestUpdate();
    }

    /**
     * Fermeture et ouverture du menu
     * Déclenche un évènement
     */
    toogle(){
       let event
       if(this._closed){
            event = new CustomEvent('sidemenu-open');
       }else{
            event = new CustomEvent('sidemenu-close');
       }

       this._closed = !this._closed;
       this._closeButton.setAttribute("icon",this. _closed ?  "chevron-left" : "chevron-right");
       this.dispatchEvent(event);
    }

    /**
     * Emission d'un évenement de masquage affichage des couche
     * @param {*} layer la couche
     * @param {*} disableEvent desactivation de l'émission d'évènement
     */
    toggleLayer(layer,disableEvent){
        layer.style.display = !layer.style.display;
        //envoyer un event pour afficher la couche target
        if(!disableEvent){
            let event = new CustomEvent('toggle-layer',{ detail: { type : layer.type, id : layer.id }});
            this.dispatchEvent(event);
        }

        //dans le cas d'un contexte d'utilisateur basique c'est le workspace qui se charge de tout organiser
        if(env.get("workspace.user.profile") !== "basic"){
            this.requestUpdate();
        }
    }

    /**
     * Déplace un couche vers l'avant plan
     * @param {*} id identifiant de la couche
     */
     moveToFront(id){
        let event = new CustomEvent('move-layer-back',{ detail: { id : id }});
        this.dispatchEvent(event);
        this.requestUpdate();
    }

     /**
     * Déplace un couche vers l'arrière plan
     * @param {*} id identifiant de la couche
     */
    moveToBack(id){
        let event = new CustomEvent('move-layer-front',{ detail: { id : id }});
        this.dispatchEvent(event);
        this.requestUpdate();
    }

    /**
     * recuperer les noms d'icone qui correspond à chaque type de couche
     * @param {*} layer 
     * @returns 
     */
    getIconNameForType(layer){
        if(layer.type == 'histogram'){
            return "chart-simple";
        }else if (layer.type == 'edge'){
            return "draw-polygon";
        }else if (layer.type == 'data'){
            if(layer.style.iconType == "coloredZones"){
                return "fill";
            }else if(layer.style.iconType == "circle"){
                return "circle";
            }else if(layer.style.iconType == "square"){
                return "square";
            }
        }
    }

    displayLayer(){
        if(this.layers.style.display === "none"){
            this.layers.style.display = "block";
        } else {
            this.layers.style.display = "none";
        }
    }
    /**
     * Affichage d'un layer
     * @param {*} layer 
     * @returns 
     */
    renderLayer(layer){
        //let icon = layer.style.display ? "eye" : "eye-slash";
        let layerHide = layer.style.display ? "" : "layer-hide";
        return html`
        <div class="layer">
            <div class="layer-head ${layerHide}">
                <div class="licon">
                   <fa-icon icon="${this.getIconNameForType(layer)}" title="${layer.title}" @click="${(e) => { this.toggleLayer(layer);}}"></fa-icon>
                </div>
                <div class="title" @click="${(e) => { this.activeLayer = layer.id}}">
                    ${layer.title}
                </div>
                ${env.get("workspace.user.profile") !== "basic" ?
                    html`<div class="ricon">
                        <fa-icon icon="arrow-up"  @click="${(e) => { this.moveLayer( layer,1); }}"></fa-icon>
                        <fa-icon icon="arrow-down" @click="${(e) => { this.moveLayer( layer,-1); }}"></fa-icon>
                    </div>`
                    : html ``}
            </div>
            ${unsafeHTML(`<workspace-sidemenu-item-${layer.type} id="item-${layer.id}"></workspace-sidemenu-item-${layer.type}>`)}
        </div>`;
    }
    
    /** Affichage */
    render(){
        return html`
            <div class="container" style="width:${this["min-width"]}">
                <fa-button id="closeButton" @click="${this.toogle}" icon="chevron-right" alt title="Ouverture/Fermeture du menu"></fa-button>
                <fa-button icon="chart-bar" alt title="Gestion des couches et données" @click="${(e) => { this.displayTab(0);}}"></fa-button>
                <fa-button icon="book-bookmark" alt title="Définition du scénario" @click="${(e) => { this.displayTab(1);}}"></fa-button>
                <div id="tabs" class="tabs">
                    <div id="tabLayer" class="tab active">
                    <div class="main-item clickable" title="Cliquer pour gérer les données" onclick="location.href='./data-settings.html?workspaceName=${this.workspaceName}'"><fa-icon icon="gears"></fa-icon>Paramétrage</div>    
                    <div class="main-item clickable" @click="${e=>{this.displayLayer()}}"><fa-icon icon="chart-bar"></fa-icon>Couches</div>
                        <div class="main-layer">
                            <div id="layers">
                                ${this._layers.map( this.renderLayer.bind(this))}
                            </div>
                            <div class="layer" background>
                                <div class="layer-head" >
                                     <div class="title" @click="${(e) => { this.activeLayer = "background"}}">
                                        Fond de cartes
                                    </div>
                                </div>
                                <workspace-sidemenu-item-background id="item-background"></workspace-sidemenu-item-background>
                            </div>
                        </div>
                    </div>
                    <div id="tabScenario" class="tab">
                        <div style="width : 100%;height:100%;background-color : var(--bg-color-alt4);color : var(--color-alt3);">
                           <slot></slot>
                           <scenario-editor></scenario-editor>
                        </div>
                    </div>
                </div>
            </div>
        `;
    }

    

    /** CSS spécifique au composant */
    static get styles() {
        return css`
        :host{
            background-color : var(--bg-color-alt2);
            overflow: hidden;
            padding : 10px 5px;
        }

        .container{
            height : 100%;
        }
        
        fa-button {
            width: 20px;
            text-align: center;
        }
        
        fa-button:first-of-type {
            margin-right: 6px;
        }
        
        .tabs {
            margin-top : 10px;
            padding : 0 5px 0 2px;
            height : 100%;
        }

        .tab {
            display: none;
            height : calc(100% - 40px);
            margin-bottom : 10px;
        }

        .active {
            display: block !important;
        }

        .main-item {
            width : 100%;
            padding : 2px;
            margin : 3px 0 2px 0;
            background-color : var(--bg-color-alt3);
            color : var(--color-alt);
        }

        .clickable {
            cursor : pointer;
        }

        .clickable:hover {
            background-color : var(--color-alt);
            color : var(--bg-color-alt3);
        }

        .main-item fa-icon{
            display : inline-block;
            margin: auto 10px auto 3px;
            width : 18px;
        }
        

        .main-layer{
            max-height: 93%;
            height: 100%;
            display: flex;
            flex-direction: column;
            overflow-y : auto;
            overflow-x : hidden;
            width: calc(100% + 4px);
        }

        .layer{
            display: flex;
            flex-direction: column;
        }

        .layer *:nth-child(2) {
            flex-grow : 1;
        }

        .layer-head {
            width : 100%;
            padding : 2px;
            margin : 3px 0 2px 0;
            background-color : var(--bg-color-alt4);
            color : var(--color-alt3);
            display: flex;
        }

        .layer-hide{
            background-color : var(--bg-color-alt3);
        }

        .layer-head .licon{
            margin: auto 10px auto 3px;
        }

        .layer-head .title {
            flex: 1 1 auto;
        }

        .layer-head .ricon{
            margin-right : 4px;
        }
        .layer-head .ricon fa-icon{
            margin: auto 1px auto 1px;
        }

        .layer-head fa-icon:hover{
            color : var(--color-alt);
        }

        `;
    }
}


customElements.define("workspace-sidemenu", SideMenuWorkspace);
export default SideMenuWorkspace;
