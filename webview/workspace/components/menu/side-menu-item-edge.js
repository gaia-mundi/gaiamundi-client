import { LitElement, html, css } from '../../../lib/lit-element.js';
import SideMenuItem from './side-menu-item.js';

class SideMenuItemEdge extends SideMenuItem{
    render(){
        return this._layer ? [this.renderTitle("style") 
        , this.renderProperty("Couleur de remplissage",this._layer.style.fillColor,"color-t","style.fillColor")
        , this.renderProperty("Couleur de bordure",this._layer.style.borderColor,"color","style.borderColor")
        , this.renderProperty("Largeur bordure",this._layer.style.borderWidth,"number","style.borderWidth")
        ]:html``;
    }
}

customElements.define("workspace-sidemenu-item-edge", SideMenuItemEdge);
export default SideMenuItemEdge;