import { LitElement, html, css } from '../../../lib/lit-element.js';
import IconTypeConstants from '../../../shared/util/IconTypeConstants.js';

class SideMenuItem extends LitElement{

    _layer;
    _isActive;

    set layer(layer){
        this._layer = layer;
        this.requestUpdate();
    }

    /**
     * Action à déclencher quand la couche associée au menu est activée
     * @param {*} state true/false
     */
    setActive(state){
        this._isActive = state;
    }

    /**
     * Masque le menu
     */
    hide(){
        this.style.display = "none";
    }

    /**
     * Affiche le menu
     */
    show(){
        this.style.display = "block";
    }

    /**
     * Affichage d'une zone de propriété
     * @param {*} title 
     * @returns 
     */
    renderTitle(title){
        return html`<span class="title">${title}</span>`;
    }

    updated(changedProperties) {
    }

    /**
     * Affichage d'une valeur de propriété
     * @param {*} value valeur de la propriété
     * @param {*} type type de la propriété (number, color, text)
     * @param {*} associatedProperty json path de la propriété pour la couche
     * @returns 
     */
    renderPropertyValue(value, type, associatedProperty,inputId){
        let id = inputId ? inputId : Date.now();
        switch(type){
            case "number" :
                return html`<input id="${id}" type="${type}" value="${value}" @change="${(e) => {this.onUdpateProperty(associatedProperty, Number(e.target.value));}}">`;
            case "color" : 
            case "text" :
                return html`<input id="${id}" type="${type}" value="${value}" @change="${(e) => {this.onUdpateProperty(associatedProperty, e.target.value);}}">`;
            case "color-t" :     
                let transparent = (e) => {
                    let cb = this.renderRoot.getElementById("cb" + id);
                    if(cb.checked){
                        this.onUdpateProperty(associatedProperty, "transparent");
                    }else{
                        let input = this.renderRoot.getElementById("col" + id);
                        this.onUdpateProperty(associatedProperty, input.value);
                    }
                }
                return html`<input id="col${id}" type="color" value="${value}" @change="${transparent}">
                            <br><input id="cb${id}" class="label" ?checked=${value === "transparent"} type="checkbox" @change="${transparent}"><label for="cb${id}">Transparent</label>`;
            case "quantile-edit":
                return this.disableMinusQuantile() 
                ? html`<button id="plus-quantile" @click=${(e) => { this.addQuantile()}}>+</button><button id="minus-quantile" @click=${(e) => { this.removeQuantile()}} disabled>-</button>`
                : html`<button id="plus-quantile" @click=${(e) => { this.addQuantile()}}>+</button><button id="minus-quantile" @click=${(e) => { this.removeQuantile()}}>-</button>`
            
            case "color-palettes":
                let colorSets = this.colorPalettes();
                let res = [];
                for( const property in colorSets){
                    res.push(html`<button @click=${(e) => { this.setColorSet(colorSets[property])}}> ${property}</button>`);
                }
                return res;
            case "list":
                return html `
                    <select @change=${(e) => {this.iconTypeChange(e.target.value)}}>
                        ${IconTypeConstants.ICON_TYPES.map(iconType => {
                            if (iconType == value) {
                                return html`<option selected="selected" value="${iconType}">${IconTypeConstants.iconLabel(iconType)}</option >`
                            } else {
                                return html`<option value="${iconType}">${IconTypeConstants.iconLabel(iconType)}</option >`
                            }
                        })}
                    </select>`
            default :  
                return html`${value}`;
        }
    }

    

    /**
     * Affichage d'une propriété 
     * @param {*} key titre de la propriété
     * @param {*} value valeur de la propriété
     * @param {*} type type de la propriété (number, color, text)
     * @param {*} associatedProperty json path de la propriété pour la couche
     * @returns 
     */
    renderProperty(key,value,type, associatedProperty,id){
        return html`
            <div class="properties">
                <div class="key">${key}</div>
                <div class="value" id=${id}>${this.renderPropertyValue(value,type,associatedProperty,id)}</div>
            </div>
        `;
    }

    /**
     * Emission d'une modification de propriété
     * @param {*} associatedProperty nom de la propriété
     * @param {*} value nouvelle valeur de la propriété
     */
     onUdpateProperty(associatedProperty, value){
        let event = new CustomEvent('update-layer',{ detail: { id : this._layer.id,  property : associatedProperty, value: value}});
        this.dispatchEvent(event);
    }    

    /** CSS spécifique au composant */
    static get styles() {
        return css`
        :host{
            background-color : var(--bg-color-main);
            font-size : var(--font-size-body2);
            overflow: hidden;
            padding : 2px;
            width : 100%;
            display: block;
        }

        .title {
            font-weight : bold;
        }

        .properties {
            display : flex;
            font-size : var(--font-size-body3);
            align-items: center;
            padding-left : 4px;
            padding : 1px;
        }

        .properties:hover{
            background-color : var(--bg-color-alt5);
        }

        .properties div.key {
            width : 75%;
        }

        .properties div.value {
            width : 25%;
            padding-right: 4px;
        }

        .properties input {
            font-size : var(--font-size-body3);
            height : calc(var(--font-size-body2) + 6px);
            width: 100%;
            padding: 0;
            background-color : var(--bg-color-main);
            border-width: 1px;
            border-color : var(--bg-color-alt2);
        }

        .properties input.label, label {
            position: relative;
            vertical-align: middle;
            width : auto;
        }
        `;
    }
}

export default SideMenuItem;