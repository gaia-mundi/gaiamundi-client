import { LitElement, html, css } from '../../../lib/lit-element.js';
import RenderTreeView from "../tree-view/indicators-tree-view.js";

class SideMenuItemDataIndicator extends RenderTreeView {

    selectNode(data){
        if(!data){
            this.selectedNode = null;
            this.selectedIndicator = null;
        }else if(data.label){
            this.selectedNode = data;
            this.selectedIndicator = null;
        }else{
            this.selectedNode = null;
            this.selectedIndicator = data;
            let event = new CustomEvent('indicator-selected', { detail: { indic : data}});
            this.dispatchEvent(event);
        }
    }

    render(){
        return html `   
        <h2>Choisir l'indicateur à affecter sur la couche</h2>
        <tree-view id ="treeview" @item-selected="${e => {this.selectNode(e.detail)}}" displayRoot folderSelectable></tree-view>
        `
    }


}
customElements.define("workspace-sidemenu-item-data-indicator", SideMenuItemDataIndicator);

export default SideMenuItemDataIndicator;
