import { LitElement, html, css } from '../../../lib/lit-element.js';
import SideMenuItem from './side-menu-item.js';
import workspacesService from "../../../shared/service/workspacesService.js";
import "./side-menu-item-data-indicator.js";
import LayerSlide from "../../components/layer-slide/layer-slide.js"
import DataLayer from "../carte/model/dataLayer.js"
import {WorkspaceExport} from "../page/workspace-page.js"


const { env }  = require('./../../container/environments.js');

const JsonPath = require('jsonpath');

class SideMenuItemData extends SideMenuItem {
    
    
     firstUpdated(){
        workspacesService.getWorkspaceById(
            workspacesService.queryWorkspaceName(),
            (wk) => {
                this._workspace = wk;
            }
        );
    }
    /**
     * rajouter un quantille
     */
    addQuantile(){

        //calcule du nouvau range
        let ranges = [...this._layer.slider.ranges];
        let maxValue =  this._layer.slider.maxValue;
        let size = this._layer.slider.ranges.length;
        let lastValue = size > 0 ? this._layer.slider.ranges[size-1]: 0;
        let newRang = lastValue + (maxValue - lastValue)/2;

        //rajouter un range
        ranges.push(newRang);
        this.onUdpateProperty("properties.slider.ranges", ranges);

        //rajouter une nouvelle couleur
        let rangeColors = [...this._layer.style.rangeFillColors];
        rangeColors.push("#000000");
        this.onUdpateProperty("style.rangeFillColors", rangeColors);
        

        this.requestUpdate();
    }

    /**
     * suprimmer un quantille
     */
    removeQuantile(){
        
        let rangeColors = [...this._layer.style.rangeFillColors];
        let ranges = [...this._layer.slider.ranges];
        
        //suprimmer un range
        ranges.pop();
        this.onUdpateProperty("properties.slider.ranges",  ranges);
        
        //suprimmer une  couleur
        rangeColors.pop();
        this.onUdpateProperty("style.rangeFillColors", rangeColors);

        this.requestUpdate();
    }

    /** recuperer le set des couleur depuis le fichhier application.properties */
    colorPalettes(){
        let tab = env.get('colorSets.sets').split(",");

        let title = '';
        let colorSets = {};
        for (let i = 0; i < tab.length; i++) {
            if( !tab[i].startsWith('#')){
                title = tab[i];
                colorSets[title] = [];
                continue;
            }
            colorSets[title].push(tab[i]);
        }
        return colorSets;
    }

    /**
     * appliquer un set de couleur/quantille sur une couche 
     * @param {*} colors 
     */
    setColorSet(colors){
        console.log("test encore plus ouf")
        let ranges = [...this._layer.slider.ranges];
        let rangeColors = [...this._layer.style.rangeFillColors];

        //difference de seuille entre l'ancien et le nouveaux.
        let diff = rangeColors.length - colors.length;
        if(diff>0){
            //enlever des seuille
            for (let i = 0; i < diff; i++) {
                ranges.pop();
            }
        }else if (diff<0){
            //rajouter des seuille
            let maxValue = this._layer.slider.maxValue;
            let pas;
            let value;
            if(this._layer.slider.ranges.length > 0){
                let lastIndex = this._layer.slider.ranges.length - 1 ;
                pas = (maxValue - ranges[lastIndex]) / (Math.abs(diff));
                value = ranges[lastIndex];

            } else{
                // si 0 range c-a-d une seule quantile
                let minValue = this._layer.slider.minValue;

                pas = (maxValue - minValue) / (Math.abs(diff)+1);
                value = minValue;
            }

            // rajouter les ranges manquant
            for (let i = 0; i < Math.abs(diff); i++) {
                value += pas;
                ranges.push(value);
            }
        }
        
        //reinit les ranges
        this.onUdpateProperty("properties.slider.ranges",  ranges);

        //mettre à jour les couleur
        this.onUdpateProperty("style.rangeFillColors", colors);

        this.requestUpdate();
    }

    disableMinusQuantile(){
        return this._layer.slider.ranges == 0 ;
    }

    iconTypeChange(newIconType){
        this.onUdpateProperty("style.iconType", newIconType);
    }

    /**
     * Envoie la fenêtre de changement d'indicateur
     */
    promptChooseIndicator(){
        this.chooseIndicator = this.shadowRoot.querySelector('#modalChooseIndicator');
        this.chooseIndicator.showModal();
    }
    /**
     * 
     * @param {*} e Indicateur sélectionné
     * 
     * Récupère l'indicateur sélectionné
     * 
     */
    indicatorSelect(e){
        this.indicator = e;
    }

    /**
     * 
     * @param {*} indic Indicateur sélectionné
     * Met à jour le JSON avec le nouvel indicateur
     * 
     */
    indicatorChange(indic){
        this.onUdpateProperty("properties.idIndic", indic.properties.id);
        this.requestUpdate();
    };
    

    renderPropertyIndic(key, value){
        return html `
        <div class="properties">
            <div class="key">${key}</div>
            <div class="value" style="display: flex, margin-top:5px">${value}<button @click="${e=>{this.promptChooseIndicator()}}" style="margin-left: 5px"><fa-icon icon="magnifying-glass"/></button></div>
            <div id= "chooseIndicator">
                <dialog id="modalChooseIndicator" ref="imagePrompt">
                    <form method="dialog"> 
                        <workspace-sidemenu-item-data-indicator @indicator-selected="${e=>{this.indicatorSelect(e.detail.indic)}}"></workspace-sidemenu-item-data-indicator>
                        <button type="submit" value="yes" @click="${e=>{this.indicatorChange(this.indicator)}}">Appliquer</button>
                        <button type="submit" value="no" formnovalidate>Retour</button>
                    </form>
                </dialog>    
            </div>

        </div>
        `
    }

    render() {
            let colorPickers = [];
            for (let i = 0; i < this._layer.style.rangeFillColors.length; i++) {
                const color = this._layer.style.rangeFillColors[i];
                colorPickers.push(this.renderProperty("Couleur de remplissage "+ parseInt(i+1), color, "color", "style.rangeFillColors["+ i +"]"));
            }

            return this._layer ? [this.renderProperty('Titre de la couche', this._layer.title, 'text', "properties.title")
                ,this.renderProperty("Type d'icone", this._layer.style.iconType, "list", "style.iconType")
                ,this.renderPropertyIndic("Indicateur", this._layer.idIndic)
                ,this.renderTitle("style")
                ,this.renderProperty("Edition de Quantile",  this.rangeFillColors ,"quantile-edit"),
                ,this.renderProperty("Palette de couleur",  null ,"color-palettes"),
                ,... colorPickers
            ] : html``;
        
    }
}

customElements.define("workspace-sidemenu-item-data", SideMenuItemData);
export default SideMenuItemData;