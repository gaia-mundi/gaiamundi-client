import "../../../shared/util/tree-view.js";
import { html, css, LitElement } from '../../../lib/lit-element.js';
import "../indicators/edit-indicator.js";
import "../dataSetList/local-data-set-list.js";
import workspacesService from "../../../shared/service/workspacesService.js";
import dataSetService from "../../../shared/service/data-set-service.js";
import IconTypeConstants from '../../../shared/util/IconTypeConstants.js';
import { loadJson} from '../../../shared/util/file.js';
import "../indicators/add-indicator.js";



const fs = require('fs');
const { env } = require('../../container/environments.js');
const workspace = env.get("workspace.location");

const path = require('path');


const workspaceName = workspacesService.queryWorkspaceName();

export const Tag = "indicator-tree-view";

class RenderTreeView extends LitElement {

    selectedIndicator = null;
    selectedNode = null;
    _workspace;
    folderName;
    dataSets=[];

    static get properties() {
        return {
            readOnly: { attribute: true, type : Boolean }
        };
      }
       

    /**
     * Aprés la mise à jour du DOM on doit référence le tag du treeview et on charge les données
     *
     * @param {*} changedProperties list des propriétés mise à jour
     * @memberof GroupeTreeView
     */
     firstUpdated(changedProperties) {
        this.treeview = this.shadowRoot.getElementById("treeview");
        this.loadIndicators();
        this._loadData();
        this.datas = this.shadowRoot.getElementById("datas");
        this.indicatorAdd = this.shadowRoot.getElementById("indicatorAdd");
        this.addFolder = this.shadowRoot.getElementById("addFolder");
        this.addIndic = this.shadowRoot.getElementById("addIndic");
        this.deletIndic = this.shadowRoot.getElementById("deletIndic");
        workspacesService.getWorkspaceById(
            workspacesService.queryWorkspaceName(),
            (wk) => {
                this._workspace = wk;
            }
        );
        
        this.prompt = this.shadowRoot.getElementById("prompt");
        if(this.addFolder){this.addFolder.style.display = "none"};
        if(this.addIndic){this.addIndic.style.display = "none"};
        if(this.deletIndic){this.deletIndic.style.display = "none"};
     }

     updated(changedProperties){
        workspacesService.getWorkspaceById(
            workspacesService.queryWorkspaceName(),
            (wk) => {
                this._workspace = wk;
            }
        );
     };

    /** Boucle d'affichage du tree view */
    updateTreeView(indicators, items){
        indicators.forEach( ind => {
            
            let item = {
                label : ind.label
            };
            let children = [];
            if(ind.children){
                this.updateTreeView(ind.children, children);
            }
            item.items = children;
            items.push(item);
            if(ind.indicators && ind.indicators.length > 0){
                ind.indicators.forEach(leaf => {
                    let leafitem = {
                        label : leaf.title
                    };
                    leafitem.data = {
                        id : leaf.id,
                        properties : leaf
                    };
                    item.items.push(leafitem);
                });
            }
        })
    }

     /**
     * Récupère la liste des jeux de données en local
     */
    _loadData() {
        dataSetService.getDataSetListForLocalWorkspace(workspaceName,
            (localDataSetsInfo)=>{
                this.dataSets = localDataSetsInfo;
                this.requestUpdate();
            },
            (err, msg) => {
                console.error(`Erreur dans le chargement du fichier de données : ${err} - ${msg}`)
            }
            );
    }

    /**
     * Chargement des indicateurs via le workspace
     */
    loadIndicators() {
        let wkId = workspacesService.queryWorkspaceName();
        workspacesService.getWorkspaceById(wkId,
            (wk) => {
                let treeData = [];
                
                this.updateTreeView(wk.indicators, treeData);
                this.treeview.data = {label : "Indicateurs", items : treeData};
            },
            (msg) => {
                console.error("Impossible de récupérer le workspace "+ wkId + " : " + msg);
            }
        )
    }

    /**
   * Emet un évenement de type '' avec comme detail l'identifiant de la catégorie sélectionnée
   *
   * @param {*} data peut être null, un dossier ou un indicateur
   * @memberof RenderTreeView
   */
    selectNode(data){
        if(!data){
            this.selectedNode = null;
            this.selectedIndicator = null;
            this.deletIndic.style.display = "none";
            this.addFolder.style.display = "none";
            this.addIndic.style.display = "none";
            this.datas.style.display = "none";
        }else if(data.label){
            this.selectedNode = data;
            this.selectedIndicator = null;
            this.datas.style.display = "none";
            this.addFolder.style.display = "inline-table";
            this.addIndic.style.display = "inline-table";
            this.deletIndic.style.display = "inline-table";
            this.loadForCreate();
        }else{
            this.selectedNode = null;
            this.selectedIndicator = data;
            this.datas.style.display = "inline-table";
            this.addFolder.style.display = "inline-table";
            this.addIndic.style.display = "none";
            this.deletIndic.style.display = "inline-table";
            this.loadSetOfData();
        }
    }
    /** On définit les valeurs des attributs de edit-indicator */
    loadSetOfData() {
    this.datas.displayIndicator(this.selectedIndicator.properties);
    }

    /** On définit les attributs de add-indicator */
    loadForCreate(){
    this.indicatorAdd.display(this.selectedNode);
    }

    /**
     * Suppression d'un dossier ou d'un indicateur
     * @param {*} nodes le noeud de départ
     * @param {*} id le label(si dossier) ou l'id(si indicateur) de ce qu'on veut supprimer
     * @returns 
     */
    fetchJsonInfoDelete(nodes, id){
        let size = nodes.length;
        let found;
        for (let i = 0; i < size && !found; i++){
            let node = nodes[i];
            if(node.label === id) {
                nodes.splice(i,1);
                workspacesService.saveWorkspace(this._workspace);
                this.loadIndicators();
                found = true;
            } else {
                if(node.indicators){
                    let indSize = node.indicators.length;

                    node.indicators = node.indicators.filter(indic => indic.id !== id);

                    if (indSize > node.indicators.length) {            
                        workspacesService.saveWorkspace(this._workspace);
                        this.loadIndicators();
                            found = true;
                        
                            
                    }
                }
                if(node.children && !found){
                    found = this.fetchJsonInfoDelete(node.children,id);
                };

            }
        };
        return found;
    }

    /** On définit le noeud de départ et l'id/label pour la suppression */
    indicatorDelete(){
        let nodes = this._workspace.indicators;
        let id;
        if(this.selectedIndicator) {
            id = this.selectedIndicator.properties.id;
        } else {
            id = this.selectedNode.label;
        }
        this.fetchJsonInfoDelete(nodes, id);
    }

    /** pop-up pour la création d'un dossier */
    folderPrompt(){
        this.modal = this.shadowRoot.querySelector('#modal');
        this.modalInputValue = this.modal.querySelector('#modalInputVal');
        this.modal.showModal();
        this.modal.addEventListener('close', () => this.newNodeName(this.modal.returnValue));         
    }

    /** pop-up pour le choix du jeu de données */
    dataSetPrompt(){
        this.modalDataSet = this.shadowRoot.querySelector('#modalDataSet');
        this.modalSelectValueDataSet = this.modalDataSet.querySelector('#modalSelectValDataSet');
        this.modalDataSet.showModal();
        this.modalDataSet.addEventListener('close', () => this.newDataSet(this.modalDataSet.returnValue)); 
    }
    
    /** On récupère les données de la pop-up précédente et on affecte les valeurs pour add-indicator */
    newDataSet(answer){
        if(answer === 'yes') {
            this.newIndicatorPrompt();
            this.dataSetChoice = this.modalSelectValueDataSet.value;
            this.indicatorAdd.dataSetName = this.dataSetChoice;
            this.indicatorAdd.workspaceName = workspaceName;
        }
        this.modalSelectValueDataSet.value = this.modalDataSet.returnValue =  '';
    }

    /** Ouvre la pop-up pour la définition de l'indicateur */
    newIndicatorPrompt(){
        this.modalNewIndicator = this.shadowRoot.querySelector('#modalNewIndicator');
        this.modalNewIndicator.showModal();
    }

    /** On récupère le label du nouveau dossier */
    newNodeName(answer){
        if(answer === 'yes') {
            this.folderName = this.modalInputValue.value;
            this.indicatorFolder();
        }
        this.modalInputValue.value = this.modal.returnValue =  '';
    }

    /** On définit le noeud de départ et l'id pour faire la création de dossier */
    indicatorFolder(){
        let nodes = this._workspace.indicators;
        //Si ni un noeud, ni un dossier n'est selectionné alors l'id est null
        let id = null;
        //Si un noeud est selectionné et que ce n'est pas la racine, on récupère le label
        if(this.selectedNode && this.selectedNode.label !== "Indicateurs") {
            id = this.selectedNode.label;
        //Si c'est un indicateur de selectionné alors on récupère l'id
        } else if(this.selectedIndicator){
            id = this.selectedIndicator.properties.id;
        }
        this.fetchJsonNewFolder(nodes, id, this.folderName);
    }

    /**
     * On vérifie l'existence ou non du label voulu dans l'arbre
     * @param {*} nodes noeud de départ
     * @param {*} name label dont on cherche l'occurence
     * @returns 
     */
    checkName(nodes, name){
        let size = nodes.length;
        let res = true;
        for (let i = 0; i < size && res; i++){
            let node = nodes[i];
            if(node.label.toLowerCase() === name.toLowerCase() && res){
                res =  false;
            }
            if(res && node.children){
                 res = this.checkName(node.children,name);
            };
        };
        return res;
    }

    /**
     * On crée dans le workspace un nouveau dossier
     * @param {*} nodes noeud de départ
     * @param {*} id le label du dossier ou l'id de l'indicateur. C'est l'identifiant que l'on cherche pour placer notre dossier
     * @param {*} name le label que l'on souhaite pour notre nouveau dossier
     * @returns 
     */
    fetchJsonNewFolder(nodes, id, name){
        let size = nodes.length;
        let found;
        let indicatorFolder = {
            "label": name,
            "children": [],
            "indicators": []
        };
        let isUnique = this.checkName(nodes, name);
        if(isUnique){
            if(id){
                for (let i = 0; i < size && !found; i++){
                    let node = nodes[i];
                    if(node.label === id) {
                        if(node.children){
                            node.children.push(indicatorFolder);
                        } else if (node.indicators){
                            nodes.splice(i,0, indicatorFolder);
                        } else {
                            nodes.push(indicatorFolder);
                        }
                        workspacesService.saveWorkspace(this._workspace);
                        this.loadIndicators();
                        found = true;
                    } else {
                        if(node.indicators){
                            node.indicators.forEach(ind => {
                                if(ind.id === id){
                                    indicatorFolder.indicators = [ind];
                                    node.indicators = node.indicators.filter(indic => indic.id !== id);
                                    node.children.push(indicatorFolder);
                                    workspacesService.saveWorkspace(this._workspace);
                                    this.loadIndicators();
                                    found = true;
                                }
                            })
                        }   
                        if(node.children && !found){
                            found = this.fetchJsonNewFolder(node.children, id, name);
                        }; 
                    }
                }
            } else {
                nodes.push(indicatorFolder);
                workspacesService.saveWorkspace(this._workspace);
                this.loadIndicators();
                found = true;
            };
            return found;
        } else {
            console.log("Existe déjà");
        }
    }

    /** Affichage */
    render(){
        return html`
        <h2> Consultation des indicateurs de l'espace de travail </h2>
        <div id= "buttons">
            <fa-button icon="folder-plus" id="addFolder"  @click="${e=>{this.folderPrompt()}}"></fa-button>
            <fa-button icon="plus" id="addIndic" @click="${e=>{this.dataSetPrompt()}}"></fa-button>
            <fa-button icon="trash" id="deletIndic" @click="${e=>{this.updated(),this.indicatorDelete()}}"></fa-button>
        </div>
        <div id= "prompt">
            <dialog id="modal" ref="imagePrompt">
                <form method="dialog">
                    <h2>Entrez le nom du dossier</h2>
                    <input id="modalInputVal" type="text" placeholder="Tapez le nom du dossier ici" required>
                    <button type="submit" value="yes">
                        OK
                    </button>
                    <button type="submit" value="no" formnovalidate>
                        Annuler
                    </button>
                </form>
            </dialog>
        </div>
        <div id= "dataSet">
            <dialog id="modalDataSet" ref="imagePrompt">
                <form method="dialog">
                    <h2>Choisissez un jeu de données</h2>
                    <select id="modalSelectValDataSet">
                        ${this.dataSets.map((set) => 
                            html`
                                <option>${set.title}</option>
                            `
                        )}
                    </select>
                    <button type="submit" value="yes">
                        OK
                    </button>
                    <button type="submit" value="no" formnovalidate>
                        Annuler
                    </button>
                </form>
            </dialog>
        </div>        
        <div id= "newIndicator">
            <dialog id="modalNewIndicator" ref="imagePrompt">
                <form method="dialog">
                    <h2>Quel est le type de calcul ?</h2>
                    <indicator-add id="indicatorAdd"></indicator-add>
                    <button type="submit" value="no" @click="${e=>{this.loadIndicators()}}" formnovalidate>
                        Retour
                    </button>
                </form>
            </dialog>
        </div>

        <div id="main">
            <div id= "tree-style">
                <tree-view id ="treeview" @item-selected="${e => {this.selectNode(e.detail)}}" displayRoot folderSelectable></tree-view>
            </div>
            <div>
                <div id= "indicator-style">
                    <indicator-edit id="datas" @node-modified="${e => this.loadIndicators()}" ></indicator-edit>
                </div>
            </div>
        </div>
        `;
    };

    
     /** CSS spécifique au composant */
     static get styles() {
        return css`
            #main{
                display: flex;
                align-items: stretch;
                justify-content: space-between;
            } 
            div #buttons {
                margin-top: 5% 0%;
            }
            div #tree-style {
                margin: 10px 30px 10% 0;
                border-radius: 5px;
                background-color: rgba(255, 255, 255, 0.6);
                
            }

            div #indicator-style {
                border-radius: 5px;
                background-color: rgba(255, 255, 255, 0.6);
                opacity: 0.9;
            }
        `
    }
}

customElements.define('indicator-tree-view', RenderTreeView);
export default RenderTreeView;