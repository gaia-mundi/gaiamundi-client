import { LitElement, html, css } from '../../../lib/lit-element.js';
import '../menu/side-menu.js'
import workspacesService from "../../../shared/service/workspacesService.js";
import "../layer-slide/layer-slide.js";
import layerService from '../../../shared/service/layerService.js';
import CarteSVG from '../carte/carte-svg.js';

const rootPath = require('electron-root-path').rootPath;
const app = window.require('@electron/remote'); 
const dialog = app.dialog;
const { env } = require('../../container/environments.js');
const path = require('path');
const JsonPath = require('jsonpath');

//Largeur du menu gzauche
const SIDEMENU_SIZE = "400px";
const CSS_SIDEMNUSIZE = css`400px`;

class WorkspacePage extends LitElement{
    
    /** Composant carte */
    _carte;

    /** Menu de coté */
    _sideMenu;

    /** slider */
    _slider;

    /**
     * définition du workspace
     *
     * @memberof WorkspacePage
     */
    _workspace;

    /**
     * Sauvegarde automatique des dernières modifications
     *
     * @memberof WorkspacePage
     */
    _autosave; 

    /**
     * definition du fond d'écran
     */
    _backgroundDef;

    /**
     * l'id de la  couche active
     */
    _activatedLayerId;

    /**
     * Propriétés et attribut de l'élément
     */
    static get properties() {
        return {
            workspaceName: { attribute: true, type: String }
        };
    }

    constructor() {
        super();
        this._autosave = true;
        this.workspaceName = workspacesService.queryWorkspaceName();
        window.onresize = () => { this.refresh();}; //on force le re calcul lors d'un redimenssionnement
        window.addEventListener("restore-state", (e) => { this.resetLayersWith(e.detail.id);}); //chargement d'une version du workspace
    }

    /**
     * Identification de la carte et chargement des données
     * @param {*} changedProperties proporiété modifiées 
     */
     firstUpdated(changedProperties){
        this._carte = this.shadowRoot.getElementById("carte");
        this._test = this.shadowRoot.getElementById("carte");
        this._sideMenu = this.shadowRoot.getElementById("sidemenu");
        this._slider = this.shadowRoot.getElementById("slider");
        this._slider.hide();
        workspacesService.getWorkspaceById(
            this.workspaceName,
            (wk) => {
                this._workspace = wk;
                
                //on change le titre
                let event = new CustomEvent('title-change',{ detail: { title : wk.title}});
                window.dispatchEvent(event);

                //on charge le background puis l'ensemble des couches
                workspacesService.loadBackgroundDefinition(wk)
                .then( bgDef => {
                    this._backgroundDef = bgDef;
                    this._carte.setBackground(bgDef)
                    this._sideMenu.setBackground(bgDef);
                    this._loadWorkspaceDefinition(wk);
                    layerService.setWorkspaceName(this.workspaceName);
                });
            }
        );
    }
    /**
     * On ordonne les couches dans le cadre d'un chargement pour un utilisateur simple
     * @param {*} wk workspace courant
     */
    static sortLayer(layers){
        let res = layers;
        if(env.get("workspace.user.profile") === "basic"){
            res = [];
            layers.forEach(layerDef => {
                let order = 1000;
                switch(layerDef.type){
                    case "data" :
                        switch(layerDef.style.iconType){
                            case "coloredZones" :
                                order = 5; 
                                break;
                            case "circle":
                                order = 1;
                                break;
                            case "square":
                                order = 1;
                                break;
                        }
                        break;
                    case "histogram" :
                        order = 4;
                        break;
                    case "edge" :
                        order = 3;
                }
                if(!layerDef.style.display){
                    order += 100;
                }
                res.push({ order : order, layer : layerDef});
            });

            //on trie en fonction de l'ordre d'affichage 
            res.sort((o1,o2) => o1.order < o2.order ? -1 : 1);

            //vérification des doublons (2 couches d'icone affichées)
            let mainLayers = new Map();
            res.forEach( l => {
                if(l.order < 100 && mainLayers.get( l.order) && l.layer.type !== "edge"){
                    l.order += 100;
                    l.layer.style.display = false;
                }else{
                    mainLayers.set(l.order, l);
                }
            });
            res.sort((o1,o2) => {
                if(o1.order === o2.order && o1.layer.type === "data"){ return o1.layer.style.iconType.localeCompare( o2.layer.style.iconType);}
                else { return o1.order < o2.order ? -1 : 1}
            });
            res = res.map(l => l.layer);
        }
        return res;
    }
    /**

     * Chargement de la définition d'un workspace
     * @param {*} wk 
     */
    _loadWorkspaceDefinition(wk){
        //mise à jour des propriétés spécifique du background
        this._carte.updateLayerProperty("background", "style.bgColor", wk.backgroundLayer.style.bgColor);
        this._carte.updateLayerProperty("background", "style.fillColor", wk.backgroundLayer.style.fillColor);
        this._carte.updateLayerProperty("background", "style.borderColor", wk.backgroundLayer.style.borderColor);
        this._sideMenu.backgroundStyle = wk.backgroundLayer.style;

        //on ne charge les couches qu'aprés avoir chargé le fond de carte car sa définition initialise
        //l'intégralité de l'affichage
        let promises = [];

       //ordre des couches en fonction du profil
       wk.layers = WorkspacePage.sortLayer(wk.layers);
       
        wk.layers.forEach(layerDef => {
            promises.push(workspacesService.loadLayerDefinition(wk, layerDef));
        });
            
        Promise.all(promises).then( layers =>{
            layers.forEach( layer => {
                
                // dans le cas ou c'est une couche de donnée rajoutée les données zones pour récupérer les centres.
                if(layer.type === 'data' || layer.type === 'histogram'){
                    layer.zones = this._backgroundDef.zones;
                }

                this._carte.addLayer(layer);
                this._sideMenu.addLayer(layer);
            });
        });

        this.refresh();
    }

    /**
     * Réinitialisation de l'ensemble des layers à partir d'un backup ou du workspace courant
     * @param {*} backupId identifiant du backup ou le workspace courant si null
     */
    resetLayersWith(backupId){
        this._autosave = !backupId;
        this.cleanLayers();
        this.restoreLayers(backupId);
    }

    /**
     * Suppression de l'ensemble des layers
     */
    cleanLayers(){
        this._carte.cleanLayers();
        this._sideMenu.cleanLayers();
    }

    /**
     * Chargement des couches correspondant à un backup ou à l'état courant
     * @param {*} backupId identifiant du backup ou le workspace courant si null
     */
    restoreLayers(backupId){
        if(backupId){
            workspacesService.loadBackupWorkspace(this.workspaceName, backupId,
                (wk) => { this._workspace = wk; this._loadWorkspaceDefinition(wk)}, 
                (err,msg) => { console.error(`Erreur dans le chargement de l'état '${backupId}': ${err} - ${msg}`);})
        }else{
            workspacesService.getWorkspaceById(
                this.workspaceName,
                (wk) => { this._workspace = wk; this._loadWorkspaceDefinition(wk)}
            );
        }
    }

    /**
     * Ouverture/Fermeture du menu de coté
     * @param {*} open 
     */
    toggleSideMenu(open){
        if(open){
            this._sideMenu.classList.remove("close");
        }else{
            this._sideMenu.classList.add("close");
        }
        setTimeout(() => {
            this.refresh();
        }
        , 0);
    }


    /**
     * Raffraichissement de l'affichage
     */
    refresh(){
        this._carte = this.shadowRoot.getElementById("carte");
        if(this._carte){
            this._carte.initViewPort();

            this._slider.style.width = ( this._carte.offsetWidth - 90) + "px";
            this._slider.refresh();
        };
    }
    

    /**
     * Réorganisation des layers
     * @param {*} layers 
     */
    reorganizeLayerForBasicUsage(layers){
        const newLayersState = WorkspacePage.sortLayer(layers);
        const configuration = newLayersState.map(l => { return {id : l.properties.id, display : l.style.display}});
        //on applique la configuration (empillage des couches et affichage) au menu et à la carte
        this._sideMenu.setDisplayConfiguration(configuration);
        this._carte.setDisplayConfiguration(configuration);
        //on savuagarde la configuration
        this.saveWorkspace();
    }
    
    /**
     * Emission d'un évenement de masquage affichage des couche
     * @param {*} type type de couche
     * @param {*} id identifiant de la couche
     */
    toogleLayer(type, id){
        if(env.get("workspace.user.profile") === "basic"){
            let layerPos = this._workspace.layers.findIndex(l => l.properties.id === id);
            const currentLayer = this._workspace.layers[layerPos];

            const oldLayers = [...this._workspace.layers];
           
            //on deplace la layer courant pour s'assurer qu'il est la priorité dans l'affichage
            const priorizeLayers = [...this._workspace.layers];
            const layer = priorizeLayers.splice(layerPos, 1);
            priorizeLayers.unshift(layer[0]);

            //on applique la nouvelle disposition des couches
            this.reorganizeLayerForBasicUsage(priorizeLayers);
        }else{
             //cas générique où on affiche ou masque simplement 
             this._carte.toogleLayer(type, id);

             //on enregistre l'état d'affichage sauf pour le fond de carte l'affichage du fond de carte
             if(CarteSVG.LayerType.background !== type){
                 this.workspaceSaveAttribute([{ path : `$.layers[?(@.properties.id=="${id}")].style.display`, value : this._carte.layer(id).displayed}]);
             }
             
             //si la couche a un slider le aussi toogle
             if( CarteSVG.LayerType.data == type && this._slider.getLayerId() === id && this._activatedLayerId == id){
                 this._slider.toogle();
             }
        }        
    }
    
    /**
     * Déplacement d'une couche vers l'avant plan,
     * @param {*} layerId identifiant de la couche
     */
    moveLayerToFront(layerId){
        this._carte.moveLayerToFront(layerId);
        this.workspaceSavePositionUpdate(layerId, 1);
    }

    /**
     * Déplacement d'une couche vers l'arrière plan,
     * @param {*} layerId  identifiant de la couche
     */
    moveLayerToBack(layerId){
        this._carte.moveLayerToBack(layerId);
        this.workspaceSavePositionUpdate(layerId, -1);
    }

    /**
     * Mise à jour d'une propriété d'une couche
     * @param {*} layerId identifiant du layer
     * @param {*} propertyPath chemin de la propriété
     * @param {*} value nouvelle valeur de la propriété
     */
    updateLayerProperty(layerId, propertyPath, value){
        let refresh = true;
       
        if(propertyPath !== 'properties.title'){
            //la mise à jour du titre de la couche ne concerne pas la carte
            refresh = this._carte.updateLayerProperty(layerId, propertyPath, value);
            this.updateLayerSlider();
        }
        
        if(propertyPath === "style.iconType" && env.get("workspace.user.profile") === "basic"){
            //en mode d'utilisation basique un changement de type d'icone peut réarrangé l'intégralité des couches
            this.reorganizeLayerForBasicUsage(this._workspace.layers);
        }
        
        if(layerId === "background"){
            this.workspaceSaveAttribute([{ path : `$.backgroundLayer.${propertyPath}`, value : value}]);
        }
        
        else{
            this.workspaceSaveAttribute([{ path : `$.layers[?(@.properties.id=="${layerId}")].${propertyPath}`, value : value}]);
        }
        if (refresh) {
            this.refresh()};
    }

    /**
     * Activation d'un menu
     * @param {*} layer 
     */
    activateLayer(layer) {
        if (layer) {
            if (layer.type == CarteSVG.LayerType.data) {
                this._slider.layer = layer;
                //afficher le slider seulement si la couche est afficher et le nombre de range est différent de 0
                if(layer.style.display && layer.slider.ranges.length > 0){
                    this._slider.show();
                }else{
                    this._slider.hide();
                }
            } else {
                this._slider.hide();
            }
            this._activatedLayerId = layer.id;
        }
    }

    /**
     * Décalge du layer dans l'ordre de définition du workspace et sauvegarde
     * @param {*} layerId 
     * @param {*} decalage 
     */
    workspaceSavePositionUpdate(layerId, decalage){
        let layer = JsonPath.value(this._workspace,  `$.layers[?(@.properties.id=="${layerId}")]`);
        let layers = this._workspace.layers;
        let pos = layers.indexOf(layer);
        layers.splice(pos, 1);
        layers.splice(pos + decalage, 0, layer);
        this.saveWorkspace();
    }
    /**
     * Modification d'un ou plusieur attribut du workspace
     * @param {*} values tableau d'objet {path : json path de l'attribut , value : nouvelle valeur}
     */
    workspaceSaveAttribute(values){
        values.forEach(value => {
            JsonPath.value(this._workspace, value.path,  value.value);
        });
        this.saveWorkspace();
    }

    /**
     * Sauvegarde du wroskpace courant
     */
    saveWorkspace(){
        if(this._autosave){
            workspacesService.saveWorkspace(this._workspace);
        }
    }

    /**
     * Export de la carte en PNG
     */
    exportCarteToPng(){
        let pathPromise = dialog.showSaveDialog({
            title : "Nom du fichier d'export",
            defaultPath : path.resolve(rootPath, this.workspaceName + ".png"),
            buttonLabel : "Export",
            filters: [
                { name : 'Images', extensions: ['png'] },
                { name: 'Tous les fichiers', extensions: ['*'] }
              ],
            properties: ['showOverwriteConfirmation ']
        });
        pathPromise.then(
            (f) => { 
                if(!f.canceled){
                    this._carte.exportToPNG(f.filePath);
                }
            },
            (e) => { console.error(e);}
        );
       
    }
    /**
     * créer une archive d'un workspace
     * @param {*} e
     */
    zipCurrentWorkspace() {
        workspacesService.zipWorkspace(this.workspaceName);
    }

    uploadWorkspaceOnServer() {
        workspacesService.uploadWorkspace(this.workspaceName);
    }

    /**
     * Création d'un backup
     * @returns identifiant du backup
     */
    backup(){
        const id = Date.now()+"";
        workspacesService.backupWorkspace(this.workspaceName,id);
        return id;
    }
    
    /**
     * update layer slider data in the workspace.json
     */
    updateLayerSlider(){
        //data slider are already updated in the layer definition just write it to file workspace.json
        this._slider.refresh();
        this._carte.refreshLegend();
        this.saveWorkspace();
    }

     /** Affichage */
    render(){
        return html`
            <layer-slider id="slider" pointerWidth="30"
                @slider-value-change="${(e) => { this.updateLayerProperty(e.detail.layerId, e.detail.property, e.detail.value);this._carte.refreshLegend();}}">
            </layer-slider>
            <carte-svg id="carte"
                @update-layer-slider="${()=> this.updateLayerSlider() }">
            </carte-svg>
            <workspace-sidemenu id="sidemenu" 
                                min-width="${SIDEMENU_SIZE}"
                                @sidemenu-open="${() => this.toggleSideMenu(true)}" 
                                @sidemenu-close="${() => this.toggleSideMenu(false)}"
                                @toggle-layer="${(e) => this.toogleLayer(e.detail.type, e.detail.id)}"
                                @move-layer-back="${(e) => this.moveLayerToBack(e.detail.id)}"
                                @move-layer-front="${(e) => this.moveLayerToFront(e.detail.id)}"
                                @update-layer="${(e) => this.updateLayerProperty(e.detail.id, e.detail.property, e.detail.value)}"
                                @activate-layer="${(e) => this.activateLayer(e.detail.layer)}">
                            <slot></slot>
            </workspace-sidemenu>
        `;
    }

    /** CSS spécifique au composant */
    static get styles() {
        return css`
        :host{
            position : relative;
            display : flex;
        }
        carte-svg{
            width : 100%;
            height : 100%;
        }

        layer-slider{
            position :absolute;
            left : 45px;
        }

        workspace-sidemenu {
           flex: 0 0 ${CSS_SIDEMNUSIZE};
           overflow : hidden;
        }

        workspace-sidemenu.close {
            flex: 0 0 30px;
        }`;
    }
}


customElements.define("workspace-page", WorkspacePage);
export default WorkspacePage;
export const WorkspaceExport = new WorkspacePage;