import { LitElement, html, css } from '../../../lib/lit-element.js';
import workspacesService from "../../../shared/service/workspacesService.js";

class WorkspaceHeader extends LitElement{

    /**
     * List des propriétés
     *
     * @readonly
     * @static
     * @memberof WorkspaceHeader
     */
    static get properties() {
        return {
          title: {type: String, attribute: false, reflect : true},
          readonly: {type: Boolean, attribute: true, reflect : true},
        };
    }

    _stateTitle;

    /**
     * Constructeur
     */
    constructor(){
        super();
    }

    /**
     * Ajout des listeners
     */
    connectedCallback() {
		super.connectedCallback();
		this._slider = this.parentNode.host;
		window.addEventListener("title-change", (e) => { this.title = e.detail.title; this.requestUpdate()});
        window.addEventListener("restore-state", (e) => { this.readonly = !!e.detail.id; this._stateTitle = e.detail.title; this.requestUpdate()});
    }

    unlock(){
        let event = new CustomEvent('restore-state',{ detail: { id : null}});
        window.dispatchEvent(event)
    }

    /**
     * Affiche du composant
     */
    render(){
        return html`
            ${this.title}
            ${this.readonly ? html`<span><fa-button icon="location-pin-lock" alt1 @click="${this.unlock}" title="Restauration de l'état courant"></fa-button>&nbsp;${this._stateTitle}</span>`: html ``}
        `;
    }

    /** CSS spécifique au composant */
    static get styles() {
        return css`
        :host{
            display : block;
            font-size : var(--font-size-header3);
            font-weight: bold;
            color : var(--color-alt);
        }
        
        span {
            font-size: var(--font-size-body1);
            font-weight : normal;
        }`;
    }
}


customElements.define("workspace-header", WorkspaceHeader);
export default WorkspaceHeader;