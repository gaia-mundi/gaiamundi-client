import { LitElement, html, css } from '../../../lib/lit-element.js';
import workspacesService from '../../../shared/service/workspacesService.js';
const app = window.require('@electron/remote');
const path = require('path');
const fs = require('fs');
const { env } = require('../../container/environments.js');

const workspaceDir = env.get("workspace.location");
const FORMAT = {year : "numeric", month : "numeric" , day : "numeric", hour:"numeric", minute:"numeric"};
class RecentWorkspace extends LitElement{

    /** liste des workspaces sur le client */
    recentWorkspaces = [];

    /**
     * Constructeur chargement de la liste des projets recents
     */
    constructor() {
        super();
        this.recentWorkspaces = workspacesService.listWorkspacesOnClient();
    }

    /**
     * 
     * @param {*} e recupere le nom du workspace
     * telecharge un workspace depuis le serveur et l'extrait dans le workspace client
     */
     _launchWorkspace(element) {
        // redirection vers l'écran principal avec le fond de carte du workspace
        window.location.href="./index.html?workspaceName="+element.name;
      }

    _deleteWorkspace(element,evt) {
        evt.stopPropagation();
       let options  = {
            buttons: ["Oui", "Annuler"],
            message: "Voulez-vous supprimer cet espace de travail?"
        };
        app.dialog.showMessageBox(options).then(r => {
            if(r.response === 0) {
                fs.rmSync(path.resolve(workspaceDir, element.name), { recursive: true });
                location.reload();
            }
        });
    }

    //affichage de la liste
    render(){
        return html`${this.recentWorkspaces.map((element, i) => 
                html`
                <div class="card1" @click="${(e) => this._launchWorkspace(element)}">
                    <fa-button id="return" icon="square-minus" @click="${(e) => this._deleteWorkspace(element,e)}" title="Supprimer l'espace de travail"></fa-button>
                    <div class="title">${element.title}</div>
                    <div class="property">
                        <div class="property-label">Echelle</div>
                        <div class="property-value">${element.echelle}</div>
                    </div>
                    <div class="property">
                        <div class="property-label">Maille</div>
                        <div class="property-value">${element.maille}</div>
                    </div>
                    <div class="property">
                        <div class="property-label">Dernier accès</div>
                        <div class="property-value">${element.accessTime.toLocaleDateString("fr-FR", FORMAT)}</div>
                    </div>
                </div>
                `)}`;
    }

    static get styles()
    {
        return css`
        :host{
            display: flex;
            flex-direction: row;
            width : 100%;
            flex-wrap: wrap;
        }

        div.card1 {
            margin : 20px;
            cursor: pointer;
            transition: all 0.3s cubic-bezier(0.17, 0.67, 0.35, 0.95);
            height : 100px;
            width:400px;
            padding : 20px;
            justify-content: center;
            align-items: center;
            text-align: center;
            opacity: 0.8;
            border : var(--border-width1) solid var(--border-color1);
            font-size: var(--font-size-header2);
            background-color: var(--bg-color-alt5);
            color : var(--color-alt2);
        }

        div.card1:hover {
            border-color : transparent;
        }

        div.title{
            width:100%;
            text-overflow: ellipsis;
            overflow: hidden;
            white-space: nowrap;
            width: 100%;
        }

        div.property{
            width:100%;
            display : flex;
        }

        div.property-label{
            font-size: var(--font-size-body1);
            text-align: left;
            font-weight : bold;
            width : 105px;
        }

        div.property-value{
            font-size: var(--font-size-body1);
            text-align: left;
            text-overflow: ellipsis;
            overflow: hidden;
            white-space: nowrap;
        }

        fa-button{
            background-color: var(--bg-color-alt5);
            border: 0px;
            display : float;
            float: right;
            margin-top : -20px;
            margin-right : -15px;
            padding: 0px;
        }
        `;
    }
}

customElements.define("recent-workspace", RecentWorkspace);
export default RecentWorkspace;