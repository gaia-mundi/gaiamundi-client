import { LitElement, html, css } from '../../../lib/lit-element.js';
import dataSetService from '../../../shared/service/data-set-service.js';
import workspacesService from '../../../shared/service/workspacesService.js';

class DataSetList extends LitElement {
    /**
     * liste des jeux de donnée
     */
    dataSets = [];

    /**
     * Propriétés et attribut de l'élément
     */
    static get properties() {
        return {
            workspacename: { attribute: true, type: String }
        };
    }

    constructor() {
        super();
        this.workspacename = workspacesService.queryWorkspaceName();
    }
    /**
     * Déclenché lors du premier chargement
     */
    firstUpdated(changedProperties) {
        this._loadData();
    }

    /**
     * Récupère la liste des jeux de donnée sur le serveur
     */
    _loadData() {
        workspacesService.getWorkspaceById(this.workspacename,
            (data) => {
                dataSetService.getDataSetList(data.echelle, data.maille,
                    (data) => {
                        this.dataSets = data;
                        this.requestUpdate();
                    },
                    (err) => {
                        console.error(err);
                    });
            },
            (err, msg) => { console.error(`Erreur dans le chargement du fichier de données : ${err} - ${msg}`) }

        )


    }
    
    /**
     * charger les jeux de données dans l'espace de travail 
     */
    downloadDataSet(){

        //recuperer 
        let checkboxElements = this.shadowRoot.querySelectorAll(".DataSetCheckBox");
        
        //charger depuis le serveur chaque jeux de donnée sélectionner 
        checkboxElements.forEach( (element) => {
            if(element.checked){
                let dataSetName = element.id;
                dataSetService.getDataSetZip(dataSetName,this.workspacename,
                    ()=> {
                        //transformation du jeux donnée de texte en json
                        dataSetService.dataSetTextFileToJsonFile(this.workspacename,dataSetName,()=>{
                            //revenire a l'espace de travail
                            history.back()
                        })
                    },
                    (err)=> console.error(err));
            }
            
        })
    }

    /** 
     * Affichage
     */
    render() {
        return html`
            <h1>Liste des jeux de données sur le serveur:</h1>
            <br>
            <br>
            <br>
            <div class="form-body">
            ${this.dataSets.map((set) => 
            html`
                <div class="object-card">
                <label>
                <input type='checkbox' class='DataSetCheckBox' id=${set.name}>
                <p>${set.title}</p>
                </label>
                </div>
            `)}
            <button @click="${this.downloadDataSet}">télécharger</button>
            </div>
            <br>
            <h3>Importer un jeu de données</h3>
            <div class="object-card">
                <select-file name="${this.workspacename}"type="jeu de données"></select-file>
            </div>
        `
    }

    static get styles()
    {
        return css`
        :host{
            display : flex;
            flex-direction: column;
            margin-right:30px;
        }
        .row {
            display: flex;
            flex-direction: row;
            justify-content: space-evenly;
        }
        
        h1, h2 {
            text-align: center;
        }
        h3 {
            text-align: center;
            border: 2px solid black;
            border-radius: 5px;
            background-color:rgba(255,255,255, 0.7);
            opacity: 0.8;
        }
        
        .form-card {
            display : flex;
            flex-direction: column;
            width: 30%;        
            margin: 48px 0;
            padding: 16px;
        }

        .form-body {
            padding: 16px;
            overflow-y: scroll;
            overflow-x: hidden;
            

        }
        .object-card {
            flex-direction: column;     
            padding: 16px;
            border: 2px solid black;
            border-radius: 5px;
            margin-bottom : 8px;
            background-color:rgba(255,255,255, 0.7);
            opacity: 0.8;
            
        }
        .form-body.models, .form-body.maps, .form-body.lastworkspaces {
            border: none;
            padding-top: 0;
            max-height: 200px;
        }        
        .form-body.maps {
            max-height: 423px;
        }
        .form-body.lastworkspaces {
            max-height: 728px;
        }
        .form-body.import {
            overflow-y: unset;
        }
        
        .form-body::-webkit-scrollbar-track {
          padding: 2px 0;
          background-color: transparent;
        }
        .form-body::-webkit-scrollbar {
          width: 5px;
        }
        .form-body::-webkit-scrollbar-thumb {
            box-shadow: inset 0 0 6px rgba(0,0,0,.3);
            background-color: #404040;
            border: 1px solid #000;
        }
        
        label.name {
            font-weight: bold;
        }
        
        input[type="text"] {
            box-sizing: border-box;
            width: 100%;
            padding: 0;
            height: 40px;
            border: 2px solid black;
            border-radius: 5px;
        }
        input[type="checkbox"] {
            display: none;
        }
        
        label input[type="checkbox"]:checked + p {
            color: #ffd700;
        }
        
        label {
            cursor: pointer;
        }
         label:hover p {
            font-weight: bold;
         }
         label:active p {
            color: #ffd700;
         }
         
        .ul {
            display: flex;
            flex-direction: column;
            justify-content: center;
            margin-top: 4px;
        }
        
        .delete:hover {
            background-color: red;
            color: #ffffff;
           
        }
        `;
    }

}

customElements.define("data-set-list", DataSetList);
export default DataSetList;
