import "../../../shared/component/icon/faicon.js"
import "../../../shared/component/icon/fabutton.js"
//import "./../tree-view/indicators-tree-view.js" 
import workspacesService from "../../../shared/service/workspacesService.js";


window.workspaceName = () => {
    return workspacesService.queryWorkspaceName()
}

window.workspaceDataSetName = () => {
    return workspacesService.queryDataSetName()
}