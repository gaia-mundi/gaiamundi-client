import { LitElement, html, css } from '../../../lib/lit-element.js';
import dataSetService from '../../../shared/service/data-set-service.js';
import workspacesService from '../../../shared/service/workspacesService.js';
const app = window.require('@electron/remote');
const path = require('path');
const fs = require('fs');
const { env } = require('../../container/environments.js');
const workspaceDir = env.get("workspace.location");


class datatSetManagement extends LitElement {
    /**
     * liste des jeux de donnée
     */
    dataSets = [];

    /**
     * Propriétés et attribut de l'élément
     */
     static get properties() {
        return {
            workspacename: { attribute: true, type: String }
        };
    }

    constructor() {
        super();
        this.workspacename = workspacesService.queryWorkspaceName();
    }
    /**
     * Déclenché lors du premier chargement
     */
    firstUpdated(changedProperties) {
        this._loadData();
    }

    /**
     * Récupère la liste des jeux de donnée en local
     */
    _loadData() {
        dataSetService.getDataSetListForLocalWorkspace(this.workspacename,
            (localDataSetsInfo)=>{
                this.dataSets = localDataSetsInfo;
                this.requestUpdate();
            },
            (err, msg) => {
                console.error(`Erreur dans le chargement du fichier de données : ${err} - ${msg}`)
            }
            );
    }

    /**
     * 
     * @param {*} set Informations de l'objet sélectoinné
     * @param {*} evt Evenement
     */
    _deleteWorkspace(set,evt) {
       
        evt.stopPropagation();
       let options  = {
            buttons: ["Oui", "Annuler"],
            message: "Voulez-vous supprimer le jeu de donnée "+ set.name
        };
        app.dialog.showMessageBox(options).then(r => {
            if(r.response === 0) {
                console.log(this.workspacename)
                fs.rmSync(path.resolve(workspaceDir, this.workspacename, "dataSets", set.name), { recursive: true });
                location.reload();
            }
        });
    }
    


    /** 
     * Affichage
     */
    render() {
        return html`
            <h1>Liste des jeux de données dans l'espace de travail:</h1>
            ${this.dataSets.map((set) => 
            html`
                <div class="card1" >
                    <fa-button id="return" icon="square-minus"  @click="${(e) => this._deleteWorkspace(set,e)}" title="Supprimer l'espace de travail" title="Supprimer l'espace de travail"></fa-button>
                    <div class="title">${set.title}</div>
                </div>
            `)}

        `
    }

    static get styles()
    {
        return css`
        :host{
            display : flex;
            flex-direction: column;
            overflow-y:auto;
        }
        div.card1 {
            margin : 20px;
            cursor: pointer;
            transition: all 0.3s cubic-bezier(0.17, 0.67, 0.35, 0.95);
            height : 100px;
            width:400px;
            padding : 20px;
            justify-content: center;
            align-items: center;
            text-align: center;
            opacity: 0.8;
            border : var(--border-width1) solid var(--border-color1);
            font-size: var(--font-size-header2);
            background-color: var(--bg-color-alt5);
            color : var(--color-alt2);
        }
        div.card1:hover {
            border-color : transparent;
        }
        .row {
            display: flex;
            flex-direction: row;
            justify-content: space-evenly;
        }
        
        h1, h2, h3 {
            text-align: center;
        }
                
        input[type="text"] {
            box-sizing: border-box;
            width: 100%;
            padding: 0;
            height: 40px;
            border: 2px solid black;
            border-radius: 5px;
        }
        a {
            cursor: pointer;
            text-align-last: center;
        }
         a:hover {
            font-weight: bold;
         }
         a:active {
            color: #ffd700;
         }
         fa-button{
            background-color: var(--bg-color-alt5);
            border: 0px;
            display : float;
            float: right;
            margin-top : -20px;
            margin-right : -15px;
            padding: 0px;
        }
        `;
    }
}

customElements.define("dataset-management", datatSetManagement);
export default datatSetManagement;