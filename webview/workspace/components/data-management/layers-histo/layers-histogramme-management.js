import { LitElement, html, css } from '../../../../lib/lit-element.js';
import { loadJson} from '../../../../shared/util/file.js';

import "../../../../shared/component/icon/faicon.js";
import "../../../../shared/component/icon/fabutton.js";

import workspacesService from "../../../../shared/service/workspacesService.js";
import layerService from '../../../../shared/service/layerService.js';
import dataSetService from '../../../../shared/service/data-set-service.js';
import layersHistoDisplay from './layer-histo-display.js'

const fs = require('fs');
const path = require('path');
const { env } = require('../../container/environments.js');

const workspace = env.get("workspace.location");

class layerHisto extends LitElement{

    /** tableau des calculs*/
    calculTypeTab = ["valeur a","somme","(somme/d)%","(a/b)%","[(a-b)/c]%","[S1/S2]%","[(a-b)/S]%"];

    _histogramColones = [];

    _histogramTitle;

    _histogramDataSource;

    _histogramDescription;

    _curentDatas = ''


    static get properties() {
        return {
            workspaceName: { attribute: true, type: String },
            calculType: {attribute: false, reflect : true, type: String},
            dataSet: {attribute: false, type: Array},
            dataSource: {attribute: false, type: String},
        }
    }

    constructor() {
        super();
        this.workspaceName = workspacesService.queryWorkspaceName();
        this.dataSets = [];
        this.dataSet = [];

    }

    firstUpdated(){
       this._loadDataSet();
       this._loadData();
    }

    /**
     * Charge les jeux dez données du workspace courant
     */
    _loadDataSet() {
        dataSetService.getDataSetListForLocalWorkspace(this.workspaceName,
            (localDataSetsInfo)=>{
                this.dataSets = localDataSetsInfo;
                this.requestUpdate();
            },
            (err, msg) => {
                console.error(`Erreur dans le chargement du fichier de données : ${err} - ${msg}`)
            }
            );
    }

    /**
     * 
     * @returns charge la liste des données à calculer
     */
     _loadData() {
        this.dataSet = [];
        let curentDatas = this.shadowRoot.querySelector('#data-sets');
        this.curentValue = curentDatas.options[curentDatas.selectedIndex].value;
        if (this.curentValue !== "none"){
        const datas = JSON.parse(fs.readFileSync(path.resolve(env.get("workspace.location"), this.workspaceName, "dataSets", this.curentValue,"data.json")));
        for(const item in datas.data) {
            if (!['idcarte', 'libel', 'codeINSEE'].includes(item))
                this.dataSet.push(item);
        };}
        this.requestUpdate();
        return this.dataSet;
    }


    /**
     * Récupérer et valider les données insérer dans le formulaire de création d'histogramme
     * @returns true si les données sont valide
     */
     _isHistogramCreationFormDataValid(){
        let histogramColones =[];
        let errorMessageList =[]

        //Récupérer les colonnes sélectionnées
        this.shadowRoot.querySelectorAll(".histogramColones-Checkbox").forEach( (element) => {
            if(element.checked){
                histogramColones.push(element.value);
            }
        });

        //validation : au moin 2 de donnée selectioner
        if(histogramColones.length == 0){
            errorMessageList.push("Veuillez sélectionner aux moins une colonne.")
        }

        //Récupérer le titre de la couche histogramme
        let histogramTitle = this.shadowRoot.getElementById('histogramTitle').value;

        //validation input titre de la couche histogramme
        if(!histogramTitle){
            errorMessageList.push("Veuillez rensigner le titre la couche histogramme" );
        }

        //Récupérer la source de donnée
        let histogramDataSource = this.shadowRoot.getElementById('histogramTitle').value;

        //Récupérer la source de donnée
        let histogramDescription = this.shadowRoot.getElementById('histogramDescription').value;

        //  envoyer les message d'erreur
        if(errorMessageList.length != 0){
            let str = "<ul>";
            for (let i = 0; i < errorMessageList.length; i++) {
                str += "<li>"+ errorMessageList[i] + "</li>";
            }
            str += "</ul>";
            this.shadowRoot.getElementById('validation-message-histogram').innerHTML = str;

            return false;
        }else{
            // affectation des données
            this._histogramColones = histogramColones;
            this._histogramDescription = histogramDataSource;
            this._histogramDataSource = histogramDescription;
            this._histogramTitle = histogramTitle;
            return true;
        }

    }

     /**
     * création d'une couche histogramme
     */
      _createHistoInfo() {
        
        let validation = this._isHistogramCreationFormDataValid();

        if(validation){
            
                    //correction du titre pour l'utiliser en developpement
                    let id = workspacesService._nameFormatting(this._histogramTitle);
            
                    // ecriture des infos de la couche
                    let jsonHistogramLayer =
                        {
                            type: "histogram",
                            properties: {
                                "title": this._histogramTitle,
                                "id": id,
                                "dataSetFile": "./dataSets/"+ this.curentValue+ "/data.json",
                                "usedData": this._histogramColones,
                                "dataSource": this._histogramDataSource,
                                "longTitle": this._histogramDescription
                            },
                            style: {
                                "fillColor": "#F2F2F2", //plusieurs couleurs de remplissage en fonction des seuils
                                "borderColor": "#FFFFF", // une seule couleur de contour
                                "display": false
                            }
                        }
            
                        this._writeLayerToWorkspaceJson(jsonHistogramLayer);

        }
        layersHistoDisplay.render();
        
    }

    /**
     * ecrire une couche dans le workspace.json
     * @param {*} layer couche
     */
     _writeLayerToWorkspaceJson(layer) {
        loadJson(path.resolve(workspace, this.workspaceName, "workspace.json"),
            (d) => {
                let json = (d.layers);
                json.push(layer);
                //ordonner la list selon type
                layerService.sortLayerList(json);
                fs.writeFile(path.resolve(workspace, this.workspaceName, "workspace.json"), JSON.stringify(d), function (onError) {
                    if (onError) throw onError;
                    console.log('File is created successfully.');
                });
                location.reload();
            },
            (err, msg) => { console.error(`Erreur dans le chargement du fichier de données : ${err} - ${msg}`) }
        );
    }

    /**
         * gener un input de selction multiple
         * @param {*} name  un partie de la valeur de l'attibut class de l'input select
         * @param {*} label le label de l'input
         */
    _renderMultiSelectionList(name, label){
        return html`
            <div class="selectBox" >
                <label > ${label}</label>
                <div class="dorpDown-element" @click=${()=>this._toggleListCheckBoxes(name)}>
                    <div class='text'> selectionner des colonnes </div> <fa-icon icon="angle-down" ></fa-icon>
                </div>
                <div id='${name}-dropList' class="multipleChoiceDropList " style='display:none'>
                    ${this.dataSet.map(element =>
                        html`
                            <div>
                                <input type="checkbox" name="histo-data1" class="${name}-Checkbox" value="${element}" >${element}</input>
                            </div>
                        `
                    )}
                </div>            
            </div>
        `
    }

    /**
         * afficher/cacher le MultiSelectionList
         * @param {*} name une partie de la valeur de l'attribut class de MultiSelectionList
         */
    _toggleListCheckBoxes(name){
        let dropList = this.shadowRoot.getElementById(name+'-dropList');
        let display = dropList.style.display;
        dropList.style.display = display == 'none' ? 'block': 'none'; 

    }


    render(){
        return html`
            <div class="form">
            <h1>Formulaire de création d'un histogramme :</h1>
            
            <br>
            <div id="form">
                <form>
                    <label>Choissiez le jeux de données</label>

                    <select id="data-sets" @change=${e=>{this._loadData()}}>
                                            <option value="none" selected>Sélectionnez un jeu de données</option>
                                            ${this.dataSets.map(selectedDataSet => {
                            return html `<option value="${selectedDataSet.name}">${selectedDataSet.name}</option>
                            `
                        })
                        }
                    </select>
                    <label>Choisissez les colonnes de l'histogramme</label>
                    
                    ${this._renderMultiSelectionList('histogramColones')}

                    <div>
                        <label for="histogramTitle">Titre de la couche histogramme</label>
                        <input type="text" id="histogramTitle" name="histogramTitle" value="" placeholder="Titre de l'histogramme ">
                    </div>

                    <div>
                        <label for="histogramDataSource">Source des données</label>
                        <input type="text" id="histogramDataSource" name="histogramDataSource" value="" placeholder="CAF 69 2017">
                    </div>

                    <div>
                        <label for="histogramDescription">Libellé long histogramme</label>
                        <input type="text" id="histogramDescription" name="histogramDescription" value="" placeholder="Libellé long">
                    </div>

                </form>
            </div>
            <div id='validation-message-histogram'></div>
            <button @click="${this._createHistoInfo}">créer couche histogramme</button>
        </div>
        `;
    }

    /** CSS spécifique au composant */
    static get styles() {
        return css`

        div #form {
            background-color: rgba(255,255,255,0.8);
        border-radius:1px;
        }

        .multipleChoiceDropList {
        border: 1px #dadada solid;
        height: 300px;
        overflow: scroll;
        position: absolute;
        background-color: white;
        z-index: 1;
        }

        .selectBox .dorpDown-element fa-icon{
            display: inline;

        }

        .selectBox label{
            display: inline;
        }
        .selectBox select{
            display: inline;
        }
        .selectBox .dorpDown-element{
            display: inline;
            border: solid;
            border-width: 1px;
            padding-left: 2px;
            padding-right: 2px;
            background-color: #ffffff;
        }

        .dorpDown-element:hover {
            background-color: #1e90ff;

        }

        .selectBox .dorpDown-element .text{
            display: inline;
        }

        #validation-message, #validation-message-histogram{
            background-color: #efcccc;
        }
        
        #data1 label:hover, #histo-data1 label {
          background-color: #1e90ff;
        }
        
        .dataCheckbox {
        width: fit-content;
        }
    `}
}

customElements.define("layer-histo", layerHisto);
export default layerHisto;