import { LitElement, html, css } from '../../../../lib/lit-element.js';
import dataSetService from '../../../../shared/service/data-set-service.js';
import workspacesService from '../../../../shared/service/workspacesService.js';
const app = window.require('@electron/remote');
const path = require('path');
const fs = require('fs');
const { env } = require('../../container/environments.js');
const workspaceDir = env.get("workspace.location");


class layersEdgeManagement extends LitElement {
    /**
     * liste des jeux de donnée
     */
    dataSets = [];

    /**
     * Propriétés et attribut de l'élément
     */
     static get properties() {
        return {
            workspacename: { attribute: true, type: String }
        };
    }

    constructor() {
        super();
        this.workspacename = workspacesService.queryWorkspaceName();
    }
    /**
     * Déclenché lors du premier chargement
     */
    firstUpdated(changedProperties) {
        this._loadData();
    }

    
    /**
     * Récuperation des donénes de contour
     * @param {*} workspaceName l'espace de travail
     * @param {*} onSuccess 
     * @param {*} onError 
     */
     getDataSetListForLocalWorkspace(workspaceName, onSuccess, onError) {
        let workspaceEdgeLayerPath = path.resolve(workspaceDir, workspaceName, 'edgelayer');

        //Récuperation de la liste des répertoire des jeux de données 
        fs.readdir(workspaceEdgeLayerPath,
            (err, dirs) => {
                if (err) {
                    onError(err);
                } else {
                    let edgeLayerList = [];
                    //pour chaque répertoire lire le fichier data-set-info.json
                    dirs.forEach(dir => {
                        let edgeLayerPath = path.resolve(workspaceEdgeLayerPath, dir);
                        if (fs.lstatSync(edgeLayerPath).isDirectory()) {
                            try {
                                let edgeLayerInfo = fs.readFileSync(path.resolve(edgeLayerPath, 'data.json'));
                                edgeLayerList.push(JSON.parse(edgeLayerInfo));
                            } catch (err) {
                                if (onError)
                                    onError(err);
                            }
                        }

                    });
                    onSuccess(edgeLayerList);
                }

            });
    }

    /**
     * Récupère la liste des jeux de donnée en local
     */
    _loadData() {
        this.getDataSetListForLocalWorkspace(this.workspacename,
            (localDataSetsInfo)=>{
                this.dataSets = localDataSetsInfo;
                this.requestUpdate();
            },
            (err, msg) => {
                console.error(`Erreur dans le chargement du fichier de données : ${err} - ${msg}`)
            }
            );
    }

    /**
     * 
     * @param {*} set Informations de l'objet sélectoinné
     * @param {*} evt Evenement
     */
    _deleteWorkspace(set,evt) {
       
        evt.stopPropagation();
       let options  = {
            buttons: ["Oui", "Annuler"],
            message: "Voulez-vous supprimer la couche de contour " + set.title
        };
        app.dialog.showMessageBox(options).then(r => {
            if(r.response === 0) {
                console.log(workspaceDir)
                fs.rmSync(path.resolve(workspaceDir, this.workspacename, "edgelayer", set.title), { recursive: true });
                location.reload();
            }
        });
    }
    


    /** 
     * Affichage
     */
    render() {
        return html`
            <h1>Liste des couches de contour dans l'espace de travail</h1>
            ${this.dataSets.map((set) => 
            html`
                <div class="card1" >
                    <fa-button id="return" icon="square-minus"  @click="${(e) => this._deleteWorkspace(set,e)}" title="Supprimer l'espace de travail" title="Supprimer l'espace de travail"></fa-button>
                    <div class="title">${set.title}</div>
                </div>
            `)}

        `
    }

    static get styles()
    {
        return css`
        :host{
            display :flex;
            flex-direction: column;
            margin-right: 200px;
            overflow-y:auto;
        }
        div.card1 {
            margin : 10px auto auto auto;
            cursor: pointer;
            transition: all 0.3s cubic-bezier(0.17, 0.67, 0.35, 0.95);
            height : auto;
            width: 60%;
            padding : 20px;
            justify-content: center;
            align-items: center;
            text-align: center;
            opacity: 0.8;
            border : var(--border-width1) solid var(--border-color1);
            font-size: var(--font-size-header2);
            background-color: var(--bg-color-alt5);
            color : var(--color-alt2);
        }
        div.card1:hover {
            border-color : transparent;
        }
        .row {
            display: flex;
            flex-direction: row;
            justify-content: space-evenly;
        }
        .column {
            float: left;
            width: 25%;
            padding: 0 10px;
        }
        
        h1, h2, h3 {
            text-align: center;
        }
                
        input[type="text"] {
            box-sizing: border-box;
            width: 100%;
            padding: 0;
            height: 40px;
            border: 2px solid black;
            border-radius: 5px;
        }
        a {
            cursor: pointer;
            text-align-last: center;
        }
         a:hover {
            font-weight: bold;
         }
         a:active {
            color: #ffd700;
         }
         fa-button{
            background-color: var(--bg-color-alt5);
            border: 0px;
            display : float;
            float: right;
            margin-top : -20px;
            margin-right : -15px;
            padding: 0px;
        }
        `;
    }
}

customElements.define("layers-edge-management", layersEdgeManagement);
export default layersEdgeManagement;