import { LitElement, html, css } from '../../../../lib/lit-element.js';
import { loadJson} from '../../../../shared/util/file.js';
import dataSetService from '../../../../shared/service/data-set-service.js';
import workspacesService from '../../../../shared/service/workspacesService.js';
const app = window.require('@electron/remote');
const path = require('path');
const fs = require('fs');
const { env } = require('../../container/environments.js');
const workspaceDir = env.get("workspace.location");

const workspace = env.get("workspace.location");


class layersDataDisplay extends LitElement {


    /**
     * Propriétés et attribut de l'élément
     */
     static get properties() {
        return {
            workspacename: { attribute: true, type: String }
        };
    }

    constructor() {
        super();
        this.workspacename = workspacesService.queryWorkspaceName();
    }
    /**
     * Déclenché lors du premier chargement
     */
    firstUpdated(changedProperties) {
        this._loadData();
        workspacesService.getWorkspaceById(
            workspacesService.queryWorkspaceName(),
            (wk) => {
                this._workspace = wk;
            }
        );
    }
    /**
     * Récupère la liste des layers locaux
     */
    _loadData() {
        loadJson(path.resolve(workspace, this.workspacename, "workspace.json"),
        (d) => {
            this.json = [];
            for (let i in d.layers) {
                if (d.layers[i].type === "data") {
                    this.json.push(d.layers[i])
                }
            }
            this.requestUpdate();
        })
    }

    /**
     * 
     * @param {*} set Informations de l'objet sélectoinné
     * @param {*} evt Evenement
     */
    _deleteLayer(set,evt) {
        evt.stopPropagation();
       let options  = {
            buttons: ["Oui", "Annuler"],
            message: "Voulez-vous supprimer la couche "+ set.properties.title +" ?"
        };
        app.dialog.showMessageBox(options).then(r => {
            if(r.response === 0) {
                    for (let i in this._workspace.layers){
                        if(this._workspace.layers[i].type==="data"){
                            if (this._workspace.layers[i].properties.id == set.properties.id){
                                this._workspace.layers.splice(i,1);
                                workspacesService.saveWorkspace(this._workspace);
                                this._loadData();
                            }
                        }
                    }

            }})};

    /** 
     * Affichage
     */
    render() {
        return html`
            <h1>Liste des couches data dans l'espace de travail</h1>
            
            ${this.json ?
                this.json.map((set) => 
                html`
                    <div class="card1" >
                        <fa-button id="return" icon="square-minus"  @click="${(e) => this._deleteLayer(set,e)}" title="Supprimer la couche" title="Supprimer la couche"></fa-button>
                        <div class="title">${set.properties.title}</div>
                    </div>
                `)
                        :
                html ``       
            }
        `
    }

    static get styles()
    {
        return css`
        :host{
            display : flex;
            flex-direction: column;
            margin-right:30px;
            height:100%;
            overflow-y:auto;
        }
        div.card1 {
            margin : 10px auto auto auto;
            cursor: pointer;
            transition: all 0.3s cubic-bezier(0.17, 0.67, 0.35, 0.95);
            height : max-content;
            width:400px;
            padding : 20px;
            justify-content: center;
            align-items: center;
            text-align: center;
            vertical-align: middle;
            opacity: 0.8;
            border : var(--border-width1) solid var(--border-color1);
            font-size: var(--font-size-header2);
            background-color: var(--bg-color-alt5);
            color : var(--color-alt2);
        }

        div.card1:hover {
            border-color : transparent;
        }
        .row {
            display: flex;
            flex-direction: row;
            justify-content: space-evenly;
        }
        
        h1, h2, h3 {
            text-align: center;
        }
                
        input[type="text"] {
            box-sizing: border-box;
            width: 100%;
            padding: 0;
            height: 40px;
            border: 2px solid black;
            border-radius: 5px;
        }
        a {
            cursor: pointer;
            text-align-last: center;
        }
         a:hover {
            font-weight: bold;
         }
         a:active {
            color: #ffd700;
         }
         fa-button{
            background-color: var(--bg-color-alt5);
            border: 0px;
            display : float;
            float: right;
            margin-top : -20px;
            margin-right : -15px;
            padding: 0px;
        }
        `;
    }
}

customElements.define("layers-data-display", layersDataDisplay);
export default new layersDataDisplay;