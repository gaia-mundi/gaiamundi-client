import { LitElement, html, css } from '../../../../lib/lit-element.js';
import { loadJson} from '../../../../shared/util/file.js';

import "../../../../shared/component/icon/faicon.js";
import "../../../../shared/component/icon/fabutton.js";

import layersDataDisplay from "./layers-data-display.js"
import workspacesService from "../../../../shared/service/workspacesService.js";
import IconTypeConstants from '../../../../shared/util/IconTypeConstants.js';
import layerService from '../../../../shared/service/layerService.js';
const fs = require('fs');
const path = require('path');
const { env } = require('../../container/environments.js');

const workspace = env.get("workspace.location");

class layerData extends LitElement{

    /** tableau des calculs*/
    calculTypeTab = ["valeur a","somme","(somme/d)%","(a/b)%","[(a-b)/c]%","[S1/S2]%","[(a-b)/S]%"];
    /** tableau des types d'icones */
    codeTypeTab = ["2,0","2,4","2,3","2,1","2,2","2,5","2,6"];

    _iconFillColors = ['#93065f'];

    _iconType = 'circle';
    
    _histogramColones = [];

    _histogramTitle;

    _histogramDataSource;

    _histogramDescription;
    

    /**
     * Propriétés et attribut de l'élément
     */
    static get properties() {
        return {
            workspaceName: { attribute: true, type: String },
            calculType: {attribute: false, reflect : true, type: String},
            codeType: {attribute: false, type: String},
            dataSet: {attribute: false, type: Array},
            layerTitle: {attribute: false, type: String},
            dataSource: {attribute: false, type: String},
            layerDescription : {attribute: false, type: String},
            data1 : {attribute: false, type: Array},
            data2 : {attribute: false, type: String},
            data3 : {attribute: false, type: Array}
        };
    }

    constructor() {
        super();
        this.workspaceName = workspacesService.queryWorkspaceName();
        this.dataSetName = workspacesService.queryDataSetName();
        this.calculType = "";
        this.codeType = "";
        this.dataSet = [];
        this.data1 = [];
        this.data3 = [];
    }

    /**
     * Identification du workspace et chargement des données associées
     * @param {*} changedProperties proporiété modifiéess
     */
     firstUpdated(changedProperties){
        //this._loadData();
        this._updateColorPickers();
    }


    _isLayerCreationFormDataValid(){
        //validate calculation type
        let data1=[];
        let data2=[];
        let data3=[];

        let errorMessageList = [];

        //validation input titre de la couche
        let layerTitle = this.shadowRoot.getElementById('layerTitle').value;

        if(!layerTitle){
            errorMessageList.push("Veuillez rensigner le titre la couche");
        }

        //recueprer la source de donnée
        let dataSource = this.shadowRoot.getElementById('dataSource').value;

        //recueprer le liblele long
        let layerDescription = this.shadowRoot.getElementById('layerDescription').value;

        //recueprer le liblele long
        let iconType = this.shadowRoot.getElementById('icone').value;

        //  envoyer les message d'erreur
        if(errorMessageList.length != 0){
            let str = "<ul>";
            for (let i = 0; i < errorMessageList.length; i++) {
                str += "<li>"+ errorMessageList[i] + "</li>";
            }
            str += "</ul>";
            this.shadowRoot.getElementById('validation-message').innerHTML = str;

            return false;
        }else{
            // affectation des données
            this.data1 = data1;
            this.data2 = data2;
            this.data3 = data3;
            this.layerTitle = layerTitle;
            this.dataSource = dataSource;
            this.layerDescription = layerDescription;
            this._iconType = iconType;
            return true;
        }

    }
    /**
     * création d'une couche donnée 
     */
     _createLayerInfo() {

        let formValidation = this._isLayerCreationFormDataValid();
        
        if(formValidation){
            //correction du titre pour l'utiliser en developpement
            const regexp = /[^A-zÀ-ÿ0-9]/g;
            let name = this.layerTitle.replaceAll(regexp, "").toLowerCase();

            // ecriture des infos de la couche
            let jsonLayer =
                {
                    type: "data",
                    properties: {
                        "title": this.layerTitle,
                        "id": name,
                        "idIndic":"",
                        "slider": {
                        }
                    },
                    style: {
                        "iconType": this._iconType, //enum : zone, circle, shape, triangle
                        "fillColor": "#F2F2F2", // couleur de remplissage
                        "borderColor": "#FFFFF", // une seule couleur de contour
                        "display": false,
                        "rangeFillColors": this._iconFillColors,
                    }
                }


            this._writeLayerToWorkspaceJson(jsonLayer);

        }

    }
    /**
         * ecrire une couche dans le workspace.json
         * @param {*} layer couche
         */
    _writeLayerToWorkspaceJson(layer) {
        loadJson(path.resolve(workspace, this.workspaceName, "workspace.json"),
            (d) => {
                let json = (d.layers);
                json.push(layer);
                //ordonner la list selon type
                layerService.sortLayerList(json);
                fs.writeFile(path.resolve(workspace, this.workspaceName, "workspace.json"), JSON.stringify(d), function (onError) {
                    if (onError) throw onError;
                    console.log('File is created successfully.');
                    location.reload();
                });
            },
            (err, msg) => { console.error(`Erreur dans le chargement du fichier de données : ${err} - ${msg}`) }
        );
    }

    /**
     *  gener un input de type select
     * @param {*} name  un partie du nom de l'attibut class de l'input select
     * @param {*} label le label de l'input
     */
     _renderList(name, label){
        return html`
        <div class="selectBox" >
            <label for=${name}> ${label}</label>
            <select id='${name}-select' name=${name}>
                ${this.dataSet.map(data =>
                    html`
                        <option value="${data}">${data}</option>
                    `
                )}
            </select>
        </div>
        `
    }

    /**
     * rajouter un input pour choisir une couleur
     * @param {*} color la couleur iniatale
     */
     _addColorPicker(color){
        if(color){
            this._iconFillColors.push(color);
        }else{
            this._iconFillColors.push('#FFFFF');
        }
        this._updateColorPickers();
    }

    /**
     * Mis à jour des selecteur du coueleur celon la liste _iconFillColors
     */
     _updateColorPickers() {
        
        //vider le div qui contient les couleurw pickers
        let iconFillColorsPickers = this.shadowRoot.getElementById('iconFillColorsPickers');
        iconFillColorsPickers.innerHTML ='';



        for (let i = 0; i < this._iconFillColors.length; i++) {
            //creation du conteneur  (input)
            let container = document.createElement('div');
            container.className = 'colorPicker';
            
            //creation du input color
            let input = document.createElement('input');
            input.type = 'color';
            input.name = 'colorPicker';
            
            //rajout d'une couleur à la liste

            input.value = this._iconFillColors[i];
            container.id = i;
            
            //Déclencher au choix d'une nouvelle couleur
            input.onchange = (e) => {
                //get index from colorPicker div
                let container = e.target.parentNode;
                let index = container.id;


                this._iconFillColors[index] = e.target.value;
            };

            let button;
            if(i == 0){
                //creation du button d'ajour d'un couleur picker
                button = document.createElement('button');
                button.type = 'button';
                button.append('+');
                button.onclick = (e)=>{   
                    this._addColorPicker();
                }
            }else{
                //creation du button du supression du couleur picker
                button = document.createElement('button');
                button.type = 'button';
                button.append('-');
                button.onclick = (e)=>{   
                    this._deleteColorPicker(e);
                }
            }
            container.append(input);
            container.append(button);
            iconFillColorsPickers.appendChild(container);
        }
    }
    
    /**
     * supprimer un couleur picker
     * @param {*} e event
     */
    _deleteColorPicker(e){
        //get index from colorPicker div
        let container = e.target.parentNode;
        let index = container.id;

        //supprimer la couleur du tableau
        this._iconFillColors.splice(index, 1);

        this._updateColorPickers();
    }

    /**
     * définier un set de couleur et de quantile 
     */
    _iconBrownFiveColor(){
        this._iconFillColors = ['#ECCF9A','#DC965A','#D67046','#B34B34','#5A1B1E'];
        this._updateColorPickers();
    }
    
    /**
     * définier un set de couleur et de quantile 
     */
    _iconRedFiveColor(){
        this._iconFillColors = ['#FFDC8C','#FFB66E','#FF8538','#F24F12','#E50916'];
        this._updateColorPickers();
    }

    /** Affichage */
    render(){
        return html`
        <div class="form">
            <h1>Formulaire de création d'une couche de données</h1>
            <div id="form">
                <form>

                    <br>
                    <div>
                        <label for="layerTitle">Titre de la couche</label>
                        <input type="text" id="layerTitle" name="layerTitle" value="" placeholder="Titre de la couche">
                    </div>
                    
                    <div>
                        <label for="dataSource">Source des données</label>
                        <input type="text" id="dataSource" name="dataSource" value="" placeholder="CAF 69 2017">
                    </div>

                    <div>
                        <label for="layerDescription">Libellé long</label>
                        <input type="text" id="layerDescription" name="layerDescription" value="" placeholder="Libellé long">
                    </div>

                    <div>
                        <label> Couleur par défaut </label>
                        <button type="button" @click=${this._iconBrownFiveColor}>Palette marron</button>
                        <button type="button" @click=${this._iconRedFiveColor}>Palette rouge</button>
                    </div>
                    
                    <div>
                        <label> Couleur et quantiles : </label>
                        <div id='iconFillColorsPickers'>
                            <!-- inputs injected dynamically -->
                        </div>
                    </div>
                    
                    <div>
                        <label for="icone">Icône:</label>
                        <select id="icone" name="iconType">
                            ${ IconTypeConstants.ICON_TYPES.map( iconType => {
                                return html`<option value=${iconType}>${IconTypeConstants.iconLabel(iconType)} </option>`
                            })                        
                            }
                        </select>
                    </div>
                    


                </form>
            </div>
            <div id='validation-message'></div>
            <button @click="${this._createLayerInfo}">Créer la couche</button>
        </div>
        `}

static get styles()
{
    return css`
    
    div #form{
        background-color: rgba(255,255,255,0.8);
        border-radius:1px;
    }
    `;
}
}

customElements.define("layer-data-create", layerData);
export default layerData;