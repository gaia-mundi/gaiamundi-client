import { LitElement, html, css } from "../../lib/lit-element.js";
import './../../shared/component/selectdir/selectdir.js'
const config = require('../../container/config.js');


class workspaceFolderConfig extends LitElement {

    static get properties() {
        return {
            workspaceFolder:{type: String},
            serveUrl:{type: String},
            mode:{type: String}
        };
      }
    

    constructor() {
        super();
        this.workspaceFolder = config.workspaceFolderPath();
        this.serveUrl = config.serverUrl();
        this.mode = config.mode();
    }

    /**
     * mettre à jour le l'url vers l'espace de travail
     * @param {*} newValue la nouvelle valeur
     */
    _updateWorkspaceFolder(newValue){
        config.setWorkspacePath(newValue);
    }

    /**
     * mettre à jour le mode d'utilisation
     * @param {*} newValue la nouvelle valeur
     */
     _updateMode(newValue){
        config.setMode(newValue);
    }


    /**
     * mettre à jour le l'url du serveur
     * @param {*} e event
     */
    _updateServerUrl(e){
        e.preventDefault();
        let input  = this.shadowRoot.getElementById("serverUrl");
        this.serveUrl = input.value;
        config.setServerUrl(input.value);
    }

    /** Affichage */
    render() {
        return html`
            
                <table id= "table">
                    <tr>
                        <td>Répertoire de travail <b class='mandatory'>*</b> :</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <select-directory folder=${this.workspaceFolder} @folder-change=${(e) => this._updateWorkspaceFolder(e.detail.newValue)}></select-directory>
                        </td>
                    </tr>
                    <tr>
                        <td>Url du serveur  :</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <p>${this.serveUrl}</p>
                            <form @submit="${(e)=> this._updateServerUrl(e)}">
                                <input id='serverUrl' type="url" placeholder="https://example.com">
                                <input type="submit" value="Mettre à jour">
                            </form>
                        </td>
                    </tr>
                    <tr>
                        <td>Mode d'utilisation  :</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <select @change="${(e) => this._updateMode(e.target.value)}">
                                <option value="basic" ?selected=${this.mode ==="basic"}>Simple</option>
                                <option value="expert" ?selected=${this.mode !=="basic"}>Avancé</option>
                            </select>
                        </td>
                    </tr>
                </table>

                <div><b class='mandatory'>*</b> : Obligatoire </div>
            `;
    }

    static get styles() {
        return css`
            .mandatory{
                color: red
            }
            table {
                padding: 5px;
                width: 90%;
                margin-left: 5%;
                margin-top: 10%;
                background-color: rgba (255,255,255, 0.6)
            }
        `;
    }
}


customElements.define("workspace-folder-config", workspaceFolderConfig);
export default workspaceFolderConfig;