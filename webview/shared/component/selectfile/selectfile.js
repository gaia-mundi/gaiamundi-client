import { LitElement, html, css } from '../../../lib/lit-element.js';
const app = window.require('@electron/remote'); 
const dialog = app.dialog;

import workspacesService from '../../service/workspacesService.js';

class SelectFile extends LitElement{
    _fileTypeTab = ["espace de travail", "fond de carte", "données", "contours"];
    _fileType;
    /**
     * Propriétés et attribut de l'élément
     */
    static get properties() {
        return {
          title: {attribute: true, type: String},
          file: {attribute: false, type: String},
          name: {attribute: true, type: String},
          type: {attribute: true, type: String},
          url: {attribute: true, type: String}
        };
      }

    constructor() {
        super();
        this.title = 'Sélectionnez un fichier';
        this.name = "";
        this.type = "";
        
    }

    displayFile(){
        let res = this.file ? this.file : "Aucun fichier selectionné";
        if(res.length > 30){
            res = "..." + res.substring(res.length - 30);
        }
        return res;
    }
    /**
     * 
     * @param {*} e evènement
     */
    _handleClick(e){

        let pathPromise = dialog.showOpenDialog({
            properties: ['openFile']
        });
        pathPromise.then(
            (f) => { 
                if(!f.canceled){
                    this.file = f.filePaths[0];
                }
            },
            (e) => { console.error(e);}
        );
    }
    _updateFileType(e) {
        this._fileType = e.target.value;
    }
    _importFile(e) {
        if(this.displayFile() !== "Aucun fichier selectionné") {
            workspacesService.copyFileFromDrive(this.file, this.type, this.name, this.url);
        }
    }

    render(){
        return html`
        <button @click="${this._handleClick}">${this.title}</button> ${this.displayFile()}<br>
        <button @click="${this._importFile}">Importer le fichier</button>
        `;
    }

    static get styles()
    {
        return css`
            :host{
                font-size : var(--font-size-body1);
            }
            button {
                border : var(--button-border);
                color : var(--button-color);
                background-color : var(--button-bg-color);
                padding : var(--button-padding);
                --button-radius : 3px;
            }
        `;
    }
}

customElements.define("select-file", SelectFile);
export default SelectFile;
