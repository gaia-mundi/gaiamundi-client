import { LitElement, html, css } from '../../../lib/lit-element.js';
import "./faicon.js";

class FaButton extends LitElement{
    
    static get properties() {
        return {
          type: {type: String, attribute: true},
          icon: {type: String, attribute: true}
        };
      }

    constructor(){
        super();
        this.type = "solid";
        this.icon = "0"
    }
      
     /** Affichage */
     render(){
        return html`
           <fa-icon type="${this.type}" icon="${this.icon}"></fa-icon>
        `;
    }

     /** CSS spécifique au composant */
     static get styles() {
        return css`:host{
          display : inline-block;
          border : var(--button-border);
          border-radius : var(--button-radius);
          background-color : var(--button-bg-color );
          color : var( --button-color);
          padding : var(--button-padding);
        }

        :host(:hover){
          color : var( --button-bg-color);
          background-color : var(--button-color );
          cursor : pointer;
        }

        :host([alt]){
          border-width : 0;
        }

        :host([alt1]){
          color : var(--color-alt);
          background-color : var(--bg-color-alt);
          border-width : 0;
        }
        :host([alt1]:hover){
          color : var(--bg-color-alt);
          background-color : var(--color-alt);
        }
        `;
    }
}

customElements.define("fa-button", FaButton);
export default FaButton;