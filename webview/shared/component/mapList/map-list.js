import { LitElement, html, css } from '../../../lib/lit-element.js';
import mapListService from "../../service/mapService.js";

class MapList extends LitElement{
  /**
   *  liste des cartes disponibles
   */
    maps = [];
    /**
     * Propriétés et attribut de l'élément
     */
    static get properties() {
        return {
            title: {attribute: true, type: String},
        };
    }

    /**
     * Déclenché lors du premier chargement pour
     * @param {*} changedProperties
     */
    firstUpdated(changedProperties){
        this._loadData();
    }

    /**
     * Récupère la liste des cartes sur le serveur
     */
    _loadData() {
        mapListService.getMapList((data) => {          
          this.maps = data;
          this.requestUpdate();
        }, (msg) => {

        })
    }

    /** Affichage */
    render() {
        return html`${this.maps.map(i => 
        html`
        <ul><h3>${i.name}</h3>
          <li>${i.codeMaille}</li>
          <li>${i.echelle}</li> 
          <li>${i.maille}</li> 
          <li>${i.authors}</li>
        </ul>
        `)}`;
    }
}

customElements.define("map-list", MapList);
export default MapList;
