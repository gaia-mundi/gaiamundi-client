import { LitElement, html, css } from '../../../lib/lit-element.js';

class MenuItem extends LitElement{
    /**
     * Propriétés et attribut de l'élément
     */
    static get properties() {
        return {
          title: {attribute: true, type: String},
          location: {attribute: true, type: String}
        };
    }
    
    /**
     * 
     * @param {*} e evènement
     */
    _handleClick(e){
        if(this.location){
            window.location.href = this.location;
        }else if(this.onclick){
            this.onclick(e);
        }
        e.stopPropagation();
    }

    /** CSS spécifique au composant */
    static get styles() {
        return css`
          :host{
            display:block;
            color : var(--color-alt);
            background-color: var(--bg-color-alt);
            padding : 2px;
            cursor : pointer;
            margin:2px;
          }

          :host(:hover){
            color : var(--color-main);
            background-color: var(--bg-color-main);
          }
        `;
    }

    /** Affichage */
    render(){
        return html`<span @click="${this._handleClick}">${this.title}</span>`;
    }
}

customElements.define("menu-item", MenuItem);
export default MenuItem;