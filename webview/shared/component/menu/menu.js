import { LitElement, html, css } from '../../../lib/lit-element.js';

class Menu extends LitElement{
    /**
     * Propriétés et attribut de l'élément
     */
    static get properties() {
        return {
          title: {attribute: true, type: String},
          location: {attribute: true, type: String}
        };
    }
    
    /**
     * 
     * @param {*} e evènement
     */
    _handleClick(e){
        if(this.location){
            window.location.href = this.location;
        }else if(this.onclick){
            this.onclick(e);
        }
    }

    /**
     * Déclenché lors du premier chargement pour 
     * @param {*} changedProperties 
     */
    firstUpdated(changedProperties){
        this.subMenu = this.shadowRoot.getElementById("submenu");
    }
    
    /** Affichage menu simple */
    renderLocation(){
        return html`<span @click="${this._handleClick}">${this.title}</span>`;
    }

    /** Affichage / Masquage du sous menu */
    renderSubMenu(){
        return html`<span>${this.title}</span><div id="submenu" class="submenu"><slot></slot></div>`;
    }

    /** CSS spécifique au composant */
    static get styles() {
        return css`
          :host{
            color : var(--color-alt);
            background-color: var(--bg-color-alt);
            padding : 2px;
            cursor : pointer;
            margin-right:1px;
          }

          :host(:hover){
            color : var(--color-main);
            background-color: var(--bg-color-main);
          }

          :host(:hover) .submenu{
            display : block;
          }

          .submenu{
              position:absolute;
              padding-top : 5px;
              margin-left : -2px;
              display : none;
              background-color: var(--bg-color-alt);

          }
        `;
    }

    /** Affichage */
    render(){
        return this.location ? this.renderLocation() : this.renderSubMenu();
    }
}

customElements.define("menu-main", Menu);
export default Menu;