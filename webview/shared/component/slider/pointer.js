import { LitElement, html, css, unsafeCSS  } from '../../../lib/lit-element.js' 

class Pointer extends LitElement{
	/**
	 * Propriété de la balise
	 *
	 * @readonly
	 * @static
	 * @memberof Pointer
	 */
	static get properties() {
	  return {
		value: {type: Number, attribute: true, reflect : true},
		color: {type: String, attribute: true, reflect : true},
	  };
	}

	_slider;
	_onDrag;
	_previous;
	_next;
	_range;
	_info;
	_canWheel = true;
	_value;
	_color;

	/**
   	* Définition du DnD
   	* @memberof Pointer
   	*/
	connectedCallback() {
		super.connectedCallback();
		this._slider = this.parentNode.host;
		this.addEventListener("mousedown", this.onDragStart.bind(this));
		
		//gestion de la molette
		this.addEventListener('wheel', this.onWheel.bind(this));
		this._previous = this.previousElementSibling;
		this._next = this.nextElementSibling;
		this.style.left = this._position() + "px";
	}

	_emitOnChange(){
		let event = new CustomEvent("change", { detail : { value : this.value}});
		this.dispatchEvent(event);
	}

	/**
	 * Identification du div range
	 */
	firstUpdated(){
		this._range = this.renderRoot.getElementById("range");
		this._info = this.renderRoot.getElementById("info");
		this._colorize();
		this.draw();
		this._updateValue();
	}

	/**
	 * Colorisation de la barre associée au pointeur
	 */
	_colorize(){
		if(this._range){
			this._range.style.backgroundColor = this._color;
		}
	}

	/**
	 * Définition de la couleur de la barre associé au pointeur
	 *
	 * @memberof Pointer
	 */
	set color(color){
		this._color = color;
		this._colorize();
	}

	/**
	 * Récupération de la couleur de la barre associé au pointeur
	 *
	 * @memberof Pointer
	 */
	get color(){
		return this._color;
	}



	/**
	 * Dessin du pointeur et de l'intervalle
	 */
	draw() {
		this.style.left = this._position() + "px";
		let rangeWidth;
		if(this._previous){
			rangeWidth = this.offsetLeft - this._previous.offsetLeft - this.offsetWidth - 4;
		}else{
			rangeWidth = this.offsetLeft;
		}
		this._range.style.left = (-rangeWidth) + "px";
		this._range.style.width = (rangeWidth +2) + "px";
	}


	/**
	 * Défini la position horizontale du pointeur
	 * @returns nombre de pixel à gauche
	 */
	_position(){
		return this._slider.position(this.value);
	}

	/**
	 *  Définition de la valeur du pointeur
	 *
	 * @memberof Pointer
	 */
	set value(value){
		if(this._slider){
			this._setPosition(this._slider.position(value));
		}else{
			this._value = value;
		}
	}

	/**
	 * Récupératin de la valeur du pointeur
	 *
	 * @memberof Pointer
	 */
	get value(){
		return this._value;
	}
	/**
	 * Mise à jour de la valeur du pointeur : 
	 * - à partir de sa position si la valeur n'est pas spécifiée
	 * - à partie de la valeur passé en paramètre
	 * @param {*} value valeur (faculatative) 
	 */
	_updateValue(value){
		this._value = value !== undefined ? value : this._slider.value(this.offsetLeft);
		this._info.innerText = parseInt(this._value);
	}

	/**
	 * Déplacement du pointer avec la molette
	 * @param {*} e 
	 */
	onWheel(e){
		if(Math.abs(e.deltaY) > 3&& this._canWheel){
			e.preventDefault();
			console.log(this.offsetLeft);
			this._setPosition(this.offsetLeft - Math.sign(e.deltaY));
		}
	}
	/**
	 * Démarrage du drag & drop du pointeur
	 * @param {*} e evènement HTML
	 */
	onDragStart(e) {
		e.preventDefault();
		this._onDrag = {
			onMove : this.onDragMove.bind(this),
			onUp : this.onDragStop.bind(this),
			x : e.clientX,
			left : this.offsetLeft
		}
		this._info.style.visibility = "visible";
		window.addEventListener("mousemove",this._onDrag.onMove);
		window.addEventListener("mouseup",this._onDrag.onUp);
	}

	/**
	 * Fin du drag & drop du pointeur
	 * @param {*} e evènement HTML
	 */
	onDragStop(e) {
		e.preventDefault();
		this._info.style.visibility = "";
		window.removeEventListener("mousemove",this._onDrag.onMove);
		window.removeEventListener("mouseup",this._onDrag.onUp);
	}

	/**
	 * Déplacement lors drag & drop du pointeur.
	 * Spécifie la nouvelle valeur
	 * @param {*} e evènement HTML
	 */
	onDragMove(e) {
		e.preventDefault();
		let x = e.clientX;
		this._setPosition(x - this._onDrag.x + this._onDrag.left);
	}

	/**
	 * Définition de la position du pointeur
	 * Délcenche le recalcul de la valeur
	 * @param {*} value 
	 */
	_setPosition(value){
		let min = this._previous ? this._previous.value : this._slider.min;
		let max = this._next ? this._next.value : this._slider.max;
		this.style.left = value +"px";
		this._updateValue();
		if(this.value < min){
			this._updateValue(min);
			this.style.left = this._position() + "px";
		}else if(this.value > max){
			this._updateValue(max);
			this.style.left = this._position() + "px";
		}	
		this._emitOnChange();
		this.draw();
	}

	/**
	 * Sytle du composnant
	 * @readonly
	 * @static
	 * @memberof Pointer
	 */
	static get styles() {
		return css `
		
		:host {
			cursor: pointer;
			height: 100%;
			left: 0px;
			position: absolute;
			top: 0;
		}
		div {
			height : 100%;
		}
		#range{
			position : relative;
			left : -10px;
			width : 100px;
			background-color : blue;
		}

		#pointeur {
			width: 100%;
			background-color : var(--bg-color-alt3);
			border : 2px solid var(--bg-color-alt2);
			position : absolute;
			height : calc(100% + 2px);
			top : -2px;
			border-radius: 10px;
		}

		#info {
			width: fit-content;
			padding : 2px;
			font-size : var(--font-size-body2);
			position : relative;
			height : 17px;
			background-color : var(--bg-color-alt3);
			border : 3px solid var(--bg-color-alt2);
			color : var(--color-alt);
			top : 10px;
			text-align: center;
			visibility : hidden;
		}

		:host(:hover) #info{
			visibility : visible;
		}
		`;
	}

	render(){
		return html`<div id="range"></div><div id="pointeur"></div><div id="info">100000</div>`;
	}
}

customElements.define('slider-pointer', Pointer);

