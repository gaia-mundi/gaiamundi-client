import { LitElement, html, css, unsafeCSS  } from '../../../lib/lit-element.js' 
import "./pointer.js";

class MultipleSlider extends LitElement{
	/**
     * Priopriété du composant
     *
     * @readonly
     * @static
     * @memberof MultipleSlider
     */
    static get properties() {
	  return {
		values: {type: Array, attribute: true,reflect : true},
        colors: {type: Array, attribute: true,reflect : true},
		min: {type: Number, attribute: true},
		max: {type: Number, attribute: true},
		step : {type: Number, attribute: true},
        pointerWidth : {type: Number, attribute: true},
        pointerColor : {type: String, attribute: true}
	  };
	}

    /**
     * Constructeur
     */
    constructor(){
        super();
        this.colors = [];
        this.values = [];
        this.min   = 0;
        this.max   = 100;
        this.step  = 1;
    }
	
    /**
     * Mise à jour d'un des pointeurs
     * @param {*} idx index du pointeur
     * @param {*} value nouvelle valeur
     */
	updatePointer(idx, value, pointer){
        this.values[idx] = value;
        this.values = [...this.values];
        let nextPoint = pointer.nextElementSibling;
        if(nextPoint){ nextPoint.draw()}
    }

    

    /**
	 * Affichage du composant
	 */
	show(){
		this.style.visibility = "visible";
	}

	/**
	 * Masquage du composant
	 */
	hide(){
		this.style.visibility = "hidden";
	}

    /**
     * Récupération de la position par rapport à une valeur
     * @param {*} value valeur du pointeur
     * @returns poisition du pointeur
     */
    position(value){
		return value * (this.offsetWidth - this.pointerWidth)/(this.max - this.min)
	}

    /**
     * Récupération de la valeur par rapport à une position
     * @param {*} postion 
     * @returns valeur correspondant à la position
     */
	value(postion){
		return postion * (this.max - this.min)/(this.offsetWidth - this.pointerWidth);
	}

	render(){
		return html`
		${this.values.map( (value, idx) => 
            html
            `<slider-pointer value="${value}" 
                            color="${this.colors[idx]}"
                            @change="${e => this.updatePointer(idx, e.detail.value, e.target)}"
                            style="width:${this.pointerWidth}px;">
            </slider-pointer>`)}	
		`;
	}

  

	static get styles() {
		return css `
		
		:host {
            display:block;
			height: 6px;
			position: relative;
            background : red;
            margin-top : 5px;
		}
		
		`;
	}
}

customElements.define('multiple-slider', MultipleSlider );
export default MultipleSlider;
