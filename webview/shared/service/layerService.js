import HttpClient from "../util/client.js";
import { loadJson } from "../util/file.js";
import workspaceService from "./workspacesService.js";

const { env } = require('../../container/environments.js');
const AdmZip = require('adm-zip');
const path = require('path');
const fs = require('fs');

const workspaceMainFolder = env.get("workspace.location");

class LayerService extends HttpClient {

    _workspaceName;

    
    setWorkspaceName( workspaceName){
        this._workspaceName = workspaceName;
    }

    _loadDataIndicator() {
        let datas;
        if(this._workspaceName){
            datas = JSON.parse(fs.readFileSync(path.resolve(workspaceMainFolder, this._workspaceName, "\workspace.json")));
        }
        return datas;
    }

    fetchIndic(nodes, id){
        let size = nodes.length;
        let res;
        for (let i = 0; i < size && !res; i++){
            let node = nodes[i];
            if(node.indicators){
                node.indicators.forEach(indIc => {
                    if(indIc.id === id){
                        res =  indIc;
                    }
                        
                })
            }
            if(!res && node.children){
                 res = this.fetchIndic(node.children,id);
            };
        };
        return res;
    }

    /**
     * Retourne le tableau de données d'une couche
     * @param {*} layerName Nom de la couche de donnée
     * @param {*} workspaceName Nom du workspace
     * @returns 
     */
    calculateData(layerDefinition, onSuccess, onError) {
        let data = this._loadDataIndicator();
        let idIndicator = layerDefinition.idIndic;
        let indicator = this.fetchIndic(data.indicators, idIndicator);  
       
        let numerator1 = indicator.usedData.firstNumerator;
        let numerator2 = indicator.usedData.secondNumerator;
        let denominator = indicator.usedData.denominator;

        let filePath = path.resolve(workspaceMainFolder, this._workspaceName, indicator.dataSetFile);
        loadJson(filePath,

            (dataSetJsonFile) => {
                let valuesTab = [];
                const idcarte = dataSetJsonFile.data.idcarte;
                if (indicator.calculationType === "valeur a") {
                    const dataToDisplay = dataSetJsonFile.data[numerator1];
                    if (dataToDisplay && idcarte) {
                        for (let i = 0; i < dataToDisplay.length; i++) {
                            const value = {
                                "idCarte": idcarte[i],
                                "value": dataToDisplay[i]
                            }
                            valuesTab.push(value);
                        }
                    }
                } else if (indicator.calculationType === "somme") {
                    let sumTab = [];
                    for (let j = 0; j < numerator1.length; j++) {
                        sumTab.push(dataSetJsonFile.data[numerator1[j]]);
                    }
                    const sums = sumTab[0].map((x, idx) => sumTab.reduce((sum, curr) => parseInt(sum) + parseInt(curr[idx]), 0));
                    if (numerator1 && idcarte) {
                        for (let i = 0; i < sums.length; i++) {
                            const value = {
                                "idCarte": idcarte[i],
                                "value": sums[i]
                            }
                            valuesTab.push(value);
                        }
                    }
                } else if (indicator.calculationType === "(somme/d)%" || indicator.calculationType === "(a/b)%" || indicator.calculationType === "[S1/S2]%") {
                    let numeratorTab = [];
                    let denominatorTab = [];
                    for (let j = 0; j < numerator1.length; j++) {
                        numeratorTab.push(dataSetJsonFile.data[numerator1[j]]);
                    }
                    for (let k = 0; k < denominator.length; k++) {
                        denominatorTab.push(dataSetJsonFile.data[denominator[k]]);
                    }
                    const sumNumerator = numeratorTab[0].map((x, idx) => numeratorTab.reduce((sum, curr) => parseInt(sum) + parseInt(curr[idx]), 0));
                    const sumDenominator = denominatorTab[0].map((x, idx) => denominatorTab.reduce((sum, curr) => parseInt(sum) + parseInt(curr[idx]), 0));

                    if (numerator1 && denominator && idcarte) {
                        for (let i = 0; i < sumNumerator.length; i++) {
                            const value = {
                                "idCarte": idcarte[i],
                                "value": (sumNumerator[i] / sumDenominator[i]) * 100
                            }
                            valuesTab.push(value);
                        }
                    }
                } else if (indicator.calculationType === "[(a-b)/c]%" || indicator.calculationType === "[(a-b)/S]%") {
                    let numerator1Tab = [];
                    let denominatorTab = [];
                    for (let j = 0; j < numerator1.length; j++) {
                        numerator1Tab.push(dataSetJsonFile.data[numerator1[j]]);
                    }
                    for (let k = 0; k < denominator.length; k++) {
                        denominatorTab.push(dataSetJsonFile.data[denominator[k]]);
                    }

                    const sumNumerator = numerator1Tab[0].map((x, idx) => numerator1Tab.reduce((sum, curr) => parseInt(sum) + parseInt(curr[idx]), 0));
                    const numerator2Data = dataSetJsonFile.data[numerator2];
                    const sumDenominator = denominatorTab[0].map((x, idx) => denominatorTab.reduce((sum, curr) => parseInt(sum) + parseInt(curr[idx]), 0));

                    if (numerator1 && numerator2 && denominator && idcarte) {
                        for (let i = 0; i < sumNumerator.length; i++) {
                            const value = {
                                "idCarte": idcarte[i],
                                "value": ((sumNumerator[i] - numerator2Data[i]) / sumDenominator[i]) * 100
                            }
                            valuesTab.push(value);
                        }
                    }
                }
                valuesTab.sort((v1,v2) => { return Number(v1.value) < Number(v2.value) ? 1 : -1});
                onSuccess(valuesTab);

            }, (err, msg) => { console.error(`Erreur dans le chargement du fichier de données : ${err} - ${msg}`) });
    }

    /**
     * Ajout dans la définition du workspace de la couche contour
     * @param {*} filename nom du fichier
     * @param {*} name nom du workspace
     */
    importEdgeLayer(filename, name) {
        // ecriture des infos de la couche
        let jsonLayer =

        {
            "type": "edge",
            "properties": {
                "id": filename,
                "title": filename,
                "url": "./edgelayer/"+filename+"/data.json"
            },
            "style": {
            "borderColor": "#A07AA1 ",
                "fillColor": "#B5D0D0 ",
                "borderWidth": 1,
                "display": true
            }
        }

        loadJson(path.resolve(workspaceMainFolder, name, "workspace.json"),
            (d) => {
                let json = (d.layers);
                json.push(jsonLayer);
                fs.writeFile(path.resolve(workspaceMainFolder, name, "workspace.json"), JSON.stringify(d), function (onError) {
                    if (onError) throw onError;
                    console.log('File is created successfully.');
                });
                window.location.href = "./index.html?workspaceName="+name
            },
            (err,msg) => { console.error(`Erreur dans le chargement du fichier de données : ${err} - ${msg}`)}
        );
    }

    /**
     * Récupérer les données à afficher sur un histogramme
     * @param {*} layer  La couche histogramme concerné
     */
    getHistogramData(layer, onSuccess, onError) {
        loadJson(path.resolve(workspaceMainFolder, this._workspaceName, layer.dataSetFile),
            (dataSetJson) => {
                //les colonnes à extraire
                let columns = layer.usedData;
                let data = [];
                
                let idCartes = dataSetJson.data.idcarte;
                let colors =  this.getHistogramColors(columns.length);

                //Construire in objet par zone qui va représenter les données à afficher pour cette zone
                for(let i = 0; i< layer.zones.length; i++){
                    let histogramData = {};
                    histogramData.idCarte = idCartes[i];
                    histogramData.zoneLabel = layer.zones.find( zone => zone.idCarte == idCartes[i]).title;
                    histogramData.columns = columns;
                    histogramData.values = columns.map(column => dataSetJson.data[column][i])
                    histogramData.layerId = layer.id;
                    histogramData.colors = colors;
                    data.push(histogramData);                  
                };
                
                onSuccess( data);                
                
            },
            (err, msg) => { 
                if(onError)
                    onError();
                console.error(`Erreur dans le chargement du fichier de données : ${err} - ${msg}`);
             }
        );
    }
    /**
     * Récupérer un jeu de couleur pour les bar et legend de l'histogramme
     * @param {*} colorNumber nombre de couleurs
     * @returns 
     */
    getHistogramColors(colorNumber) {
        //default colors
        let colors = ['gray','#008CA4','blue','orange','red','green','violet', 'yellow', '#723525','#2DDD93'];
        let nbrDefaultColor = colors.length;
        
        if( colorNumber <= nbrDefaultColor){
            return  colors.slice(0,colorNumber);
        }else{
            //if needed color more than the nbr of default one add some random colors
            let letters = '0123456789ABCDEF';
            for(let j=0; j <= colorNumber - nbrDefaultColor; j++){
                let color = '#';
                for (var i = 0; i < 6; i++) {
                    color += letters[Math.floor(Math.random() * 16)];
                }
                colors.push(color);
            }
            return colors;
        }
    }

    /**
     * ordonnée les couches par type
     * @param {*} layers les couches
     */
     sortLayerList(layers){
        let sortOrder = () => {
            return function (a, b) {
                    if (a.style.iconType == b.style.iconType || (a.type == b.type && (a.type == 'edge' || a.type == 'histogram') )) {
                        return 0;
                    } else if ((a.style.iconType == "circle" ||  a.style.iconType == "square")
                        || (a.type == "histogram"  && (b.style.iconType == "coloredZones" || b.type == "edge"))
                        || (a.type == "edge" && b.style.iconType == "coloredZones")
                         ) {
                        return -1;
                    } else {
                        return 1;
                    }
            }
        }    
        layers.sort(sortOrder()).reverse();
    }

}

export default new LayerService();
