import HttpClient from "../util/client.js";
const { env } = require('../../container/environments.js');
const fs = require('fs');
const path = require('path');
import { loadJson } from '../util/file.js';
const app = window.require('@electron/remote');
const workspacesDir = path.resolve(app.app.getAppPath(),env.get("workspace.location"));

class DataSetService extends HttpClient {

    /**
     * Récuperation de la liste du jeux de donnée qui correspond à une carte
     * @param {*} echelle echelle de la carte
     * @param {*} maille maille de la carte
     * @param {*} onSuccess 
     * @param {*} onError 
     */
    getDataSetList(echelle, maille, onSuccess, onError) {
        let url = this.url("api/data-sets")
        let params = "?echelle=" + echelle + "&maille=" + maille;

        this.getJson(url + params, onSuccess, onError);
    }

    getDataSetInfo(workspacename, datafilepath) {
        datafilepath = datafilepath.substring(0, datafilepath.lastIndexOf("\\"));
        let dataSetInfo = fs.readFileSync(path.resolve(workspacesDir, workspacename, datafilepath, "data-set-info.json"));
        let dataSetInfoJson = JSON.parse(dataSetInfo);
        return dataSetInfoJson;
        //prendre le datafilepath et prendre que le nom du repertoire (sans le data.json)
        //data-set-info.json
        //let datSetInfo = fs.readFileSync(path.resolve(dataSetPath, 'data-set-info.json'));
        //JSON.parse(datSetInfo)
        // title<- on doit l'afficher
    }

    /**
     * Récuperation de la liste du jeux de donnée de l'espace de travail en cours
     * @param {*} workspaceName l'espace de travail
     * @param {*} onSuccess 
     * @param {*} onError 
     */
    getDataSetListForLocalWorkspace(workspaceName, onSuccess, onError) {
        let workspaceDataSetsPath = path.resolve(workspacesDir, workspaceName, 'dataSets');

        //Récuperation de la liste des répertoire des jeux de données 
        fs.readdir(workspaceDataSetsPath,
            (err, dirs) => {
                if (err) {
                    onError(err);
                } else {
                    let localDataSetsInfo = [];
                    //pour chaque répertoire lire le fichier data-set-info.json
                    dirs.forEach(dir => {
                        let dataSetPath = path.resolve(workspaceDataSetsPath, dir);
                        if (fs.lstatSync(dataSetPath).isDirectory()) {
                            try {
                                let datSetInfo = fs.readFileSync(path.resolve(dataSetPath, 'data-set-info.json'));
                                localDataSetsInfo.push(JSON.parse(datSetInfo));
                            } catch (err) {
                                if (onError)
                                    onError(err);
                            }
                        }

                    });
                    onSuccess(localDataSetsInfo);
                }

            });



    }

    /**
     * téléchargement et extraction  du zip du jeux de donnée depuis le serveur
     * @param {*} dataSetName Nom du jeux de donnée
     * @param {*} workspaceName Nom de l'espace du travail
     * @param {*} onSuccess 
     * @param {*} onError 
     */
    getDataSetZip(dataSetName, workspaceName, onSuccess, onError) {
        let url = "api/data-sets/" + dataSetName;
        let destination = path.resolve(env.get("workspace.location"), workspaceName, "dataSets", dataSetName);
        this.downloadZipFile(url, destination, onSuccess, onError);
    }

    /**
     * transformation du fichier qui contient le jeux de donnée data.txt en data.json
     * @param {*} workspaceName le nom du workspace
     * @param {*} dataSetName le nom du jeux de donnée
     * @param {*} onSuccess
     * @param {*} onError
     */
    dataSetTextFileToJsonFile(workspaceName, dataSetName, onSuccess, onError) {

        let dataSetDir = path.resolve(env.get("workspace.location"), workspaceName, "dataSets", dataSetName);
        let txtFilepath = dataSetDir + "/data.txt";
        let metaDataDataSetFilePath = dataSetDir + "/data-set-info.json";

        //récupérer les donner metadata et créer le fichier data.json
        this._getJsonObjFromTextFile(txtFilepath,
            (json) => {
                loadJson(metaDataDataSetFilePath,
                    (res) => {
                        res["data"] = json;
                        //creation du fichier
                        fs.writeFile(dataSetDir + '/data.json', JSON.stringify(res), () => {
                            if (onSuccess) {
                                onSuccess();
                            }
                        });
                    },
                    (error) => {
                        console.error(`Erreur lors du chargement de la donnée à l'url : ${metaDataDataSetFilePath} : ${error.name} - ${error.message}`);
                        if (onError) {
                            onError(error.message);
                        }
                    }
                );
            },
            () => {
                if (onError) {
                    onError();
                }
            }
        );
    }

    /**
     * transformation du fichier text du jeux de donnée en objet json
     * @param {*} txtFilepath
     * @param {*} onSuccess
     * @param {*} onError
     */
    _getJsonObjFromTextFile(txtFilepath, onSuccess, onError) {
        //lecture du fichier text
        fs.readFile(txtFilepath, 'utf8', (err, data) => {
            if (err) {
                console.error(err)
                if (onError) {
                    onError(err.message);
                }
            } else {
                //transformation des données text en tableau 2 dimension
                let tab = [];
                let lines = data.split(/\r?\n/);
                for (let i = 0; i < lines.length; i++) {

                    tab[i] = lines[i].split('\t');
                }

                //transformation du tableau en objet json
                let json = {}
                for (let i = 0; i < tab[0].length; i++) {
                    
                    let limiter = tab.length ;
                    let limiterValueChanged = false;

                    if (tab[0][i] != '0') {//ignoré les 2 derniere colonnes 0
                        json[tab[0][i]] = [];

                        for (let j = 1; j < limiter; j++) {

                            //préciser lgine de fin des donnée
                            // la fin des données est la ligne -2 ou la valeur de la colonne libel == ensemble 
                            if(tab[j+2][1] == 'ensemble' && !limiterValueChanged){
                                limiter = j;
                                limiterValueChanged = true;
                            }

                            json[tab[0][i]].push(tab[j][i]);

                        }
                    }
                }
                onSuccess(json);
            }
        })
    }


}

export default new DataSetService();
