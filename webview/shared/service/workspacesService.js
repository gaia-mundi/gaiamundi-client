import HttpClient from "../util/client.js";
import { loadJson } from "../util/file.js"
import mapService from "./mapService.js";
import layerService from "./layerService.js";
const { env } = require('../../container/environments.js');
const fse = require('fs-extra');
const AdmZip = require('adm-zip');
const path = require('path');
const fs = require('fs');
const app = window.require('@electron/remote');
const dialog = app.dialog;
const workspaceMainFolder = path.resolve(app.app.getAppPath(),env.get("workspace.location"));
const apiExport = "api/workspaces/export";
const BACKGROUND_FOLDER ="background";
const DATA_FOLDER ="data";
const DATALAYER_FOLDER ="datalyer";
const EDGELAYER_FOLDER ="edgelayer";
//couleur par defaut
const DEFAULTCOLOR = { bgColor : env.get("theme.background.backgroungColor"),
                    borderColor : env.get("theme.background.borderColor"),
                    fillColor : env.get("theme.background.fillColor")
                    }

class WorkspacesService extends HttpClient {

    getWorkspacesList(onSuccess, onError) {
        let apiCall = this.url("api/workspaces/");

        this.getJson(apiCall, onSuccess, onError);
    }

    getWorkspacesModelsList(onSuccess, onError) {
        let apiCall = this.url("api/workspaces/models");

        this.getJson(apiCall, onSuccess, onError);
    }

    /**
     * 
     * @returns nom du workspace passé en paramètre
     */
    queryWorkspaceName() {
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        return urlParams.get("workspaceName");
    }

    /**
     * 
     * @returns nom du jeu de donnée passé en paramètre
     */
    queryDataSetName() {
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        return urlParams.get("dataSetName");
    }

    /**
     * récupérer le contenu du ficher workspace.json
     * @param {*} workspaceId l'identifiant du workspace
     * @param {*} onSuccess call back de succes prend en paramètre le json lu
     * @param {*} onError clallback d'erreur prend en paramètre : le nom de l'erreur et le message de l'erreur
     */
    getWorkspaceById(workspaceId, onSuccess, onError) {
        let workspaceJsonPath = workspaceMainFolder+"/"+workspaceId+"/workspace.json";
        loadJson(workspaceJsonPath, onSuccess, onError);
    }

    /**
     * Sauvegarde d'un workspace
     * @param {*} wk 
     */
    saveWorkspace(wk){
        let wkfile = path.resolve(workspaceMainFolder, wk.name, "workspace.json");
        let data = JSON.stringify(wk, null, '\t');
        return fs.writeFileSync(wkfile, data);
    }

     /**
     * Chargement d'un fichier du workspance
     * @param {*} workspace espace de travail
     * @param {*}  relFilePath chemin relatif du fichier dans le workspace
     * @param {*} data to merge with the result
     * @returns une promise
     */
      loadWorkspaceFile(workspace, relFilePath, data) {
        return new Promise((resole,reject) => {
            let bgFile = path.resolve(workspaceMainFolder, workspace.name, relFilePath);
            loadJson(bgFile,
                (d) => { resole({...d, ...data})},
                (err,msg) => { console.error(`Erreur dans le chargement du fichier '${bgFile}': ${err} - ${msg}`); reject(msg);}
            );
        });
    }

    /**
     * Chargement du fichier de scenario
     * @param {*} workspaceId identifiant du workspace
     * @returns une promise renvoyant le contenu du fichier sous forme de Delta (voir Quill)
     */
    loadWorkspaceScenario(workspaceId){
        if(!workspaceId){
            workspaceId = this.queryWorkspaceName();
        }

        let scenarioFile = path.resolve(workspaceMainFolder, workspaceId, "scenario.json");
        return new Promise(function(resolve, reject) {
            fs.access(scenarioFile, function(error) { 
                //on s'assure que le fichier existe
                if (error) { fs.writeFileSync(scenarioFile, "{}")};
                //on charge le fichier contenant la description JSON du scenario 
                loadJson(scenarioFile, 
                    (d) => { resolve(d)},
                    (err,msg) => { console.error(`Erreur dans le chargement du fichier '${scenarioFile}': ${err} - ${msg}`); reject(msg);}
                );
                
            });
        });
    }

    /**
     * Sauvegarde du contenu du scenario dans le ficheer
     * @param {*} workspaceId identifiant workspace
     * @param {*} text texte du scénario sous forme de Delta (voir Quill)
     * @returns promise 
     */
    saveWorkspaceScenario(workspaceId,text){
        if(!workspaceId){
            workspaceId = this.queryWorkspaceName();
        }
        let scenarioFile = path.resolve(workspaceMainFolder, workspaceId, "scenario.json");
        return new Promise(function(resolve,reject){
            fs.writeFile(scenarioFile, JSON.stringify(text),function(err){
                if (err) 
                    reject(err); 
                else 
                    resolve();
            });
        });
    }

    /**
     * Chargement du fichier de fond de carte
     * @param {*} workspace espace de travail
     * @returns une promise
     */
    loadBackgroundDefinition(workspace) {
        //on définit les couleurs par défaut si elles ne sont pas définies
        workspace.backgroundLayer.style = {...DEFAULTCOLOR, ...workspace.backgroundLayer.style};
        return this.loadWorkspaceFile(workspace, workspace.backgroundLayer.properties.url, {style : workspace.backgroundLayer.style});
    }

    /**
     * Cahrgement du fichier de couche
     * @param {*} workspace espace de travail
     * @param {*} layer définition du layer dans le workspace
     * @return une promise 
     */
    loadLayerDefinition(workspace, layer) {
        if (!layer.properties.url) {
            //le cas ou url est undefined c-a-d on est sur une couche donnée pas de fichier de donnée à charger
            return new Promise((resole, reject) => {
                resole({ ...layer.properties, ...{ style: layer.style, type: layer.type } });
            })
        }
            return this.loadWorkspaceFile(workspace, layer.properties.url, { ...layer.properties, ...{ style: layer.style, type: layer.type } });
    }


    /**
     * 
     * @returns la liste des espaces de travail présents sur le client
     */
    listWorkspacesOnClient() {
        let tabListing = [];
        const listing = fs.readdirSync(workspaceMainFolder);
        listing.forEach(function(file) {
            try {
                if (fs.lstatSync(path.resolve(workspaceMainFolder, file)).isDirectory()) {
                    let files = fs.readdirSync(path.resolve(workspaceMainFolder, file));
                    files.forEach(function(data){
                        if(data === "workspace.json"){
                            let object = JSON.parse(fs.readFileSync(path.resolve(workspaceMainFolder, file, data)));
                            const stats = fs.statSync(path.resolve(workspaceMainFolder, file, data));
                            // ajout de la propriété accessTime au fichier workspace.json
                            object = { ...object, accessTime: stats.mtime};
                            tabListing.push(object);
                        }
                    });
                }
            } catch (e) {
              console.error("Something went wrong : " + e);
            }
        });
        // tri par date d'accès décroissant
        const sortByMapped = (map,compareFn) => (a,b) => compareFn(map(a),map(b));
        const byValue = (a,b) => b - a;
        const toTime = e => e.accessTime;
        const byTime = sortByMapped(toTime,byValue);
        tabListing.sort(byTime);
        return tabListing;
    }

    /**
     * 
     * @param {*} title titre du workspace
     * @param {*} mapName fichier du fond de carte à récupérer du serveur
     * @param {*} maille la maille de la carte
     * @param {*} echelle l'echelle de la carte
     * 
     */
    createWorkspace(type, title, mapName, maille, echelle,url) {
        let name = this._nameFormatting(title);

        const dir = workspaceMainFolder + "/" + name;
        let notExistingDir = false;
        // creation du repertoire du workspace
        if (!fs.existsSync(dir)){
            notExistingDir = true;
            fs.mkdir(dir, { recursive: true }, () => {
                // telechargement du fond de carte dans le dossier background
                mapService.downloadMapZipFile(mapName, path.resolve(dir, BACKGROUND_FOLDER));
                setTimeout(() => {
                    fs.rename(path.resolve(dir, BACKGROUND_FOLDER, "data.json"), path.resolve(dir, BACKGROUND_FOLDER, "geojsondata.json"), () => {
                        mapService.convertGeoJsonMapToGaiaMundiMap(path.resolve(dir, BACKGROUND_FOLDER, "geojsondata.json"), path.resolve(dir, BACKGROUND_FOLDER, "data.json"), notExistingDir, dir);
                    });
                }, 500);


                const filename = "data.json";

                // ecriture des infos du workspace
                let jsonWorkspace =
                    {
                        "name": name,
                        "title": title,
                        "echelle": echelle,
                        "maille": maille,
                        "backgroundLayer": {
                            "properties" : {
                                "name": mapName,
                                "url": "./" + BACKGROUND_FOLDER + "/" + filename,
                            },
                            "style" : {
                                "fillColor": DEFAULTCOLOR.fillColor,
                                "borderColor": DEFAULTCOLOR.borderColor,
                                "bgColor": DEFAULTCOLOR.bgColor
                            }
                        },
                        "layers" : []
                    }

                fs.writeFile(dir + '/workspace.json', JSON.stringify(jsonWorkspace), function (onError) {
                    if (onError) throw onError;
                    setTimeout(() => {
                        if(url) {
                            location.href = url+name;
                        }else{
                            location.reload();
                        }
                    }, 500)
                });
            });
        }
        else {
            if(notExistingDir === true) {
                fs.rmSync(dir, { recursive: true });
            }
            dialog.showErrorBox('Nom incorrect','entrez un nom valide et non-existant');
        }
    }

    createWorkspaceFromZipMap(url, title,redirectUrl){
        let notExistingDir = false
        let name = this._nameFormatting(title);

        const dir = path.resolve(workspaceMainFolder, name);

        // creation du repertoire du workspace
        if (!fs.existsSync(dir)){
            try{
                notExistingDir = true;
                fs.mkdirSync(dir, { recursive: true });
                // telechargement du fond de carte dans le dossier background
                let zip = new AdmZip(url);
                zip.extractAllTo(path.resolve(dir, BACKGROUND_FOLDER),true);
                fs.rename(path.resolve(dir, BACKGROUND_FOLDER, "data.json"), path.resolve(dir, BACKGROUND_FOLDER, "geojsondata.json"), (err) => {
                    mapService.convertGeoJsonMapToGaiaMundiMap(path.resolve(dir, BACKGROUND_FOLDER, "geojsondata.json"), path.resolve(dir, BACKGROUND_FOLDER, "data.json"), notExistingDir, dir);
                });

                const filename = "data.json";

                let mapInfo = JSON.parse(fs.readFileSync(path.resolve(dir, BACKGROUND_FOLDER, filename)));
                // ecriture des infos du workspace
                let jsonWorkspace =
                    {
                        "name": name,
                        "title": title,
                        "echelle": mapInfo.echelle,
                        "maille": mapInfo.maille,
                        "backgroundLayer": {
                            "properties" : {
                                "name": mapInfo.title,
                                "url": "./" + BACKGROUND_FOLDER + "/" + filename,
                            },
                            "style" : {
                                "fillColor": DEFAULTCOLOR.fillColor,
                                "borderColor": DEFAULTCOLOR.borderColor,
                                "bgColor": DEFAULTCOLOR.bgColor
                            }
                        },
                        "layers" : []
                    }

                fs.writeFile(dir + '/workspace.json', JSON.stringify(jsonWorkspace), function (onError) {
                    if (onError) throw onError;
                    fs.readdir(dir, function(err, files) {
                        if (!err) {
                            if (!files.length) {
                                fs.rmSync(dir, { recursive: true });
                            } else {
                                setTimeout(() => {
                                    if(redirectUrl){
                                        location.href=redirectUrl+name;
                                    }else{
                                        location.reload();
                                    }
                                }, 500)
                            }
                        }
                    });
                });
            }catch(e){
                console.error(e.name + " " + e.message);
            }
        }
        else {
            if(notExistingDir === true) {
                fs.rmSync(dir, { recursive: true });
            }
            dialog.showErrorBox('Nom incorrect','entrez un nom valide et non-existant');
        }
    }

    createWorkspaceFromModel(title, apiCall,url){
        let notExistingDir = false;
        //correction du titre pour l'utiliser en developpement
        let name = this._nameFormatting(title);

        const dir = workspaceMainFolder + "/" + name;

        if (!fs.existsSync(dir)){
            notExistingDir = true;
            fs.mkdirSync(dir, { recursive: true });
            this.downloadZipFile(apiCall, dir);
            //modification du workspace.json pour lui donner les bons title et name
            setTimeout(() => {
                let file = JSON.parse(fs.readFileSync(path.resolve(dir, "workspace.json")));
                file =  { ...file, name: name, title: title};
                fs.writeFileSync(path.resolve(dir, "workspace.json"), JSON.stringify(file))

                //on transforme la map du format geojson au format gaiamundi
                fs.rename(path.resolve(dir, BACKGROUND_FOLDER, "data.json"), path.resolve(dir, BACKGROUND_FOLDER, "geojsondata.json"), (err) => {
                    mapService.convertGeoJsonMapToGaiaMundiMap(
                        path.resolve(dir, BACKGROUND_FOLDER, "geojsondata.json"), 
                        path.resolve(dir, BACKGROUND_FOLDER, "data.json"), 
                        true, dir);
                });
                
                if(url) {
                    location.href = url+name;
                }else{
                    location.reload();
                }
            }, 500)
        }
        else {
            if(notExistingDir === true) {
                fs.rmSync(dir, { recursive: true });
            }
            dialog.showErrorBox('Nom incorrect','entrez un nom valide et non-existant');
        }
    }

    _nameFormatting(title) {
        const regexp = /[^A-zÀ-ÿ0-9]/g;
        //correction du titre pour l'utiliser en developpement
        return title.replaceAll(regexp, "").toLowerCase();
    }

    /**
     * Exporter l'espace de travail vers le serveur
     * @param {} workspaceName le nom de l'espace de travail
     * @param {*} onSuccess 
     * @param {*} onError 
     */
    uploadWorkspace(workspaceName, onSuccess, onError) {
        // réprtoire à zipper
        var dir = workspaceMainFolder + "/" + workspaceName;

        let zip = new AdmZip();
        zip.addLocalFolder(dir);

        //creation du zip sous forme de buffer
        zip.toBuffer((buffer, err) => {
            if (err) {
                onError(err);
                dialog.showErrorBox("problème d'archivage","la création de l'archive a échouée, veuillez réessayer.");
            } else {
                this.uploadZipFile(buffer, workspaceName, this.url("api/workspaces"),
                    () => {
                        if (onSuccess)
                            onSuccess();
                            let options  = {
                                message: "Espace uploadé avec succès."
                            }
                            app.dialog.showMessageBoxSync(options);
                    },
                    () => {
                        if (onError)
                            onError();
                    }
                );
            }
        });

    }
    /**
     * 
     * @param {*} title titre du workspace pour la création du zip
     */
    zipWorkspace(title) {
        let zip = new AdmZip();
        let name = this._nameFormatting(title);
        const dir = path.resolve(workspaceMainFolder,name);
        zip.addLocalFolder(dir);
        zip.writeZip(dir+".zip", (err) =>{
            if(!err) {
                let options = {
                    message: "Espace archivé avec succès."
                }
                app.dialog.showMessageBoxSync(options);
            }
            else {
                dialog.showErrorBox("problème d'archivage","la création de l'archive a échouée, veuillez réessayer.");
            }
        })
    }

    /**
     * Suppression d'un backup
     * @param {*} workspaceId identifiant du workspace (si null on prend le workspace courant)
     * @param {*} backupId identifiant du backup à supprimer
     */
    deleteBackupWorkspace(workspaceId, backupId){
        if(!workspaceId){
            workspaceId = this.queryWorkspaceName();
        }
        const workspaceFolder = path.resolve(workspaceMainFolder,workspaceId);
        const backupFolder =  path.resolve(workspaceFolder,env.get("workspace.backup.location"));
        const dest = path.resolve(backupFolder,backupId);
        fs.rmSync(dest, { recursive: true, force: true });
    }
    /**
     * Création d'un backup du workspace
     * @param {*} workspaceName identifiant du workspace
     * @param {*} backupId identifiant du backup
     */
    backupWorkspace(workspaceName, backupId){
        const workspaceFolder = path.resolve(workspaceMainFolder,workspaceName);
        const backupFolder =  path.resolve(workspaceFolder,env.get("workspace.backup.location"));
        const dest = path.resolve(backupFolder,backupId);
        //creation du répertoire de backup
        fse.ensureDir(dest, err => {if(err) {console.error("Impossible de créer le répertoire "+dest + ", " + err.message)}});
        //copy d'un fichier ou d'un repertoire dans le fichier de backup
        function copy(src, callback){
            let srcPath = path.resolve(workspaceFolder,src);
            let desPath = path.resolve(dest,src);
            fse.copy(srcPath,desPath, err => {
                if(err){console.error(`Erreur dans la copie de '${backupWorkspaceDef}': ${err} - ${msg}`);}
                else if(callback){ callback(desPath)}
            });
            return desPath;
        }
        //copy de la définition du workspace
        copy("workspace.json", backupWorkspaceDef => {
            //changement des urls pour être en adequation avec la racine du projet
            loadJson(backupWorkspaceDef,
                (d) => { 
                    //fonction de remplacement des urls
                    function replace(relPath){
                        return "./" + path.relative(workspaceFolder,path.resolve(dest, relPath))
                                          .replace(/\\/g, '/');
                    }

                    //changement de l'ensemble des urls
                    d.backgroundLayer.properties.url = replace(d.backgroundLayer.properties.url);
                    d.layers.forEach(l => {
                        if(l.properties.url){
                            l.properties.url = replace(l.properties.url);
                        }

                    });
                    
                    //sauvegarde de la transformation
                    const data = JSON.stringify(d, null, '\t');
                    fs.writeFile(backupWorkspaceDef, data, err=> {
                        if(err){ console.error(`Erreur dans la transformation du fichier de workspace '${backupWorkspaceDef}': ${err} - ${msg}`);}
                    });
                },
                (err,msg) => { console.error(`Erreur dans le chargement du fichier '${backupWorkspaceDef}': ${err} - ${msg}`);}
            );
        });
        //copy de l'ensemble des répertoire du workspace (excepté le backup)
        fs.readdirSync(workspaceFolder, { withFileTypes: true }).filter(dirent => dirent.isDirectory() && dirent.name !== "backup").forEach(d => copy(d.name));
    }
    /**
     * Récupération d'un backup du workspace
     * @param {*} workspaceName identifiant du workspace
     * @param {*} backupId identifiant du backup
     * @param {*} onSuccess call back de succes prend en paramètre le json du workspace
     * @param {*} onError clallback d'erreur prend en paramètre : le nom de l'erreur et le message de l'erreur
     */
    loadBackupWorkspace(workspaceName, backupId,onSuccess, onError) {
        const workspaceFolder = path.resolve(workspaceMainFolder,workspaceName);
        const backupFolder =  path.resolve(workspaceFolder,env.get("workspace.backup.location"));
        const workspaceJsonPath = path.resolve(backupFolder,backupId,"workspace.json");
        loadJson(workspaceJsonPath, onSuccess, onError);
    }

    /**
     * 
     * @param {*} url absolute path of file to copy
     */
    copyFileFromDrive(url, fileType, name, redirectUrl) {
        if(!name){
            dialog.showErrorBox('Erreur','Veuillez saisir un nom');
        }else{
            // get filename from the path
            const filename = url.substring(url.lastIndexOf('\\') + 1).replace(".zip", "");
            switch(fileType) {
                case "fond de carte":
                    this.createWorkspaceFromZipMap(url, name,redirectUrl);
                    break;
                case "espace de travail":
                    let zipWorkspace = new AdmZip(url);
                    let dirName = this._nameFormatting(name);
                    if (!fs.existsSync(path.resolve(workspaceMainFolder, dirName))) {
                        fs.mkdirSync(path.resolve(workspaceMainFolder, dirName), {recursive: true});
                        zipWorkspace.extractAllTo(path.resolve(workspaceMainFolder, dirName), true);
                        //Mise à jour du nom et titre du workspace
                        this.getWorkspaceById(dirName,
                            wk => {
                                wk.name = dirName;
                                wk.title = name;
                                this.saveWorkspace(wk);
                                if(redirectUrl){
                                    location.href=redirectUrl+dirName;
                                }else{
                                    location.reload();
                                }
                            });
                    }
                    else {
                        dialog.showErrorBox('Espace existant','cet espace existe déjà dans votre application');
                    }
                    break;
                case "jeu de données":
                    let zipData = new AdmZip(url);
                    if (fs.existsSync(path.resolve(workspaceMainFolder, filename))) {
                        zipData.extractAllTo(path.resolve(workspaceMainFolder, name, "dataSets", filename), true);
                        location.reload();
                    }
                    else {
                        dialog.showErrorBox('Erreur','erreur lors de transfert de fichier');
                    }
                    break;
                case "contour":
                    let zipEdge = new AdmZip(url);
                    if (!fs.existsSync(path.resolve(workspaceMainFolder, filename))) {
                        zipEdge.extractAllTo(path.resolve(workspaceMainFolder, name, "edgelayer", filename), true);
                        setTimeout(() => {layerService.importEdgeLayer(filename, name)}, 500);
                        layerService.importEdgeLayer(filename, name)
                        location.reload();
                    }
                    else {
                        dialog.showErrorBox('Contour existant','ce contour existe déjà dans votre application');
                    }
                    break;
                default:
                    fs.copyFile(url, path.resolve(workspaceMainFolder, filename), (err) => {
                        if (err) throw err;
                        console.log('Le fichier ' + filename + ' a été copié avec succès');
                    });
            }
        }
    }

    /**
     *
     * @returns la liste des espaces de travail présents sur le client en zip
     */
    listZipWorkspaceOnClient() {
        let ListTab = [];
        const listing = fs.readdirSync(path.resolve(workspaceMainFolder, env.get("localzipfiles.workspace")));
        listing.forEach(function(file) {
            try {
                function extractMapInfoFile(filepath, file, tempDirectory) {
                    try {
                        const zipToExtract = new AdmZip(filepath);
                        // on extrait le fichier mapInfo.json qui contient les infos basiques de la carte dans un dossier temporaire
                        zipToExtract.extractEntryTo(file, tempDirectory, false, true);
                    } catch (e) {
                        console.error("Something went wrong : " + e);
                    }
                }
                const zip = new AdmZip(path.resolve(workspaceMainFolder, env.get("localzipfiles.workspace"), file));
                for (const zipEntry of zip.getEntries()) {
                    if (zipEntry.name === "workspace.json") {
                        // extraction du fichier mapInfo.json pour récupérer les données
                        extractMapInfoFile(path.resolve(workspaceMainFolder, env.get("localzipfiles.workspace"), file) , zipEntry.name, workspaceMainFolder);
                        // lecture du fichier
                        const object = fs.readFileSync(path.resolve(workspaceMainFolder, zipEntry.name));
                        // écriture des données dans un tableau
                        ListTab.push(JSON.parse(object));
                        // suppression du fichier mapInfo.json traité
                        fs.unlinkSync(path.resolve(workspaceMainFolder, zipEntry.name));
                    }
                }
            } catch (e) {
                console.log("Something went wrong : " + e);
            }
        });
        return ListTab;
    }
}

export default new WorkspacesService();
