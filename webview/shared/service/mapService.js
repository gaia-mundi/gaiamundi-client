const path = require('path');
const rootPath = require('electron-root-path').rootPath;
import HttpClient from "../util/client.js";
const polylabel = require('polylabel');
const fs = require('fs');
const app = window.require('@electron/remote');
const dialog = app.dialog;
import { loadJson } from "../util/file.js"
const { env } = require('../../container/environments.js');
const AdmZip = require('adm-zip');

const workspace = env.get("workspace.location");

class MapListService extends HttpClient {
    
    getMapList(onSuccess, onError) {
        let apiCall = this.url("api/maps/");

        this.getJson(apiCall, onSuccess, onError);
    }

    /**
     * telecharger le zip de la carte depuis le serveur vers un dir rn local
     * @param {*} mapId l'identifiant de la carte
     * @param {*} dest le repertoire de destination
     * @param {*} onSuccess 
     * @param {*} onError 
     */
    downloadMapZipFile(mapId, dest,  onSuccess, onError) {
        let apiCall = "api/maps/"+mapId;

        this.downloadZipFile(apiCall, dest, onSuccess, onError);
    }

     /**
      * Converti un fichier geojson en structure facilement exploitable par GaiaMundi
      * @param {*} inputPath chemin du fichier geojson
      * @param {*} outputPath chemin du fichier au format gaiamundi (path & centroid précalculé)
      * @param notExistingDir booleen pour determiner si le repertoire du workspace vient d'etre crée
      * @param dir chemin vers le répertoire du workspace
      */
      convertGeoJsonMapToGaiaMundiMap(inputPath,outputPath, notExistingDir, dir){
        let inputFile = inputPath.startsWith("./") || inputPath.startsWith("../") ? path.resolve(rootPath, inputPath) : inputPath;
        let ouputFile = outputPath.startsWith("./") || outputPath.startsWith("../")? path.resolve(rootPath, outputPath) : outputPath;
        //lecture du fichier
        let rawdata = fs.readFileSync(inputFile);
        let geojsonData = JSON.parse(rawdata);
        let res = {zones : []};
        //définition des méta informations
        res.title = "TODO";
        res.zoneGeographiqe = "TODO";
        res.maille = "TODO";

        if(geojsonData.features !== undefined) {
            //chargmeent des zones
            geojsonData.features.forEach(feature => {
                let path;
                let computeCentroid = false;

                let convert = (coord, currentPath) => {
                    let path = currentPath;
                    path += `M ${coord[0][0]} ${coord[0][1]}`;
                    for (let i = 1; i < coord.length; i++) {
                        path += ` L ${coord[i][0]} ${coord[i][1]}`;
                    }
                    return path;
                }

                switch (feature.geometry.type) {
                    case 'LineString' :
                        path = convert(feature.geometry.coordinates, "");
                        break;
                    case 'Polygon' :
                        computeCentroid = true;
                        path = ""
                        feature.geometry.coordinates.forEach(coor => {
                            path = convert(coor, path) + " "
                        });
                        break;
                    case 'MultiPolygon' :
                        computeCentroid = true;
                        path = ""
                        feature.geometry.coordinates.forEach(coor => {
                            coor.forEach(poly => path = convert(poly, path) + " ");
                        });
                        break;
                }
                let zone = {
                    title: feature.properties.nom, //TODO valider le nom de cette propriété pour le titre, voir également pour l'identifiant
                    path: path,
                }

                if (computeCentroid) {
                    let coordinates = feature.geometry.coordinates;
                    if(feature.geometry.type === 'MultiPolygon'){
                        //récupération 
                        let maxArea = 0, maxPolygon = [];
                        for (let i = 0, l = feature.geometry.coordinates.length; i < l; i++){
                            const p = feature.geometry.coordinates[i];
                            const area = this.polygonArea(p[0]);
                            if (area > maxArea){
                                maxPolygon = p;
                                maxArea = area;
                            }
                        }
                        coordinates = maxPolygon;
                    }
                    let centroid = polylabel(coordinates, 1.0);
                    zone.cx = centroid[0];
                    zone.cy = centroid[1]
                }
                res.zones.push(zone);
            });

            let data = JSON.stringify(res);
            fs.writeFileSync(ouputFile, data);
        }
        else {
            dialog.showErrorBox("Format incorrect","le format d'entrée du fond de carte n'est pas geoJSON");
            if(notExistingDir === true) {
                fs.rmSync(dir, { recursive: true });
            }
        }
    }   

    /**
     * Calcul de l'aire d'un polygone
     * @param {*} coordinates 
     * @returns 
     */
    polygonArea(coordinates){
        var total = 0;

        for (var i = 0, l = coordinates.length; i < l; i++) {
          var addX = coordinates[i][0];
          var addY = coordinates[i == coordinates.length - 1 ? 0 : i + 1][1];
          var subX = coordinates[i == coordinates.length - 1 ? 0 : i + 1][0];
          var subY = coordinates[i][1];
    
          total += (addX * addY * 0.5);
          total -= (subX * subY * 0.5);
        }
    
        return Math.abs(total);
    }

    /**
     *
     * @returns la liste des fonds de cartes présents sur le client
     */
    listZipMapOnClient() {
        let ListTab = [];
        const listing = fs.readdirSync(path.resolve(workspace, env.get("localzipfiles.background")));
        listing.forEach(function(file) {
            try {
                function extractMapInfoFile(filepath, file, tempDirectory) {
                    try {
                        const zipToExtract = new AdmZip(filepath);
                        // on extrait le fichier mapInfo.json qui contient les infos basiques de la carte dans un dossier temporaire
                        zipToExtract.extractEntryTo(file, tempDirectory, false, true);
                    } catch (e) {
                        console.error("Something went wrong : " + e);
                    }
                }
                const zip = new AdmZip(path.resolve(workspace, env.get("localzipfiles.background"), file));
                for (const zipEntry of zip.getEntries()) {
                    if (zipEntry.name === "mapInfo.json") {
                        // extraction du fichier mapInfo.json pour récupérer les données
                        extractMapInfoFile(path.resolve(workspace, env.get("localzipfiles.background"), file) , zipEntry.name, workspace);
                        // lecture du fichier
                        const object = fs.readFileSync(path.resolve(workspace, zipEntry.name));
                        // écriture des données dans un tableau
                        ListTab.push(JSON.parse(object));
                        // suppression du fichier mapInfo.json traité
                        fs.unlinkSync(path.resolve(workspace, zipEntry.name));
                    }
                }
            } catch (e) {
                console.error("Something went wrong : " + e);
            }
        });
        return ListTab;
    }


}

export default new MapListService();
