const ICON_TYPES_DATA = [
    { circle: { label: 'Cercle', value: 'circle' } },
    { key: 'Carré', value: 'square' },
    { key: 'Zone colorée', value: 'coloredZones' }
];

const ICON_CIRCLE = 'circle';
const ICON_SQUARE = 'square';
const ICON_COLORED_ZONES = 'coloredZones';

const ICON_TYPES = [ICON_CIRCLE,ICON_SQUARE, ICON_COLORED_ZONES] 

const ICON_CIRCLE_LABEL = 'Cercle';
const ICON_SQUARE_LABEL = 'Carré';
const ICON_COLORED_ZONES_LABEL = 'Zone colorée';



class IconTypeConstants {

    static get ICON_TYPES() {
        return ICON_TYPES;
    }

    static get ICON_CIRCLE(){
        return ICON_CIRCLE;
    }

    static get ICON_SQUARE(){
        return ICON_SQUARE;
    }

    static get ICON_COLORED_ZONES(){
        return ICON_COLORED_ZONES;
    }

    static iconLabel(iconType){
        switch (iconType) {
            case ICON_CIRCLE:
                return ICON_CIRCLE_LABEL
                break;
            case ICON_SQUARE:
                return ICON_SQUARE_LABEL
                break;
            case ICON_COLORED_ZONES:
                return ICON_COLORED_ZONES_LABEL
                break;
        }
    }
    
}

export default IconTypeConstants;