const { env } = require('../../container/environments.js');
const fs = require('fs');
const AdmZip = require('adm-zip');
const FormData = require('form-data');
const app = window.require('@electron/remote');
const dialog = app.dialog;
//définition de l'url racine
const serverUrl = env.get("server.url");
const root = serverUrl ?
                serverUrl.endsWith("/") 
                    ? serverUrl
                    : serverUrl +"/"
                : "http://localhost:3000/";

//determination du protocole de communication
const protocol = root && root.startsWith('https://') ? require('https') : require('http');

class HttpClient {
    /**
     * Client à utiliser module {@link https://nodejs.org/api/http.html http} ou 
     * {@link https://nodejs.org/api/https.html https}
     * 
     * @readonly
     * @memberof HttpClient
     */
    get client() {
        return protocol;
    }

    /**
     * Récupération de l'url absolue
     * @param {*} path chemin relatif
     * @returns 
     */
    url(path){
        let absolutePath = path;
        if(absolutePath.startsWith("/")){
            absolutePath = absolutePath.substring(1);
        }
        return root + absolutePath;
    }

    /**
     * Téléchargement d'un fichier
     * @param {*} path chemin relatif pour l'url (source)
     * @param {*} destination chemin du fichier local (destination)
     * @param {*} success call back method en cas de succes (ne prend pas de paramètres)
     * @param {*} error call back method en cas d'erreur (message d'erreur en paramètre)
     */
    downloadFile(path, destination,success, error){
        let url = this.url(path);
        console.debug(`Télechargemnent du fichier ${url}`);
        let file = fs.createWriteStream(destination);
        let request = this.client.get(url, function(response) {
            response.pipe(file);
            file.on('end', function() {
                if(success){
                    file.close(success);  // appel du callback àla fin du téléchargement
                }else{
                    file.close();
                }
            });
        }).on("error", function(err) { // erreur lors du téléchargement
                console.error(`Erreur lors du téléchargement de l'url ${url} : ${err.name} - ${err.message}`)
                fs.unlink(destination); // suppression du fichier créé
                if (error) error(err.message);
        });
    }

    /**
     * Téléchargement et décompressage d'un fichier
     * @param {*} path url du fichier à telecharger
     * @param {*} destination chemin du repertoire local (destination)
     * @param {*} success call back method en cas de succes (ne prend pas de paramètres)
     * @param {*} error call back method en cas d'erreur (message d'erreur en paramètre)
     */
    downloadZipFile(path, destination,success, error){
        let url = this.url(path);
        console.debug(`Télechargemnent de l'archive ${url}`);

        this.client.get(url, function(res) {    
            //Buffer de données
            let data = [], dataLen = 0; 
          
            res.on('data', function(chunk) {
              data.push(chunk);
              dataLen += chunk.length;
          
            }).on('end', function() {
              let buf = Buffer.alloc(dataLen);
          
              //remplissage du buffer de sortie
              for (let i = 0, 
                       len = data.length, 
                       pos = 0; 
                    i < len; i++) { 
                data[i].copy(buf, pos); 
                pos += data[i].length; 
              } 
          
              //constitution du zip en mémoire
              try{
                let zip = new AdmZip(buf);
                zip.extractAllTo(destination,true);
                if(success){ success();}
              }catch(e){
                console.error(`Erreur lors de la décompression de l'archive ${url} dans le répertoire ${destination}: ${e.name} - ${e.message}`);
                if (error) error(err.message);
              }
            });
          }).on("error", function(err) { // erreur lors du téléchargement
            fs.rmSync(destination, { recursive: true });
            dialog.showErrorBox('Erreur de téléchargement','la connexion au serveur a échouée merci de réessayer');
            console.error(`Erreur lors du téléchargement de l'url ${url} : ${err.name} - ${err.message}`)
            if (error) error(err.message);
    });
    }


    /**
     * Téléchargement d'un json
     * @param {*} url chemin du json
     * @param {*} onSuccess call back method en cas de succes (prends paramètre le json téléchargé)
     * @param {*} onError call back method en cas d'erreur (prends paramètre le json téléchargé)
     */
    getJson(url, onSuccess, onError) {
       
            this.client.get(url, function(res){
                let body = "";

                res.on("data", function(chunk){
                    body += chunk;
                })
                res.on("error", function(error) {
                    console.error(`Erreur lors du chargement du json à l'url : ${url} : ${error.name} - ${error.message}`);
                    if (onError) {
                        onError(error.message);
                    }
                })
                res.on("end", function(){
                    try {
                        let json = JSON.parse(body);
                        onSuccess(json);
                    } catch (error){
                        console.error(`Erreur lors du parsing du json à l'url : ${url} : ${error.name} - ${error.message}`);
                        if (onError) {
                            onError(error.message);
                        }
                    }
                })
            }).on("error",function(error) {
                console.error(`Erreur lors de la connexion à l'url : ${url} : ${error.name} - ${error.message}`);
                if (onError) {
                    onError(error.message);
                }
            });
    }

    getFilePath(url, onSuccess, onError) {
        this.client.get(url, function(res){
            let body = "";
    
            res.on("data", function(chunk){
                body += chunk;
            })
            res.on("error", function(error) {
                console.error(`Erreur lors du chargement de la donnée à l'url : ${url} : ${error.name} - ${error.message}`);
                if (onError) {
                    onError(error.message);
                }
            })
            res.on("end", function(){
                try {
                    let filePath = body;
                    onSuccess(filePath);
                } catch (error){
                    console.error(`Erreur lors du chargement de la donnée à l'url : ${url} : ${error.name} - ${error.message}`);
                    if (onError) {
                        onError(error.message);
                    }
                }
            })
        })
    }

    /**
     * Upload d'un zip
     * @param {*} buffer 
     * @param {*} filename 
     * @param {*} url 
     * @param {*} onSuccess 
     * @param {*} onError 
     */
    uploadZipFile(buffer, filename, url, onSuccess, onError) {

        //création d'un stream multipart/form-data à partir du buffer
        let form = new FormData();
        form.append('workspace', buffer, { filename: filename + '.zip' });

        //l'appel post vers le webservice
        form.submit(url, (err, res) => {
            if(err) {
                dialog.showErrorBox("Erreur d'upload","la connexion au serveur a échoué merci de réessayer");
            }

            res.resume();

            res.on("error", function (error) {
                console.error(`Erreur lors du chargement de la donnée à l'url : ${url} : ${error.name} - ${error.message}`);
                dialog.showErrorBox("Erreur d'upload","la connexion au serveur a échoué merci de réessayer");
                if (onError) {
                    onError(error.message);
                }
            });

            res.on('end', function () {
                try {
                    onSuccess();
                } catch (error) {
                    console.error(`Erreur lors du chargement de la donnée à l'url : ${url} : ${error.name} - ${error.message}`);
                    if (onError) {
                        onError(error.message);
                    }
                }
            });

        })

    }
}

export default HttpClient;


