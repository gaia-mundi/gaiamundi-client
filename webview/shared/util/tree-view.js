import { html, css, LitElement } from '../../../webview/lib/lit-element.js';
import '../../shared/component/icon/faicon.js';

/**
 * Composant affichant les composant HTML en fonction de la navigation
 *
 * @class TreeView
 * @extends {LitElement}
 */
class TreeView extends LitElement {
    static selectedElement;
    static selectedItem;

    static get properties() {
        return { 
        displayRoot : { attribute: true, type : Boolean /*display a root folder*/ },
        folderSelectable : { attribute: true, type : Boolean},
        data : { attribute: false, reflect : true /*contain tree view data*/ },
      }
    }

   /**
   * Style du composant
   *
   * @readonly
   * @static
   * @memberof LitElement
   */
   static get styles() {
    return css`
		ul.root { padding : 0}
        ul { list-style: none;}
        li > fa-icon { vertical-align: middle; cursor : grab;}
        li > span { vertical-align: middle; cursor : pointer; padding: 2px 6px;margin-left:-6px}
        li > span.selected { color: var(--color-alt); background-color: var(--bg-color-alt) }
        li.close > ul {display : none;}
		/* scrollbar */
		:host::-webkit-scrollbar {
		width: 8px;
		height: 8px;
		background-color: transparent;
		visibility: hidden;
		}
		:host::-webkit-scrollbar-track {
		background-color: rgba(var(--neutral-1), .05);
		border-radius: 8px;
		}
		:host::-webkit-scrollbar-thumb {
		background-color: rgba(var(--neutral-1), .10);
		border-radius: 8px;
		}
		:host::-webkit-scrollbar-thumb:active,
		:host::-webkit-scrollbar-thumb:hover {
		background-color: rgba(var(--neutral-1), .20)
		}`;
  }

  toggleItem(icon){
    icon.parentElement.classList.toggle("close");
    icon.setAttribute("icon", icon.getAttribute("icon") === "folder" ? "folder-open" : "folder");
  }

  selectItem(text,item){
    let event;
    if(this.selectedItem === item){
      this.selectedItem = null;
      this.selectedElement.classList.remove("selected");
      this.selectedElement = null;
      event = new CustomEvent('item-selected');
    }else{
      this.selectedItem = item;
      if(this.selectedElement){
        this.selectedElement.classList.remove("selected");
      }
      this.selectedElement = text;
      this.selectedElement.classList.add("selected");
      event = new CustomEvent('item-selected', { detail: item.data ? item.data : item});
    } 

    this.dispatchEvent(event);
  }
  openRoot(label){
    if(label ==="Indicateurs"){
        return "open"
    }else{
      return "close"
    }
  }
  renderItem(item){

    let icon = item.items ? html`${item.label==="Indicateurs" ? html `<fa-icon icon="folder-open" @click="${e => this.toggleItem(e.target)}"></fa-icon>` : html `<fa-icon icon="folder" @click="${e => this.toggleItem(e.target)}"></fa-icon>`}`
                          : html``;
    let span = this.folderSelectable  || !item.items ? html`<span  @click="${e => this.selectItem(e.target, item)}">${item.label}</span>`
                                      : html`<span>${item.label}</span>`;

    return html`<li class=${this.openRoot(item.label)}>${icon}${span}
                ${item.items ? html`<ul>${item.items.map(i => this.renderItem(i))}</ul>`
                                :html``}
                </li>`;
  }
  /**
   * HTML du composant
   *
   * @return {*} 
   * @memberof LitElement
   */
  render() {
    return this.data 
          ? this.displayRoot ? html`<ul class="root">${this.renderItem(this.data)}</ul>`
                             : html`<ul class="root">${this.data.items.map(i => this.renderItem(i))}</ul>`
          : html``;
  }
}

customElements.define('tree-view', TreeView);
export default new TreeView();