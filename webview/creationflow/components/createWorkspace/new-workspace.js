import { LitElement, html, css } from '../../../lib/lit-element.js';
import mapListService from "../../../shared/service/mapService.js";
import workspacesService from '../../../shared/service/workspacesService.js';
const { env } = require('../../container/environments.js');
const path = require('path');
const fs = require('fs');
const apiWorkspaceModel = "api/workspaces/models/";
const apiWorkspace = "api/workspaces/";
const workspaceDir = env.get("workspace.location");
const app = window.require('@electron/remote');
const dialog = app.dialog;

class Neworkspace extends LitElement{
    
    /** Composant liste des cartes */
    maps = [];

    /** Composant modèles */
    workspacesModels = [];

        /**
     * Propriétés et attribut de l'élément
     */
    static get properties() {
        return {
            nameWorkspace: {type:String, attribute: false},
            nameMap: {type:String, attribute: false},
            file: {type:String, attribute: false}
        };
    }

    /**
     * Identification de la carte et chargement des données
     * @param {*} changedProperties proporiété modifiées 
     */
     firstUpdated(changedProperties){
        this._loadData();
     }

    /**
     * Récupère la liste des cartes, workspaces et modèles sur le serveur
     */
     _loadData() {
        mapListService.getMapList((data) => {          
          this.maps = data
          this.requestUpdate();
        }, (msg) => {
            console.error("Impossible de récupérer les fonds de carte sur le serveur : " + msg);
            this.maps = [];
            this.requestUpdate();
        })
        workspacesService.getWorkspacesModelsList((data) => {
            this.workspacesModels = data;
            this.requestUpdate();
        }, (msg) => {
            console.error("Impossible de récupérer les modèles sur le serveur : " + msg);
            this.workspacesModels = [];
            this.requestUpdate();
        })
    }
    
    /**
     * 
     * @param {*} e 
     * create workspace on form validation
     */
    _submitMapForm(selectedType, map, e) {
        if(this.nameMap){
            workspacesService.createWorkspace(selectedType, this.nameMap, map.name, map.maille, map.echelle,"../../webview/workspace/index.html?workspaceName=");
            e.preventDefault();
        }
    }

    _updateNameWorkspace(e) {
        this.nameWorkspace = e.target.value;
        this.requestUpdate();
    }

    _updateNameMap(e) {
        this.nameMap = e.target.value;
        this.requestUpdate();
    }

    _submitModelForm(selectedModelIndex,e) {
        if(this.nameWorkspace){
            workspacesService.createWorkspaceFromModel(this.nameWorkspace, apiWorkspaceModel+selectedModelIndex,"../../webview/workspace/index.html?workspaceName=");
        }
        e.preventDefault();
    }


     /** Affichage */
    render(){
        return html`
        <div class="row">
            <div class="form-card">
                <h1>A partir d'un modèle</h1>
                <div class="name-selector">
                    <input type="text" id="name-workspace" name ="name" required placeholder="nom du nouvel espace de travail" @change="${this._updateNameWorkspace}">
                </div>
                <div class="card1">
                    Import depuis votre poste
                    <select-file url="../../webview/workspace/index.html?workspaceName=" name="${this.nameWorkspace}" type="espace de travail"></select-file>
                </div>
                <h2>Modèles sur le serveur</h2>
                <div class="model-container">
                    ${this.workspacesModels.map((element, i) =>
                    html`
                    <div class="card1" @click="${(e) => this._submitModelForm(element.name,e)}">
                        <div class="title">${element.title}</div>
                        <div class="property">
                            <div class="property-label">Echelle</div>
                            <div class="property-value">${element.echelle}</div>
                        </div>
                        <div class="property">
                            <div class="property-label">Maille</div>
                            <div class="property-value">${element.maille}</div>
                        </div>
                </div>
                `)}
                </div>
            </div>
            <div class="form-card">
                <h1>A partir d'un fond de carte</h2>
                <div class="name-selector">
                    <input type="text" id="name-workspace" name ="name" required placeholder="nom du nouvel espace de travail" @change="${this._updateNameMap}">
                </div>
                <div class="card1">
                    Import depuis votre poste
                    <select-file url="../../webview/workspace/index.html?workspaceName=" name="${this.nameMap}" type="fond de carte"></select-file>
                </div>
                <h2>Fond de carte sur le serveur</h2>
                <div class="model-container">
                    ${this.maps.map((element, i) =>
                    html`
                    <div class="card1" @click="${(e) => this._submitMapForm("serverMap", element,e)}">
                        <div class="title">${element.title}</div>
                        <div class="property">
                            <div class="property-label">Echelle</div>
                            <div class="property-value">${element.echelle}</div>
                        </div>
                        <div class="property">
                            <div class="property-label">Maille</div>
                            <div class="property-value">${element.maille}</div>
                        </div>
                        <div class="property">
                            <div class="property-label">Auteur(s)</div>
                            <div class="property-value">${element.authors}</div>
                        </div>
                    </div>
                    `)}
                </div>
            </div>
        </div>
       `;
    }

    static get styles()
    {
        return css`
        :host{
            display : flex;
            flex-direction: column;
            justify-content: center;
            background-position: center;
            background-size: cover;
            background-repeat: no-repeat;
        }

        row h1, .row h2{
            font-size: var(--font-size-header1);
            color : var(--color-alt3);
            font-weight: bold;
            margin : 0px;
        }

        .row h2{
            font-size: var(--font-size-header2);
            margin : 0;
        }

        div.card1 {
            margin : 20px;
            cursor: pointer;
            transition: all 0.3s cubic-bezier(0.17, 0.67, 0.35, 0.95);
            height : 100px;
            width:400px;
            padding : 20px;
            justify-content: center;
            align-items: center;
            text-align: center;
            opacity: 0.8;
            border : var(--border-width1) solid var(--border-color1);
            font-size: var(--font-size-header2);
            background-color: var(--bg-color-alt5);
            color : var(--color-alt2);
        }

        div.card1:hover {
            border-color : transparent;
        }

        div.title{
            width:100%;
            text-overflow: ellipsis;
            overflow: hidden;
            white-space: nowrap;
            width: 100%;
        }

        div.property{
            width:100%;
            display : flex;
        }

        div.property-label{
            font-size: var(--font-size-body1);
            text-align: left;
            font-weight : bold;
            width : 105px;
        }

        div.property-value{
            font-size: var(--font-size-body1);
            text-align: left;
            text-overflow: ellipsis;
            overflow: hidden;
            white-space: nowrap;
        }


        .row {
            display: flex;
            flex-direction: row;
            justify-content: space-evenly;
        }
        
        .form-card {
            display : flex;
            flex-direction: column;
            width: 50%;        
            padding-left: 10px;
        }
        
        input[type="text"] {
            box-sizing: border-box;
            width: 80%;
            padding: 0;
            height: 40px;
            border: 2px solid black;
            border-radius: 5px;
            margin : 5px;
            padding-left : 10px;
        }

        div.model-container{
            height:calc(100% - 350px);
            overflow: hidden scroll;
        }

        div.model-container::-webkit-scrollbar-track{
            padding: 2px 0;
            background-color: transparent;
        }
        div.model-container::-webkit-scrollbar{
            width: 5px;
        }

        div.model-container::-webkit-scrollbar-thumb{
            box-shadow: inset 0 0 6px var(--border-color1);
            background-color: #404040;
            border: 1px solid #000;
        }
        `;
    }
}

customElements.define("new-work-space", Neworkspace);
export default Neworkspace;
