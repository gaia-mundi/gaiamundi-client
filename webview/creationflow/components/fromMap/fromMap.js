import { LitElement, html, css } from '../../../lib/lit-element.js';
import mapListService from "../../../shared/service/mapService.js";
import workspacesService from '../../../shared/service/workspacesService.js';
const { env } = require('../../container/environments.js');
const path = require('path');
const fs = require('fs');
const apiWorkspaceModel = "api/workspaces/models/";
const apiWorkspace = "api/workspaces/";
const workspaceDir = env.get("workspace.location");
const app = window.require('@electron/remote');
const dialog = app.dialog;

class Neworkspace extends LitElement{
    
    /** Composant liste des cartes */
    maps = [];
    /** Composant workspace */
    workspaces = [];
    /** Composant modèles */
    workspacesModels = [];
    /** liste des workspaces sur le client */
    recentWorkspaces = [];

        /**
     * Propriétés et attribut de l'élément
     */
    static get properties() {
        return {
            nameWorkspace: {type:String, attribute: false},
            nameMap: {type:String, attribute: false},
            file: {type:String, attribute: false}
        };
    }

    /**
     * Identification de la carte et chargement des données
     * @param {*} changedProperties proporiété modifiées 
     */
     firstUpdated(changedProperties){
        this._loadData();
        this.recentWorkspaces = workspacesService.listWorkspacesOnClient();
     }

    /**
     * Récupère la liste des cartes, workspaces et modèles sur le serveur
     */
     _loadData() {
        mapListService.getMapList((data) => {          
          this.maps = data
          this.requestUpdate();
        }, (msg) => {
            console.error("Impossible de récupérer les fonds de carte sur le serveur : " + msg);
            this.maps = [];
            this.requestUpdate();
        })
        workspacesService.getWorkspacesModelsList((data) => {
            this.workspacesModels = data;
            this.requestUpdate();
        }, (msg) => {
            console.error("Impossible de récupérer les modèles sur le serveur : " + msg);
            this.workspacesModels = [];
            this.requestUpdate();
        })
        workspacesService.getWorkspacesList((data) => {
            this.workspaces = data;
            this.requestUpdate();
        }, (msg) => {
            console.error("Impossible de récupérer les workspaces sur le serveur : " + msg);
            this.workspaces = [];
            this.requestUpdate();
        })
    }
    
    /**
     * 
     * @param {*} e 
     * create workspace on form validation
     */
    _submitMapForm(e) {
        let selectedMapIndex = this.shadowRoot.querySelector('input[name="map"]:checked').value;
        let selectedType = this.shadowRoot.querySelector('input[name="map"]:checked').className;
        this.nameMap = this.shadowRoot.getElementById("name-map").value;

        workspacesService.createWorkspace(selectedType, this.nameMap, this.maps[selectedMapIndex].name, this.maps[selectedMapIndex].maille, this.maps[selectedMapIndex].echelle);
        e.preventDefault();
    }

    _updateNameWorkspace(e) {
        this.nameWorkspace = e.target.value;
        this.requestUpdate();
    }

    _updateNameMap(e) {
        this.nameMap = e.target.value;
        this.requestUpdate();
    }

    _submitModelForm(e) {
        let selectedModelIndex = this.shadowRoot.querySelector('input[name="model"]:checked').value;
        this.nameWorkspace = this.shadowRoot.getElementById("name-workspace").value;

        workspacesService.createWorkspaceFromModel(this.nameWorkspace, apiWorkspaceModel+selectedModelIndex);
        e.preventDefault();
    }

    _existingWorkspace(e) {
        const index = e.target.id;
        const dir = workspaceDir + "/" + index;

        // creation du repertoire du workspace
        if (!fs.existsSync(dir)){
            workspacesService.downloadZipFile(apiWorkspace+index, dir);
            setTimeout(() => {
                window.location.href="../../webview/workspace/index.html?workspaceName="+index;
            }, 1000)
        }
        else {
            dialog.showErrorBox('Espace existant','cet espace existe déjà dans votre application');
        }
    }

    _launchWorkspace(e) {
        let selectedModelIndex = this.shadowRoot.querySelector('input[name="workspace"]:checked').value;
        window.location.href="../../webview/workspace/index.html?workspaceName="+selectedModelIndex;
    }

    _deleteWorkspace(e) {
        const workspaceChecked = e.target.value;
        let options  = {
            buttons: ["Oui", "Annuler"],
            message: "Voulez-vous supprimer cet espace de travail?"
        }
        app.dialog.showMessageBox(options).then(r => {
            if(r.response === 0) {
                fs.rmSync(path.resolve(workspaceDir, workspaceChecked), { recursive: true });
                location.reload();
            }
        }) ;
    }
    /**
     * créer une archive d'un workspace
     * @param {*} e 
     */
    _zipWorkspace(e) {
        let nom = e.target.value;
        workspacesService.zipWorkspace(nom);
    }

     /** Affichage */
    render(){
        return html`
        <div class="row">
            <div class="form-card"> 
                <h2>Espaces récents</h2>
                <div class="form-body lastworkspaces">
                ${this.recentWorkspaces.map((element, i) => 
                    html`
                    <div class="object-card">
                        <label>
                        <input type="radio" id="${i}" name="workspace" value="${element.name}" @click="${this._launchWorkspace}">
                            <p>workspace ${element.title}</p>
                                <ul>
                                    <li>echelle : ${element.echelle}</li>
                                    <li>maille : ${element.maille}</li>
                                    <li>dernier accès : ${element.accessTime}</li>
                                </ul>
                        </label>
                        <button class="delete" value="${element.name}" @click="${this._deleteWorkspace}">Supprimer cet espace</button>
                    </div>
                    `)}
                </div>
            </div>
            <div class="form-card">
                <h2><span style="color:limegreen;">Nouvel espace</span> depuis un modèle ou utiliser un espace existant</h2>
                <div class="form-body">
                    <form id="modelForm" name="modelForm" >
                        <label class="name" for="name-workspace">Choisissez le nom de votre espace de travail : </label>
                        <input type="text" id="name-workspace" name ="name" required placeholder="mon espace de travail" @change="${this._updateNameWorkspace}">
                        <h3>Modèle d'espace de travail</h3>
                        <div class="form-body models">
                        ${this.workspacesModels.map((element, i) =>
                            html`
                            <div class="object-card">
                                <label>
                                    <input type="radio" id="${i}" name="model" class="zipMap" value="${element.name}" required @click=${this._submitModelForm}>
                                    <p>modèle ${element.title}</p>
                                    <ul>
                                        <li>echelle : ${element.echelle}</li>
                                        <li>maille : ${element.maille}</li>
                                    </ul>
                                </label>
                            </div>
                            `)}
                        </div>
                    </form>
                </div>
                <h2><span style="color:royalblue;">Utiliser</span> un espace de travail du serveur</h2>
                ${this.workspaces.map((element, i) =>
                    html`
                    <div class="object-card">
                        <label>
                            <input type="radio" id="${element.name}" name="existingworkspace" class="existingworkspace" value="${element.name}" required @click=${this._existingWorkspace}>
                            <p>workspace ${element.title}</p>
                            <ul>
                              <li>title : ${element.name}</li>
                            </ul>
                        </label>
                    </div>
                `)}
                <h2><span style="color:orange;">Importer</span> un espace de travail</h2>
                <div class="object-card">
                    <select-file name="${this.nameWorkspace}" type="espace de travail"></select-file>
                </div>
            </div>
            <div class="form-card">
                <h2><span style="color:limegreen;">Nouvel espace</span> à partir d'un fond de carte</h2>
                <div class="form-body">
                    <form id="myForm" name="myForm">
                        <label class="name" for="name-map">Choisissez le nom de votre espace de travail : </label>
                        <input type="text" id="name-map" name ="name" required placeholder="mon espace de travail" @change="${this._updateNameMap}">
                        <h3>Fonds de carte du serveur</h3>
                        <div class="form-body maps">
                        ${this.maps.map((element, i) =>
                            html`
                            <div class="object-card">
                                <label>
                                <input type="radio" id="${i}" name="map" class="serverMap" value="${i}" required @click="${this._submitMapForm}">
                                <p>${element.title}</p>
                                <ul>
                                    <li>code maille : ${element.codeMaille}</li>
                                    <li>echelle : ${element.echelle}</li> 
                                    <li>maille : ${element.maille}</li> 
                                    <li>auteur(s) : ${element.authors}</li>
                                </ul>
                                </label>
                            </div>
                        `)}
                        </div>
                    </form>
                    <div class="form-body maps import">
                        <h3><span style="color:orange;">Importer</span> un fond de carte</h3>
                        <div class="object-card">
                            <select-file name="${this.nameMap}" type="fond de carte"></select-file>
                        </div>
                    </div>
                </div>

            </div>
        </div>
       `;
    }

    static get styles()
    {
        return css`
        :host{
            display : flex;
            flex-direction: column;
            justify-content: center;
            background-image: url(../../webview/assets/img/world-map.png);
            background-position: center;
            background-size: cover;
            background-repeat: no-repeat;
        }
        .row {
            display: flex;
            flex-direction: row;
            justify-content: space-evenly;
        }
        
        h1, h2, h3 {
            text-align: center;
        }
        
        .form-card {
            display : flex;
            flex-direction: column;
            width: 30%;        
            margin: 48px 0;
            padding: 16px;
        }

        .form-body {
            border: 2px solid black;
            border-radius: 5px;
            padding: 16px;
            overflow-y: scroll;
            overflow-x: hidden;
        }
        .form-body.models, .form-body.maps, .form-body.lastworkspaces {
            border: none;
            padding-top: 0;
            max-height: 200px;
        }        
        .form-body.maps {
            max-height: 423px;
        }
        .form-body.lastworkspaces {
            max-height: 728px;
        }
        .form-body.import {
            overflow-y: unset;
        }
        
        .form-body::-webkit-scrollbar-track {
          padding: 2px 0;
          background-color: transparent;
        }
        .form-body::-webkit-scrollbar {
          width: 5px;
        }
        .form-body::-webkit-scrollbar-thumb {
            box-shadow: inset 0 0 6px rgba(0,0,0,.3);
            background-color: #404040;
            border: 1px solid #000;
        }
        
        .object-card {
            display : flex;
            flex-direction: column;     
            padding: 16px;
            border: 2px solid black;
            border-radius: 5px;
            margin-bottom : 8px;
        }
        
        label.name {
            font-weight: bold;
        }
        
        input[type="text"] {
            box-sizing: border-box;
            width: 100%;
            padding: 0;
            height: 40px;
            border: 2px solid black;
            border-radius: 5px;
        }
        input[type="radio"] {
            display: none;
        }
        
        label {
            cursor: pointer;
        }
         label:hover p {
            transform: scale(1.3) translateX(11%);
            font-weight: bold;
         }
         label:active p {
            color: #ffd700;
         }
         
        .ul {
            display: flex;
            flex-direction: column;
            justify-content: center;
            margin-top: 4px;
        }
        
        .delete:hover {
            background-color: red;
            color: #ffffff;
           
        }
        `;
    }
}

customElements.define("new-work-space", Neworkspace);
export default Neworkspace;
