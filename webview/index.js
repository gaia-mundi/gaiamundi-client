const config = require('./../container/config.js')

let newWorkspaceElement = document.getElementById('newWorkspace');
let loadWorkspaceElement = document.getElementById('loadWorkspace');
let messageElement = document.getElementById('message');


if(!config.isWorkspaceFolderPathsDefined()){
    newWorkspaceElement.onclick = ()=>{ messageElement.innerHTML = ' <p> Veuillez remplir les configurations obligatoires dans Paramètres</p>'};
    loadWorkspaceElement.onclick = ()=>{ messageElement.innerHTML = ' <p> Veuillez remplir les configurations obligatoires dans Paramètres</p>'};
}