const log = require('electron-log');
const { env } = require('./environments');
const path = require('path');
const rootPath = require('electron-root-path').rootPath

/*
Récupration de la configuration des logs
et définition du niveau de log ainsi que la localisation du fichier de log
*/
log.transports.file.level =  env.get("log.level");
log.transports.console.level = env.get("log.level");;
log.transports.file.resolvePath = () => path.join(rootPath, env.get("log.file.location"));

/**
 * Par défault tous les logs sont warning
 */
if(!log.transports.file.level){
    log.transports.file.level = "warn";
    log.transports.console.level = "warn";
}
Object.assign(console, log.functions)

exports.log = log;