const { env }  = require('./environments.js');
const rootPath = require('electron-root-path').rootPath;
const path = require('path');

let propertiesPath =  path.join(rootPath, 'application.properties');

/**
 * vérifier que l'url vers l'espace de travail est definie ainsi que l'url vers l'espace de travail backup
 */
exports.isWorkspaceFolderPathsDefined = function (){
    let workspacePath = this.workspaceFolderPath(); 
    return workspacePath ? true : false;
}

/**
 * récupérer  l'url vers l'espace de travail
 */
exports.workspaceFolderPath = function (){
    return env.get('workspace.location') ;
}

/**
 * récupérer l'url  du serveur
 */
exports.serverUrl = function (){
    return env.get('server.url') ;
}

/**
 * mettre à jour le l'url vers l'espace de travail
 * @param {*} value la nouvelle valeur
 */
exports.setWorkspacePath = function (value){
    env.set('workspace.location',value);
    env.save(propertiesPath);
}

/**
 * mettre à jour le l'url du serveur
 * @param {*} value la nouvelle valeur
 */
exports.setServerUrl = function (value){
    env.set('server.url',value);
    env.save(propertiesPath);
}

/**
 * récupérer  l'url vers l'espace de travail
 */
 exports.mode = function (){
    return env.get('workspace.user.profile') ;
}

/**
 * récupérer l'url  du serveur
 */
exports.setMode = function (value){
    env.set('workspace.user.profile',value);
    env.save(propertiesPath);
}
 
