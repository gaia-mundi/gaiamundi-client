const { app, BrowserWindow, dialog } = require('electron');
const path = require('path');
const { env } = require('./container/environments');
const {initialize, enable } = require('@electron/remote/main');
const PROTOCOL = "gaiamundi";

let mainWin;

// Initialisation du module Remote pour pouvoir y accéder depuis les pages web
initialize();

const displayPageFromUrl = function(url){
  let workspaceName = url.substring(PROTOCOL.length+3);
  if(workspaceName.length > 2){
    mainWin.loadFile("webview/workspace/index.html",{ query: { workspaceName: workspaceName}});
  }else{
    mainWin.loadFile('webview/index.html');
  }
}
if (process.defaultApp) {
  if (process.argv.length >= 2) {
    app.setAsDefaultProtocolClient(PROTOCOL, process.execPath, [path.resolve(process.argv[1])]);
  }
} else {
  app.setAsDefaultProtocolClient(PROTOCOL);
}

const gotTheLock = app.requestSingleInstanceLock()

if (!gotTheLock) {
  app.quit()
}else{
  app.on('second-instance', (event, argv) => {
    if (process.platform !== 'darwin') {
      // Find the arg that is our custom protocol url and store it
      displayPageFromUrl(argv.find((arg) => arg.startsWith(PROTOCOL + '://')));
    }
    // On essaye d'ouvrir une seconde instance.Donc on met en avant notre fenêtre
    if (mainWin) {
      if (mainWin.isMinimized()) { mainWin.restore();} 
      mainWin.focus();
    }
  });

  // Protocol handler for osx
  app.on('open-url', function (event, url) {
    event.preventDefault();
    displayPageFromUrl(url);
  });

  /**
   * Création de la fenêtre principale de l'application
   */
  const createWindow = () => {
    mainWin = new BrowserWindow({
      width: 800,
      height: 600,
      icon: __dirname + '/webview/assets/img/logo.ico',
      webPreferences: {
        nodeIntegration: true,
        contextIsolation: false
      },
      autoHideMenuBar: true
    })

    if (env.get('app.debug')) {
      mainWin.webContents.openDevTools();
    }

    setProxy(mainWin);
    
    enable(mainWin.webContents);

    //appel depuis une url
    const deepLinkUrl = process.argv.find((arg) => arg.startsWith(PROTOCOL + '://'));
    if(deepLinkUrl){
      displayPageFromUrl(deepLinkUrl);
    }else{
      mainWin.loadFile('webview/index.html');
    }
    
    
  }

  /**
   * Definition d'un proxy
   * @param {*} session brower courant
   */
  const setProxy = (session) => {
    let proxy = env.get("proxy");
    if(proxy){
      session.setProxy(proxy);
    }
  }

  app.whenReady().then(() => {
    createWindow()
  })
}

