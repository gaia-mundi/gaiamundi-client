# GaïaMundi – Client

La solution GaïaMundi est une solution client-serveur.

Le client est une application Electron JS qui donne accès à l’ensemble
des fonctionnalités de l’application et la partie serveur est un serveur
headless développé en NodeJS.

Cette solution ne fait pas intervenir de base de données l’ensemble des
éléments sont persistés sur fichier que ce soit coté serveur ou coté
client lourd (Electron JS).

Les communications entre client et serveur se font via des API REST au
format JSON.

Pour consulter la partie serveur, veuillez vous rendre au dépôt suivant : [https://gitlab.com/gaia-mundi/gaia-mundi-server](https://gitlab.com/gaia-mundi/gaia-mundi-server)


## Développement

Le client est un client lourd développé en utilisant
[Electron](https://www.electronjs.org/). Il permet aux utilisateurs de
construire des cartes interactives à travers des espaces de travail.

### Pré-requis

- NodeJS v16 : [https://nodejs.org/en/download/](https://nodejs.org/en/download/)
- NPM (Node Package Manager)

### Installation

Il faut tout d'abord installer les dépendences du projet à travers la commande suivante :

```sh
npm install
```

### Démarrer le serveur locale

Pour démarrer l'application en mode développement, utilisez la commande :

```sh
npm run start
```

Afin d'ouvrir l'outil de débogage "DevTools", assurez vous que la variable d'environement `app.debug` est égale à `true`.

### Compilation

Le packaging de l’application utilise la librairie
[electron-packager](https://github.com/electron/electron-packager), les
commandes suivantes sont définies :

- `npm run package-win` pour packager la solution pour Windows,

- `npm run package-linux` pour packager la solution pour des
  environnement Linux

## Structure du code

Le fichier principal est le fichier `index.js`

Le reste du code est séparé en deux parties :

- le répertoire _container_ qui contient toute la définition de
  l’environnement d’exécution NodeJS,

- le répertoire _webview_ qui contient l’ensemble du code HTML/CSS/JS
  gérant les écrans et services de l’application.

La fonction de lancement principale du fichier est la méthode
createWindow, celle-ci est en charge de lancer le navigateur Chromium
intégré à Electron et de charger la page initiale de l’application
_webview/index.html_.

## Le conteneur

La définition des éléments du conteneur NodeJS sont définis dans le
répertoire _container_.

Celui-ci met à disposition les fichiers :

- _config.js_, ce module est en charge de la récupération et
  définition de paramètres modifiables via l’application (url du
  serveur, répertoire racine des espaces de travail, mode de
  fonctionnement)

- _environnement.js_, ce module est en charge de récupérer l’ensemble
  des variables d’exécution de l’application. Pour cela, il exporte
  l’objet env qui est un
  [PropertiesReader](https://github.com/steveukx/properties#readme).
  Cet objet permet de récupérer via leur identifiant
  (env.get(variableId)) la valeur d’une variable. Cette valeur est
  définie en regardant en priorité dans :

  1.  les paramètres d’exécution du client

  2.  le fichier _application.properties_ qui contient l’ensemble des
      paramètres. Le format de ce fichier est du type .ini. Voir le
      paragraphe Paramétrage de l’application (application.properties)

<!-- -->

- _log.js_ défini un objet log prenant en compte tous les paramètres
  définis dans la configuration de l’application.

## L’interface utilisateur

L’interface utilisateur est définie dans le répertoire _webview_.

Pour définir l’ensemble des écrans, le standard [Web
Components](https://developer.mozilla.org/fr/docs/Web/Web_Components)
est utilisé, il permet d’assurer une autonomie à chaque composant
graphique et assure l’évolutivité de chacun indépendamment de leur
développements. En premier intention, nous utilisons la librairie
[Lit](https://lit.dev/) pour faciliter les implémentations.

Pour la gestion du SVG, l’application utilise également la librairie
[Snap.svg](http://snapsvg.io/) pour simplifier les développements.

## Structure du code

La structure du code est la suivante :

- le répertoire principal contient le fichier index.html, index.js et
  l’ensemble des répertoire correspondant à chaque grande partie de
  l’application

- le répertoire _assets_ contient les css et image utilisé par la page
  principale. Chaque fois que nous auront une sous page ou un
  composant nécessitant ce type de fichier de manière spécifique on
  ajoute un fichier ayant ce nom.

- Le répertoire _shared_ contient tous les éléménets partageable pour
  l’ensemble des pages de l’application (services métiers, composant,
  …)

- Le répertoire lib contient toutes les librairies nécessaire au
  fonctionnement des pages HTML.

D’une manière générale, le fonctionnement est le suivant :

- On décrit une page HTML qui inclue un ou plusieurs modules
  Javascript

- Les modules JavaScript décrivent des composants custom et font appel
  à une couche service pour exécuter les actions (communication avec
  le serveur, accès au disque local, …)

Cette structure interdit qu’un composant (couche de présentation) accède
directement à un élément de stockage ou à la partie serveur.

## Les pages principales de l’application

Les pages principales de l’application sont :

- _index.html_, page d’accueil donnant les principaux choix
  fonctionnels

- _creationflow/index.html_, page de création d’un nouvel espace de travail

- _parameters/index.html_, page de gestion des paramètres de
  l’application

- _workspace/index.html_, page principale affichant les cartes et
  menus d’un espace de travail

- _workspace/list-index.html_, page de sélection d’un espace de
  travail

- _workspace/indicators.html_, page de gestion des indicateurs d’un
  espace de travail

- _workspace/layer-creation.html_, page de création d’une nouvelle
  couche pour l’esapce de travail

## Les éléments communs à l’ensemble de l’application

Ces éléments sont contenus dans le répertoire _shared_

### Les composant web

L’application définit les composant techniques suivant dans le
répertoire _shared/component_

<table>
    <colgroup>
        <col style="width: 24%" />
        <col style="width: 24%" />
        <col style="width: 51%" />
    </colgroup>
    <thead>
        <tr class="header">
            <th><strong>Répertoire</strong></th>
            <th><strong>Tag Name</strong></th>
            <th><strong>Description</strong></th>
        </tr>
    </thead>
    <tbody>
        <tr class="odd">
            <td>icon</td>
            <td>fa-button<br />
            fa-icon</td>
            <td>Affichage d’un bouton avec un icone.<br />
            Affichage d’un icone.</td>
        </tr>
        <tr class="even">
            <td>mapList</td>
            <td>map-list</td>
            <td>Affichage de la liste des fonds de carte sur le serveur</td>
        </tr>
        <tr class="odd">
            <td>menu</td>
            <td>menu-main<br />
            menu-item</td>
            <td>Barre de menu principale<br />
            Elément de menu.</td>
        </tr>
        <tr class="even">
            <td>selectdir</td>
            <td>select-directory</td>
            <td>Sélection d’un répertoire local</td>
        </tr>
        <tr class="odd">
            <td>selectfile</td>
            <td>select-file</td>
            <td>Sélection d’un fichier local</td>
        </tr>
        <tr class="even">
            <td>slider</td>
            <td>multiple-slider</td>
            <td>Gestion d’un règle de sélection multi intervalles</td>
        </tr>
    </tbody>
</table>

### Les services

Le répertoire _shared/service_ définit l’ensemble des services à
destination de chaque entité manipulée.

- _countour-services.js_, exporte un singleton gérant les fonctions
  suivantes :

  - getContourList : récupération de la liste des couches contours
    sur le serveur correspondant à une maille et une échelle

  - getContourZip : récupération de l’archive d’une couche contour
    disponible sur le serveur.

- _data-set-services.js_, exporte un singleton gérant les fonctions
  suivantes :

  - getDataSetList : récupération de la liste des jeux de données
    sur le serveur correspondant à une maille et une échelle

  - getDataSetInfo : récupération des informations relatives à un
    jeux de données local (voir §Structure des jeux de données,
    _data-set-info.json_)

  - getDataSetListForLocalWorkspace : récupération des jeux de
    données disponibles dans l’espace de travail courant

  - getDataSetZip : récupération de l’archive d’un jeux de données
    disponible sur le serveur.

  - dataSetTextFileToJsonFile : transformation d’un jeux de donnée
    au format CSV (voir §Structure des jeux de données, _data.txt_),
    en un format json local. Cette méthode est utilisé lors du
    chargement d’un jeux de données depuis le serveur (composant
    data-set-list)

- _layerServices.js_, exporte un singleton gérant les fonctions
  suivantes :

  - calculateData: récupération du jeu de données associé à une
    couche

  - importEdgeLayer: Ajout dans l’espace de travail d’une nouvelle
    couche contour

  - getHistogramData: récupération des données associée à un
    histogramme

  - getHistogramColors: récupération des couleur d’un histogramme.

  - sortLayerList : tri des couches en fonction de leur type.
    Utilisé pour le mode « basic ».

- _mapServices.js_, exporte un singleton gérant les fonctions
  suivantes :

  - getMapList: récupération de la liste des fonds de cartes
    disponible sur le serveur

  - downloadMapZipFile: récupération d’un fond de carte depuis le
    serveur

  - convertGeoJsonMapToGaiaMundiMap: converti le format GeoJSON en
    un format GaïaMundi qui prédéfini le dessin des zones au format
    SVG et calcule les centroides pour chaque zone.

  - listZipMapOnClient: Récupération de la liste des fonds de carte
    présents sur le PC client.

- _workspacesServics.js_, exporte un singleton gérant les fonctions
  suivantes :

  - getWorkspacesList: récupération de la liste des espace de
    travail disponibles sur le serveur

  - getWorkspacesModelsList: récupération de la liste des templates
    disponible sur le serveur.

  - queryWorkspaceName: récupération de l’identifiant de l’espace de
    travail courant

  - getWorkspaceById: récupération d’un espace de travail local .

  - saveWorkspace: sauvegarde d’un espace de travail en local

  - loadWorkspaceScenario : chargement du scénario associé à un
    espace de travail local

  - saveWorkspaceScenario : sauvegarde du scénario associé à un
    espace de travail en local

  - loadBackgroundDefinition : récupération de la définition d’un
    fond de carte pour un espace de travail local (ficher de donnée
    associé)

  - loadLayerDefinition : chargement de la définition d’une couche
    (fichier de données associé) pour un espace de travail local.

  - listWorkspacesOnClient : liste des espaces de travail disponible
    en local

  - createWorkspace : création d’un nouvel espace de travail à
    partir d’un fond de carte.

  - createWorkspaceFromModel : création d’un nouvel espace de
    travail à partir d’un template

  - uploadWorkspace : chargement d’un espace de travail local sur le
    serveur

  - deleteBackupWorkspace : suppression de la sauvegarde d’un espace
    de travail.

  - backupWorkspace : création d’une sauvegarde d’un espace de
    travail.

  - loadBackupWorkspace ; chargement de la sauvegarde d’un espace de
    travail.

  - listZipWorkspaceOnClient : liste des archives d’espace de
    travail disponible en local

### Les utilitaires

La liste de utilitaires sont regroupés dans le répertoire _shared/util_.

<table>
<colgroup>
<col style="width: 23%" />
<col style="width: 76%" />
</colgroup>
<thead>
<tr class="header">
<th>Fichier</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><em>Client.js</em></td>
<td>Classe HttpClient dont hérite toute classes désirant communiquer
avec le serveur</td>
</tr>
<tr class="even">
<td><em>Css-utils.js</em></td>
<td><p>Modification des règles CSS via Javascript</p>
<p>Conversion de couleur Hex HSL</p></td>
</tr>
<tr class="odd">
<td><em>File.js</em></td>
<td>Chargement de fichier JSON</td>
</tr>
<tr class="even">
<td><em>IconTypeConstants.js</em></td>
<td>Définition des types de couches de données</td>
</tr>
<tr class="odd">
<td><em>Snap-z-index.js</em></td>
<td>Plugin Snap pour gérer l’ordre des couches dans une carte au format
SVG</td>
</tr>
<tr class="even">
<td><em>Tree-view.js</em></td>
<td>Composant web permet d’afficher une arborescence</td>
</tr>
</tbody>
</table>

## Gestion d’un espace de travail.

Comme préciser le fichier principal est le fichier workspace/index.html.

Celui-ci est comprend deux composant principaux :

- Workspace-header _(/workspace/components/page/workspace-header.js_)
  en charge de la barre d’en-tête de la page

- Workspace-page _(/workspace/components/page/workspace-page.js_),
  composant principal de l’application en charge de l’affichage du
  menu de gauche et de la carte. Il contient :

  - Le slider pour les couches de données (layer-slider -
    _(/workspace/components/layer-slide_).

  - Le composant qui gère la carte carte-svg
    (_/workspace/components/carte_).

  - Le menu de gauche workspace-side-menu
    (_/workspace/components/menu_).

### Le composant workspace-page

Ce composant est en charge de la gestion de l’espace de travail, il
assure :

- Les chargements et sauvegarde au niveau de l’espace

- La cohérence entre la carte et le menu par :

  - La définition et la mise en relation des différentes couches

  - La propagation des actions du menu sur la carte

Son fonctionnement est le suivant :

- La méthode firstUpdated charge la définition de l’espace de travail
  en commençant par charger le fond de carte qu’elle associe au menu
  et à la carte.

- Puis via la méthode \_loadWorkspaceDefinition, elle va charger les
  couches une à une dans le menu et la carte.

- La méthode refresh sert au rafraichissement de l’affichage pour la
  carte (modification de la définition écran) et du slider (largeur)

- Le mode « basic » nécessitant un affichage particulier (ordre des
  couches), on appelle la méthode reorganizeLayerForBasicUsage.

Les actions du menu devant être propager sur la carte ce composant offre
un ensemble de méthodes passerelles :

- toggleLayers : affichage/masquage d’une couche

- moveLayerToFront : déplacement d’une couche vers le premier plan

- moveLayerToBack : déplacement d’une couche vers l’arrière-plan

- updateLayerProperty : mise à jour des propriétés d’une couche

- activateLayer : définition d’un layer comme actif

Le composant gère également les sauvegardes de l’espace de travail :

- workspaceSavePositionUpdate : décalage de l’ordre des couches.

- workspaceSaveAttribute : modification d’attribut du workspace.

- saveWorkspace : sauvegarde globale du workspace.

### Gestion du menu de droite

Le menu de droite est géré par le composant workspace-sidemenu défini
dans le fichier _webview/workspace/menu/side-menu.js_.

Ce composant contient deux méthodes utilisées à l’initialisation :

- setBackground : qui permet la définition du menu associé au fond de
  carte

- addLayer : qui ajoute un menu associé à chaque couche de la carte

Ces deux méthodes sont directement appelée par le composant
workspace-page (voir §Le composant workspace-page méthode
\_loadWorkspaceDefinition).

Le composant va gérer les interactions entre les éléments de menu et le
workspace-page :

- Un clic sur l’icône d’un élément de menu déclenche la méthode
  toogleLayer qui masque le menu et envoi un évènement de type
  toggle-layer.

- Les flèches sur les entête d’élément de menu permettent de modifier
  la position des couches (méthode moveLayer) . Cela déclenche les
  évènements move-layer-back ou move-layer-front.

- Lors de la modification d’un paramètre pour un élément de menu ce
  dernier émet un évènement de type update-layer contenant les
  paramètres modifiés. Le composant propage cet évènement (voir
  méthode updated)

L’ensemble des évènements est envoyé au conteneur (workspace-page) pour
propager les modifications sur la carte.

<span id="_Toc116638827" class="anchor"></span>

### Les éléments de menu

L’ensemble des éléments de menu héritent de la classe SideMenuItem
défini dans le fichier _webview/workspace/menu/side-menu-item.js_.

Cette classe contient, en autre, le code permettant de générer les
interfaces pour chaque propriété en fonction de leur type (voir méthodes
renderProperty et renderPropertyValue).

On définit 4 types de menu :

| **Classe**             | **Tag**                              | **Fichier**                                          | **Description**                      |
| ---------------------- | ------------------------------------ | ---------------------------------------------------- | ------------------------------------ |
| SideMenuItemBackground | _workspace-sidemenu-item-background_ | _webview/workspace/menu/side-menu-item-backgound.js_ | Menu associé au fond de carte        |
| SideMenuItemData       | _workspace-sidemenu-item-data_       | _webview/workspace/menu/side-menu-item-data.js_      | Menu associé aux couches de données. |
| SideMenuItemEdge       | _workspace-sidemenu-item-edge_       | _webview/workspace/menu/side-menu-item-edge.js_      | Menu associés aux couches contour    |
| SideMenuItemHistogram  | _workspace-sidemenu-item-histogram_  | _webview/workspace/menu/side-menu-item-histogram.js_ | Menu associé aux couches histogram   |

Les spécifications portées par ces classes sont principalement contenues
dans la méthode render qui fait appel à la méthode renderProperty pour
afficher les champs de paramétrage de la couche.

### L’éditeur de scénario

L’éditeur est géré par le composant scenario-editor décrit dans le
fichier _webview/workspace/menu/editor/editor.js_.

Ce composant utilise [Quill.js](https://quilljs.com/docs/api/). La barre
d’outil de ce composant n’est pas compatible avec l’approche Web
Components, c’est pourquoi vous trouverez sa définition directement dans
le fichier _/webview/workspace/index.html_.

Ce composant dans son constructeur initialise un éditeur WYSIWYG Quill.

Deux boutons non standard ont été ajouté :

- savestate : qui permet la sauvegarde de l’espace de travail. Une
  action sur le bouton déclenche la méthode
  displayStateDefinitionPopup qui édite un tooltip de type « backup ».
  Ce tooltip est défini par la méthode \_extendTooltip qui lui ajoute
  une méthode save déclenchant la sauvegarde du workspace (saveState)

- togglelock : qui fait passer l’éditeur du mode lecture au mode
  édition. La modification du mode d’édition est défini dans la
  méthode \_toogleLock.

La sauvegarde d’un état crée un lien hypertexte spécifique dans
l’éditeur Quill. Ce lien est défini par la classe StateBlot. C’est cette
classe qui déclenche l’émission d’un évènement de type « restore-state »
lorsque l’on clique sur le lien. Cet évènement est capté par le
composant workspace-page pour charger la sauvegarde correspondante
(constructeur puis méthode resetLayersWith).

Le composant workspace-header intercepte également cet évènement pour
afficher un icone spécifiant que nous sommes dans un état spécifique. Un
clic sur cet icone va déclencher (méthode unlock) également un évènement
de type restore-state mais sans paramètre indiquant à l’application que
l’on revient à l’état le plus récent de l’espace de travail.

### Gestion de la carte

La gestion de la carte est délégué au composant carte-svg définit dans
le fichier _/webview/workspace/carte/carte-svg.js_.

Ce composant va manipuler une balise SVG pour afficher la carte. Le SVG
ne permettant pas la création de composant spécifiques (non présent dans
la spécification SVG), nous allons associer des balises SVG à des
classes JavaScript permettant de les manipulés. L’ensemble de ces
classes se trouvent dans le répertoire _/webview/workspace/carte/menu_.

La structure du composant SVG est la suivante : chaque élément
fonctionnel est contenu dans un groupe SVG qui lui est spécifique. On
trouve à la racine :

- L’histogramme

- La légende

- Les boutons d’actions (zoom)

- Le couches

Les couches sont-elles mêmes divisés en deux groupes SVG. Un pour le
fond de carte et un pour l’ensemble des autres couches.

La méthode la plus importante et première appeler pour le traitement de
l’affichage est la méthode initializeViewPortWithBackground. Cette
méthode va créer les groupes (SVG) associé au fond de carte et à
l’ensemble des autres couches puis va déclencher la méthode
initOriginalSize qui va définir les dimensions de la carte. La méthode
initViewPort va enfin être appelé pour :

- Calculer les coordonnées de la viewBox associé au composant SVG.

- Dessiner les boutons (zoom) .

- Dessiner la légende.

- Mettre à jour la taille de la zone associée à l’histogramme.

#### Les _couches_

L’ajout de couche se fait via la méthode addLayer qui va sélectionner la
classe associée à la gestion de la couche SVG.

Toutes les classes héritent de la classe Layer contenu dans le fichier
_/webview/workspace/carte/model/layer.js_. Elle prend en charge toutes
les fonctionnalités de base d’une couche : changement de profondeur,
affichage/masquage, calcul d’épaisseur de trait, colorisation.

Une couche est associé à un groupe SVG spécifique qui contient tous ces
composants graphique.

Les classes qui en héritent sont les suivantes, elle se situent toutes
dans le répertoire _/webview/workspace/carte/model_

<table>
<colgroup>
<col style="width: 17%" />
<col style="width: 19%" />
<col style="width: 62%" />
</colgroup>
<thead>
<tr class="header">
<th><strong>Classe</strong></th>
<th><strong>Fichier</strong></th>
<th><strong>Description</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Background</td>
<td>background.js</td>
<td>Gestion de la couche associé au fond de carte (unique)</td>
</tr>
<tr class="even">
<td>DataLayer</td>
<td>dataLayers.js</td>
<td><p>Gestion des couches de données :</p>
<ul>
<li><p>Icone (cercle ou carré)</p></li>
<li><p>Zone coloré</p></li>
</ul>
<p>Cette classe contient la méthode draw qui appele en fonction de sa
configuration les méthodes :</p>
<ul>
<li><p>drawCircle pour les cercles</p></li>
<li><p>drawSquare pour les carrer</p></li>
<li><p>drawColoredZonespour les zones colorées.</p></li>
</ul>
<p>Toutes ces méthode font appel à la méthode initSlider pour redéfinir
les intervalles de valeurs du slider (voir Le composant
workspace-page).</p>
<p>La méthode mousemove déclenche des évènements overZone et outZone
pour préciser quelle zone est survolée en fonction de son type</p></td>
</tr>
<tr class="odd">
<td>EdgeLayer</td>
<td>edgeLayer.js</td>
<td>Gestion d’une couche contour</td>
</tr>
<tr class="even">
<td>HistogramLayer</td>
<td>histogramLayer.js</td>
<td><p>Gère les couches associées à un histogramme.</p>
<p>La méthode mousemove est utilisée pour mettre en surbrillance la zone
survolée.</p>
<p>La méthode mouseclick est pour afficher/masquer l’histogramme et le
mettre à jour.</p></td>
</tr>
</tbody>
</table>

#### La légende

La légende est créée dans la méthode firstUpdated du composant
carte-svg.

Elle correspond à la classe Legend du fichier
_webview/worskpace/carte/model/legend.js_.

La légende doit toujours avoir la même taille, on se base donc sur le
ratio pour faire le rapport entre un nombre de pixel et la valeur réelle
dans le SVG (voir viewBox dans Gestion de la carte).

La carte propage toutes les actions sur les couches à la légende :

- toggle ; affichage/masquage d’un couche

- moveToBack, moveToFront : changement de l’ordre des couches

- updateLayerProperty : mise à jour des couleurs d’affichage

La méthode draw est en charge du dessin de la légende.

Chaque type de couche associé à une légende possède sa propre classe
héritant de LegendItem
(_webview/worskpace/carte/model/legend-item.js_) :

- LegendItemData (_legend-item-data.js_) : gère la légende des couches
  de données

- LegendItemEdge (_legend-item-edge.js_) : gère la légende des couches
  contour

Pour l’ensemble de ces classes c’est la méthode draw qui déclenche
l’affichage de la légende pour le composant correspondant.

#### L’histogramme

Comme pour la légende, l’histogramme est créée dans la méthode
firstUpdated du composant carte-svg.

Elle correspond à la classe Histogram du fichier
_webview/worskpace/carte/model/histogram.js_.

La méthode currentLayerId permet de spécifier à quelle couche est
associé l’histogramme et la méthode draw est en charge de l’affichage de
ce dernier.

#### L’info bulle

L’info bulle est créée dans la méthode firstUpdated du composant
carte-svg.

Il correspond à la classe InfoBulle du fichier
_webview/worskpace/carte/model/infobulle.js_.

Les méthodes principales sont addInfo et removeInfo qui définissent les
infos à afficher. Ces méthodes sont appelées par le composant carte-svg,
lors de la réception d’évènement de type overZone et outZone.

La méthode draw est en charge de l’affichage de l’info bulle. Deux
possibilités :

- L’info bulle est multivaluée alors toutes les informations des
  différentes couches survolées sont affichées.

- L’info bulle est monovaluée (par défaut), alors une seule
  information est affichée, celle correspondant à la couche en premier
  plan. Si dans le cas où deux informations se percutent seule la
  valeur la plus faible est affichée

<span id="_Toc116638834" class="anchor"></span>

# Annexe

## Structure des couches contours

Une couche est un fichier ZIP contenant :

- Un fichier _data.json_ décrivant les paramètres relatifs au
  contour (titre echelle et maille) :

```
{ ‟name‟ : ‟ …. ‟,‟title‟ : ‟ …. ‟,‟maille‟ : ‟ …. ‟, ‟echelle‟ : ‟ … ‟,
‟zones‟ : \[{‟path‟ :  ‟ …‟},…\]\]}
```

- name : identifiant de la couche

- title : titre de la couche

- maille : maille de la couche

- echelle : echelle de la couche

<!-- -->

- zones : tableau principal contenant la liste des tracés

  - path : Chemin au format SVG(path) pour le tracé des contours.
    Les coordonnées sont au format GeoJSON

## Structure des jeux de données

Une couche est un fichier ZIP contenant :

- Un fichier _data-set-info.json_, décrivant le jeux de données :

```
{ ‟name‟ : ‟ …. ‟,‟title‟ : ‟ …. ‟,‟maille‟ : ‟ …. ‟, ‟echelle‟ : ‟ … ‟}
```

- name : identifiant du jeux de donnée

- title : titre du jeux de données

- maille : maille du jeux de données

- echelle : echelle du jeux de données

<!-- -->

- Un fichier _data.txt_ qui contient le jeu de données. Ce fichier est
  un fichier au format CSV avec comme séparateur la tabulation. La
  première ligne donne le titre/identifiant de la donnée.

## Structure des fonds de carte

Une couche est un fichier ZIP contenant :

- Un fichier _mapInfo.json_, décrivant le fond de carte :

```
{ ‟name‟ : ‟ …. ‟,‟title‟ : ‟ …. ‟,‟maille‟ : ‟ …. ‟, ‟echelle‟ : ‟ … ‟,
‟authors‟ : ‟ … ‟}
```

- name : identifiant du jeux de donnée

- title : titre du jeux de données

- maille : maille du jeux de données

- echelle : echelle du jeux de données

- authors : auteurs du fond de carte

<!-- -->

- Un fichier _data.json_ au format GeoJSON qui décrit les contours du
  fond de carte.

## Structure d’un fichier de définition d’un espace de travail

Ce fichier est une archive ZIP contenant l’intégralité de la définition
d’un espace de travail :

- Un fichier _workspace.json_, qui définit l’ensemble des paramètres
  de l’espace :

  - name : identifiant de l’espace

  - title : titre de l’espace

  - echelle : échelle de l’espace

  - maille : maille de l’espace de travail

  - backgroungLayer : définition du fond de carte

    - proprerties : propriété du fond de carte

      - id : identifiant de la couche

      - name : nom du fond de carte

      - url : localisation du fichier _data.json_ décrivant le
        fond de carte (voir §Structure des fonds de carte)

    - style : paramètre d’affichage du fond de carte

      - bgColor : couleur de fond au format hexadécimal du fond
        de carte

      - borderColor : couleur des contours de zone au format
        hexadécimal

      - fillColor : couleur de remplissage des zones du fond de
        carte

  - layers : liste des couches de l’espace de travail.  
    Pour chaque couche on identifie

    - type : le type de la couche (edge = contour, histogram =
      histogramme, data = données)

    - properties : liste des propriétés liées à la couche elle
      sont spécifique en fonction du type de la couche

      - id : identifiant de la couche

      - title : titre de la couche

      - Si couche de type edge :

        - url : localisation du fichier data.json décrivant le
          contour (voir §Structure des couches contours)

      - Si couche de type histogram :

        - dataSetFile : localisation du fichier data.json
          décrivant le contour (voir §Structure des jeux de
          données dans un espace de travail)

        - usedData : la liste des noms de données à utiliser
          dans l’histogramme

        - dataSource : nom court de l’histogramme

        - longTitle : nom long de l’histogramme

      - Si couche de type data :

        - indicator : l’identifiant de l’indicateur à afficher

        - slider : définition des quantiles

          - maxValue : valeur maximale

          - minValue : valeur minimale

          - ranges : tableau des valeurs de séparation de
            chaque quantile

    - style : paramètre d’affichage du fond de carte

      - bgColor : couleur de fond au format hexadécimal de la
        couche

      - borderColor : couleur des contours de la couche

      - fillColor : couleur de remplissage de la couche

      - display : true/false pour savoir si la couche est
        affichée ou masqué

      - Si couche de type data :

        - iconType : type d’affichage (circle = icone de type
          cercle, square = icone de type cercle,
          coloredZones : zone colorisée)

        - rangeFillColors : tableau de couleur d’affichage de
          chaque quantile (format hexadécimale).

- Un répertoire _background_ contenant les fichiers _data.json_ et
  _mapInfo.json_ décrit au paragraphe _Structure des fonds de carte._

- Un répertoire _edgelayer_ contenant, pour chaque couche contour, un
  répertoire correspondant à l’identifiant de la couche. Ce répertoire
  contient alors un fichier _data.json_ au format décrit dans le
  paragraphe _Structure des couches contours_.

- Un répertoire _dataSets_ contenant pour chaque jeu de données un
  répertoire correspondant à l’identifiant du jeu de données. Dans ce
  répertoire, on trouve les fichiers _data-set-info.json_ et
  _data.txt_ tel que décrit au paragraphe Structure des jeux de
  données. Plus un fichier _data.json_, issu de la transformation du
  fichier data.txt (voir §Structure des jeux de données dans un espace
  de travail)

- Un répertoire _backup_ qui contient l’ensemble des sauvegardes
  utilisées dans le cadre des scénarios. Le nom de ce répertoire
  correspond à la date (en milliseconde) de création de la sauvegarde.
  Il contient une copie du fichier _workspace.json_ et des répertoires
  _background_, _dataSets_ et _edgelayer_ tels qu’ils étaient au
  moment de la création de la sauvegarde.

- Un fichier _scenario.json_ qui est une sauvegarde du contenu de
  l’éditeur WYSIWYG au format
  [Quill](https://quilljs.com/docs/api/#getcontents) (éditeur utilisé
  par la partie client de l’application).

### Structure des jeux de données dans un espace de travail

Le fichier est un fichier JSON. Contenant les propriétés suivantes :

- name : identifiant du jeux de donnée

- title : titre du jeux de données

- maille : maille du jeux de données

- echelle : echelle du jeux de données

- data : contenant la liste des donnée. Chaque propriété a :

  - pour nom, le nom de la colonne dans le fichier _data.txt_ dont
    les données sont issues (voir §Structure des jeux de données).

  - Pour valeur un tableau contenant la valeur de la donnée pour
    chaque ligne du fichier _data.txt_

## Paramétrage de l’application (application.properties)

Ce fichier est au [format
INI](https://fr.wikipedia.org/wiki/Fichier_INI) :

```
\[workspace\]

;Localisation du répertoire contenant les espaces de travail

location=myWorkspace

;Nom du sous répertoire contenant les sauvegarde de l’espace de travail

backup.location=backup

;Mode d’utilisation (basic\|expert)

user.profile=basic

\[localzipfiles\]

;Répertoire locaux pour dépôt des archives locales

url background=zipbackgrounds

dataset=zipdatasets

contours=zipcontours

workspace=zipworkspaces

\[server\]

;URL d’accés au serveur Gaia Mundi

url=http://localhost:3000

\[log\]

;Mode de traçage

level=debug

;fichier de trace

file.location=/log/gaiamundi.log

\[theme\]

;Couleur par défaut d’un fond de carte

background.backgroungColor=#FFFFFF

background.borderColor=#8F548A

background.fillColor=#F2EFE9

\[colorSets\]

;Jeux de couleurs prédéfini pour les quantiles.

;Format : titre1, couleur 1, .., couleur 5, titre 2, couleur 1,…

sets=5 couleur marron,#ECCF9A,#DC965A,#D67046,#B34B34,#5A1B1E,5 couleur
rouge,#FFDC8C,#FFB66E,#FF8538,#F24F12,#E50916
```
